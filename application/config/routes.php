<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "user_folder/user";
$route['404_override'] = '';

//user
$route['register'] = "user_folder/user/register";
$route['login'] = "user_folder/user/login";
$route['logout'] = "user_folder/user/logout";
$route['forgot_password'] = "user_folder/user/forgot_password";
$route["password_reinit/(:any)/(:any)"] = 'user_folder/user/password_reinit/$1/$2';
$route['email_validation/(:any)/(:any)'] = 'user_folder/user/email_validation/$1/$2';
$route['switchlang/(:num)'] = "user_folder/user/switchLang/$1";
$route['change_password'] = "user_folder/user/change_password";


// -------------------- user non connecté --------------------

$route['question/create'] = "quizz/question/create/1";


$route['question/repondre/(:any)'] = "quizz/reponse/repondre/$1";
$route['question/manage/(:num)'] = "quizz/question/manage/$1";
$route['question/edit/(:num)/(:any)'] = 'quizz/question/edit/$1/$2';
$route['question/stats/(:any)'] = 'quizz/question/stats/$1';
$route['question/clear/(:any)/(:any)'] = 'quizz/question/effacerResultats/$1/$2';
$route['question/delete/(:any)/(:any)'] = 'quizz/question/delete/$1/$2';
//			-----------------------------------------



// --------- user connecté, gestion questionnaire  -----------
$route['mesQuestionnaires'] = "espacemembre/demandeur/manageQuizzes";
$route['mesQuestionnaires/1'] = "espacemembre/demandeur/manageQuizzes/1";

$route['statsQuestionnaire/(:any)'] = 'espacemembre/questionnaire/stats/$1';
$route['gererQuestionnaire/(:any)']= "espacemembre/questionnaire/gerer/$1";
$route['exportQuestionnaire/(:any)'] = 'espacemembre/questionnaire/exporter/$1';
$route['viderQuestionnaire/(:any)'] = 'espacemembre/questionnaire/effacerResultat/$1';
$route['rmQuestionnaire/(:any)'] = 'espacemembre/questionnaire/supprimerQuestionnaire/$1';
$route['creerQuestionnaire'] = "espacemembre/questionnaire/nouveauQuestionnaire";
$route['modifierNom/(:any)'] = 'espacemembre/questionnaire/modifierNom/$1';
$route['changeOrder/(:any)'] = 'espacemembre/questionnaire/changeOrder/$1';

//			---------------------------------------             


// --------------- user connecté, gestion questions ---------------
$route['question/vider/(:num)'] = 'quizz/question/effacerResultat/$1';
$route['question/create/(:any)'] = "quizz/question/create/$1"; // ajoute une question a un questionnaire


//            -----------------------------------


//QrCode
$route['qr/(:any)'] = 'quizz/question/QrCode/$1';
$route['qrQt/(:any)'] = 'espacemembre/questionnaire/QrCode/$1';

//url raccourcie vers la question
$route['q/(:any)'] = 'quizz/reponse/repondre/$1';
$route['qt/(:any)'] = 'espacemembre/questionnaire/repondre/$1';

/* End of file routes.php */
/* Location: ./application/config/routes.php */