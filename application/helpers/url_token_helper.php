<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function idToToken($data) {
    return trim(base64_encode(base_convert($data,10,16)), "=");
}
 
function tokenToId($data) {
    return base_convert(base64_decode($data),16,10);
}

?>