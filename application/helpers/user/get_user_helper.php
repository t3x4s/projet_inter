<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//------------------------------------------------------------------------------------------------------------

/**
 * getUser
 *
 * Retourne l'id par défaut ou l'email de l'utilisateur s'il est connecté
 * Le 1er paramètre permet de retourner des valeurs numériques uniquement (0 si non trouvé)
 * Le second permet de retourner l'email de l'utilisateur connecté
 * 
 * @access	public
 * @param   $numerics bool
 * @param   $lookUpEmail : bool
 * @return  bool | int
 */
if(! function_exists('getUser'))
{
	function getUser ($numerics = FALSE,$lookUpEmail = FALSE)
	{
		$CI =& get_instance();

		if($lookUpEmail == TRUE) return $CI->encrypt->decode($CI->session->userdata('e_code'));


		$data = $CI->_db->select('ME_id')
						->from('t_membre_me')
						->where('ME_mail',$CI->encrypt->decode($CI->session->userdata('e_code')))
						->get()->row_array();

		if($numerics == TRUE){
			return empty($data) ? 0 : $data['ME_id'];
		} 
		else
		{
			empty($data) ? FALSE : $data['ME_id'];
		}
	}
}
/* End of file getUser_helper.php */
/* Location: ./application/helpers/user/getUser_helper.php */