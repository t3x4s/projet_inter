<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//------------------------------------------------------------------------------------------------------------

/**
 * check_member_activated
 *
 * Vérifie si le compte d'un membre a été activé ou non
 * (On vérifie s'il n'y a pas de correspondance dans la base)
 *
 * @param $email : String
 * @return bool
 */

if ( ! function_exists('check_member_activated'))
{
	function check_member_activated($email)
	{

 		$CI = & get_instance();

		$data = $CI->_db->select('VA_id_membre')
						->from('s_valider_ens_va')
						->join('t_membre_me','ME_id=VA_id_membre','inner')
						->where('ME_mail',$email)
						->get()->row_array();
						
		return empty($data) ? TRUE : FALSE;
	}
}

/* End of file check_member_activated.php */
/* Location: ./application/helpers/check_member_activated.php */