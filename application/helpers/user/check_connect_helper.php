<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//------------------------------------------------------------------------------------------------------------

/**
 * check_connect
 *
 * Vérifie si un membre est connecté ou non
 * Renvoie True si oui False sinon
 *
 * (On utilisera l'id de la session)
 *
 * @return bool
 */

if ( ! function_exists('check_connect'))
{
	function check_connect()
	{
		$CI = & get_instance();
		if($CI->session->userdata('e_code'))
		{
			return true;
		}
		return false;
	}
}
/* End of file check_connect.php */
/* Location: ./application/helpers/check_connect.php */