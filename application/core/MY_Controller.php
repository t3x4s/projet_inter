<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MY_Controller
 *
 * Classe mère du site
 *
 * @package Projet INTER
 * @category application/core
 * @author Thibault LR
 * 
 */
class MY_Controller extends CI_Controller
{

	/**
	* Titre de la page
	*
	* @var string
	* @access protected
	*/
	protected $_title = "Enseignement interactif";


	/**
	* Base de donnée principale
	*
	* @var db object()
	* @access public
	*/
		public $_db;


	function __construct()
	{

		parent::__construct();

		
		//chargement des bases de données
		try {
			$this->_db = $this->load->database('default',TRUE);
		} catch (Exception $e) {
			show_error('Unable to load database');
			log_message('error',$e->getMessage());
		}
		
		if( $this->_db->conn_id == FALSE)
		{
			show_error('Le site est en maintenance');
		}

		/*   -----   on charge la langue choisie par l'user ----- */
		$lang = $this->session->userdata('lang');
		if($lang!="") $this->config->set_item('language', $this->session->userdata('lang'));
		$this->lang->load('navbarHeaderFooter', $this->config->item('language'));
		/* 	 -----											----- */ 

		$this->load->language('common');

		$this->output->enable_profiler(false);
	}


//------------------------------------------------------------------------------------------------------------

/**
 * _render
 *
 * permet de faire le rendu d'une ou de plusieurs vues passées en paramètre
 *
 *
 * @access	public
 * @param   $viewArray
 */

	public function _render($viewArray)
	{

		//set title
		if($this->_title != '') {
			$template['title'] = $this->_title;
		}
		else {
			$template['title'] = "Projet INTER";
		}
		$template['content'] = $viewArray;
		
		$this->load->view('template/template',$template);

	}
}
