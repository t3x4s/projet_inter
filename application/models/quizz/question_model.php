<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Question_model extends CI_Model {


	/**
	 * @var $_arrayResults
	 */
	private $_arrayResults = array();

	/**
	 * @var $_bool
	 */
	private $_bool = FALSE;

	/**
	 * @var $_arrayData
	 */
	private $_arrayData = array();


	//constructeur
	function __construct()
	{
		parent::__construct();

		$this->load->helper('user/get_user');
	}

//------------------------------------------------------------------------------------------------------------

/**
 * get_TypesQuestion
 *
 * Retourne les types de question disponibles
 *
 *
 * @access	public
 */

	public function get_TypesQuestion ()
	{
		$this->_arrayResults = $this->_db	->select('TQ_id')
											->select('TQ_label')
											->from('t_type_question_tq')
											->get()->result_array();

		if( ! empty($this->_arrayResults))
		{
			$temp = array();
			foreach ($this->_arrayResults as $key => $value) {
				$temp[$value['TQ_id']] = $value['TQ_label'];
			}
		}		

		return (empty($this->_arrayResults)) ? FALSE : $temp;

	}


//------------------------------------------------------------------------------------------------------------

/**
 * addQuestion
 *
 * Ajoute une question en fonction du type de celle ci
 *
 *
 * @access	public
 * @param   typeQuestion : integer
 * @return  bool
 */

	public function addQuestion($typeQuestion)
	{
		$this->load->helper('url_token');
		
		if($typeQuestion == 1) //QCM
			$this->_bool = $this->addQuestionCM();
		else if($typeQuestion == 2)
			$this->_bool =	$this->addQuestionCM(); //Choix unique
		else if($typeQuestion == 3)
		{
			$this->_bool =	$this->addQuestionLibre();

		}
		else {}

		return idToToken($this->_bool);
	}

//------------------------------------------------------------------------------------------------------------

/**
 * addQuestionLibre
 *
 * Ajoute une question
 *
 *
 * @access	public
 * @return  bool
 */

	public function addQuestionLibre ()
	{
		$this->_arrayData = array(
							'QU_label'=>$this->input->post('NomQuestion'),
							'QU_membre_id'=>getUser(true),//utilisateur anonyme
							'QU_questionnaire_id'=> $this->input->post('questionnaire'),
							'QU_type_id'=>3);
		$this->_arrayData['QU_ordre'] = $this->getOrdre($this->_arrayData['QU_questionnaire_id']);
		$this->_bool = $this->_db->insert('t_question_qu',$this->_arrayData);

		return ($this->_bool == FALSE) ? FALSE : $this->_db->insert_id();
	}


//------------------------------------------------------------------------------------------------------------

/**
 * addQuestionCM
 *
 * Ajout de questions aux options multiples
 *
 *
 * @access	public
 * @return  bool
 */

	public function addQuestionCM()
	{

		//ajout de la question
		$this->_arrayData = array(
							'QU_label'=>$this->input->post('NomQuestion'),
							'QU_membre_id'=>getUser(true),
							'QU_questionnaire_id'=>$this->input->post('questionnaire'),
							'QU_type_id'=>$this->input->post('type_question'));
		$this->_arrayData['QU_ordre'] = $this->getOrdre($this->_arrayData['QU_questionnaire_id']);


		if($this->_db->insert('t_question_qu',$this->_arrayData))
		{
			//on récupère le dernier id inséré
			$idQuestion = $this->_db->insert_id();

			$this->_arrayData = array(); //reset du tableau

			//tableaux temporaires
			$arrayLibelleCb = $this->input->post('optionName');
			
			// tableaux de reponses
			for ($i=0; $i < sizeof($arrayLibelleCb); $i++) { 
				$this->_arrayData[$i] = array('CR_label'=>$arrayLibelleCb[$i],
											'CR_question_id'=>$idQuestion);
			}
			
			//insertion dans la base
			foreach ($this->_arrayData as $key => $value) {
				
				if( ! $this->_db->insert('t_choix_reponse_cr',$value))
				{
					show_error('Fail db');
				}
			}

			return $idQuestion; //retourne l'id pour le qrcode
		}
		else
		{
			return FALSE;
		}
		
	}


//------------------------------------------------------------------------------------------------------------

/**
 * getQuestion
 *
 * Récupère les infos d'une question à partir d'un id
 *
 *
 * @access	public
 * @param   $id : integer
 * @return  array/bool
 */

	public function getQuestion ($idQuestion)
	{
		$sql="SELECT QU_id, QU_label, QU_max_time, QU_questionnaire_id, QU_type_id, QU_fermee
				FROM t_question_qu
				WHERE QU_id= ?
			";
		$query = $this->db->query($sql, array($idQuestion) )->row_array();
		return empty($query) ? FALSE : $query;
	}


//------------------------------------------------------------------------------------------------------------

/**
 * getProposals
 *
 * Retourne les propositions de réponse d'une question
 *
 *
 * @access	public
 * @param   $idQuestion : integer
 * @return  array | bool
 */

	public function getProposals ($idQuestion)
	{
		$this->_arrayResults = $this->_db	->select('CR_id')
											->select('CR_label')
											->select('CR_question_id')
											->select('CR_correct')
											->from('t_choix_reponse_cr')
											->where('CR_question_id',$idQuestion)
											->get()->result_array();

		return empty($this->_arrayResults) ? FALSE : $this->_arrayResults;
	}



//------------------------------------------------------------------------------------------------------------

/**
* isThereCorrectAnswers
 *
 * Retourne si oui ou non il existe des réponses correctes à une question
 *
 *
 * @access	public
 * @param   $idQuestion : integer
 * @return  bool
 */

	public function isThereCorrectAnswers ($idQuestion)
	{
		$this->_arrayResults = $this->_db	->select('CR_id')
											->from('t_choix_reponse_cr')
											->where('CR_question_id',$idQuestion)
											->where('CR_correct',1)
											->get()->row_array();
											
		return empty($this->_arrayResults) ? FALSE : TRUE;
	}	


//------------------------------------------------------------------------------------------------------------

/**
 * getAnswers
 *
 * Retourne les réponses d'une question
 *
 *
 * @access	public
 * @param   $idQuestion
 * @return  array | bool
 */

	public function getAnswers($idQuestion)
	{
		$this->_arrayResults = $this->_db	->select('CR_correct')
											->from('t_choix_reponse_cr')
											->where('CR_question_id',$idQuestion)
											->get()->result_array();

		if(! empty($this->_arrayResults))
		{
			$temp = array();
			foreach ($this->_arrayResults as $key => $value) {
				$temp[] = $value['CR_correct'];
			}
		}
		return empty($this->_arrayResults) ? FALSE : $temp;
							
	}


//------------------------------------------------------------------------------------------------------------

/**
 * getIdAnswers
 *
 * Retourne l'id des réponses d'une question
 *
 *
 * @access	public
 * @param   $idQuestion
 * @return  array | bool
 */

	public function getIdAnswers($idQuestion)
	{
		$this->_arrayResults = $this->_db	->select('CR_id')
											->from('t_choix_reponse_cr')
											->where('CR_question_id',$idQuestion)
											->get()->result_array();

		if(! empty($this->_arrayResults))
		{
			$temp = array();
			foreach ($this->_arrayResults as $key => $value) {
				$temp[] = $value['CR_id'];
			}
		}
		return empty($this->_arrayResults) ? FALSE : $temp;
							
	}


//------------------------------------------------------------------------------------------------------------


/**
 * getLabelAnswers
 *
 * Retourne le label des réponses d'une question
 *
 *
 * @access	public
 * @param   $idReponse
 * @return  string
 */

	public function getLabelAnswers($idReponse)
	{
		$label = $this->_db	->select('CR_label')
											->from('t_choix_reponse_cr')
											->where('CR_id',$idReponse)
											->get()->row_array();

		return $label['CR_label'];
							
	}


//------------------------------------------------------------------------------------------------------------

/**
 * getCorrectAnswers
 *
 * Retourne si une réponse d'une question est correcte ou non 
 *
 *
 * @access	public
 * @param   $idReponse
 * @return  int : 0 pour faux et 1 pour vrai
 */

	public function getCorrectAnswers($idReponse)
	{
		$correct = $this->_db	->select('CR_correct')
											->from('t_choix_reponse_cr')
											->where('CR_id',$idReponse)
											->get()->row_array();

		return $correct['CR_correct'];
							
	}


//------------------------------------------------------------------------------------------------------------


/**
 * getTypeFromQuestion
 *
 * Retourne le type d'une question
 *
 *
 * @access	public
 * @param   $idQuestion
 * @return  integer | bool
 */

	public function getTypeFromQuestion($idQuestion)
	{
		$sql=	"SELECT QU_type_id
			 	 FROM t_question_qu
				 WHERE QU_id= ?
				";

		$query = $this->db->query($sql, array($idQuestion) )->row_array();
		return empty($query) ? FALSE : $query['QU_type_id'];
	}

//------------------------------------------------------------------------------------------------------------

/**
 * getTypeFromQuestion
 *
 * Retourne le label d'une question
 *
 *
 * @access	public
 * @param   $idQuestion
 * @return  Label de la question
 */

	public function getLabelFromQuestion($idQuestion)
	{
		/*
		$this->_arrayData = $this->_db	->select('QU_label')
										->from('t_question_qu')
										->where('QU_id',$idQuestion)
										->get()->row_array();
		return empty($this->_arrayData) ? FALSE : $this->_arrayData['QU_label'];
		*/

		$sql=	"SELECT QU_label
				FROM t_question_qu
				WHERE QU_id= ?
				";
		$query = $this->db->query($sql, array($idQuestion) )->row_array();
		return empty($query) ? FALSE : $query['QU_label'];
	}

//------------------------------------------------------------------------------------------------------------

/**
 * getTypeFromQuestion
 *
 * Retourne le type d'une question
 *
 *
 * @access	public
 * @param   $idQuestion
 * @return  Maxtime de la question
 */

	public function getMaxTimeFromQuestion($idQuestion)
	{
		$this->_arrayData = $this->_db	->select('QU_max_time')
										->from('t_question_qu')
										->where('QU_id',$idQuestion)
										->get()->row_array();
		return empty($this->_arrayData) ? FALSE : $this->_arrayData['QU_max_time'];
	}

//------------------------------------------------------------------------------------------------------------

/**
 * getTypeFromQuestion
 *
 * Retourne le type d'une question
 *
 *
 * @access	public
 * @param   $idQuestion
 * @return  est-ce que la question est fermee
 */

	public function getFermeFromQuestion($idQuestion)
	{
		$this->_arrayData = $this->_db	->select('QU_fermee')
										->from('t_question_qu')
										->where('QU_id',$idQuestion)
										->get()->row_array();
		return empty($this->_arrayData) ? FALSE : $this->_arrayData['QU_fermee'];
	}

//------------------------------------------------------------------------------------------------------------

/**
 * getTypeFromQuestion
 *
 * Retourne le type d'une question
 *
 *
 * @access	public
 * @param   $idQuestion
 * @return  numero de la question dans le questionnaire
 */

	public function getOrdreFromQuestion($idQuestion)
	{
		$this->_arrayData = $this->_db	->select('QU_ordre')
										->from('t_question_qu')
										->where('QU_id',$idQuestion)
										->get()->row_array();
		return empty($this->_arrayData) ? FALSE : $this->_arrayData['QU_ordre'];
	}

//------------------------------------------------------------------------------------------------------------

/**
 * getNbReponsesQCM
 *
 * retourne le nombre de réponses à une question choix multiple/unique
 *
 *
 * @access	public
 * @param   $idQuestion : integer
 * @return  array | bool
 */

	public function getNbReponsesQCM($idQuestion)
	{
		$sql=	"SELECT COUNT(DISTINCT(RQ_session_id)) as NbRepQCM
					FROM tj_reponse_propose_rq
					WHERE RQ_id_question= ?
				";
		$query = $this->db->query($sql, array($idQuestion) )->row_array();
		return empty($query) ? FALSE : $query['NbRepQCM'];
	}


//------------------------------------------------------------------------------------------------------------

/**
 * getNbReponsesLibre
 *
 * Retourne le nobmre de réponses d'une question libre
 *
 *
 * @access	public
 * @param   $idQuestion
 * @return  int | bool
 */

	public function getNbReponsesLibre($idQuestion)
	{
		$sql=	"SELECT COUNT(RL_question_id) as getNbReponsesLibre
				FROM t_reponse_libre_rl
				WHERE RL_question_id= ?
				";
		$query = $this->db->query($sql, array($idQuestion) )->row_array();
		return empty($query) ? FALSE : $query['getNbReponsesLibre'];
	}


//------------------------------------------------------------------------------------------------------------

/**
 * addQuickQuestionCM
 *
 * Ajoute une question à choix multiple avec 2 options (vrai ou faux)
 * ou ajoute une qcm avec un certains nombres de propositions(A,B,C,...)
 *
 *
 * @access	public
 * @return  bool
 */

	public function addQuickQuestionCM ($nbPropositions = 2)
	{
		//ajout de la question
		$this->_arrayData = array(
							'QU_label'=>'Question',
							'QU_membre_id'=>getUser(true),
							'QU_questionnaire_id'=>1,
							'QU_type_id'=>1);



		if($this->_db->insert('t_question_qu',$this->_arrayData) == TRUE)
		{
			//on récupère le dernier id inséré
			$idQuestion = $this->_db->insert_id();

			$this->_arrayData = array(); //reset du tableau

			//on considère que pour une question a 2 entrées on a Oui ou Non comme proposition de réponse
			if($nbPropositions == 2)
			{
				//ajout de 2 propositions de réponse Vrai et Faux
				$this->_arrayData[0] = array('CR_label'=>$this->lang->line('true'),'CR_question_id'=>$idQuestion,'CR_correct'=>0);
				$this->_arrayData[1] = array('CR_label'=>$this->lang->line('false'),'CR_question_id'=>$idQuestion,'CR_correct'=>0);				
			}
			else if($nbPropositions > 2 && $nbPropositions <26)
			{
			
				$alphabet = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');

				for ($i=0; $i < $nbPropositions; $i++) { 
					$this->_arrayData[] = array('CR_label'=>$alphabet[$i],'CR_question_id'=>$idQuestion,'CR_correct'=>0);
				}
			}
			else
			{
				//nombre de propositions trop important 
				return FALSE;
			}
			


			//insertion dans la base
			foreach ($this->_arrayData as $key => $value) {
				
				if( ! $this->_db->insert('t_choix_reponse_cr',$value))
				{
					show_error('Fail db');
				}
			}

			return $idQuestion; //retourne l'id pour le qrcode
		}
		else
		{
			return FALSE;
		}
	}

//------------------------------------------------------------------------------------------------------------

/**
 * addQuickQuestionMoreLess
 *
 * Ajoute une question avec 2 options (plus ou moins)
 *
 *
 * @access	public
 * @return  bool
 */

	public function addQuickQuestionMoreLess ()
	{
		//ajout de la question
		$this->_arrayData = array(
							'QU_label'=>'Question',
							'QU_membre_id'=>getUser(true),
							'QU_questionnaire_id'=>1,
							'QU_type_id'=>1);



		if($this->_db->insert('t_question_qu',$this->_arrayData) == TRUE)
		{
			//on récupère le dernier id inséré
			$idQuestion = $this->_db->insert_id();

			$this->_arrayData = array(); //reset du tableau

		
				//ajout de 2 propositions de réponse Oui et Non
				$this->_arrayData[0] = array('CR_label'=>$this->lang->line('more'),'CR_question_id'=>$idQuestion,'CR_correct'=>0);
				$this->_arrayData[1] = array('CR_label'=>$this->lang->line('less'),'CR_question_id'=>$idQuestion,'CR_correct'=>0);				
			


			//insertion dans la base
			foreach ($this->_arrayData as $key => $value) {
				
				if( ! $this->_db->insert('t_choix_reponse_cr',$value))
				{
					show_error('Fail db');
				}
			}

			return $idQuestion; //retourne l'id pour le qrcode
		}
		else
		{
			return FALSE;
		}
	}


//------------------------------------------------------------------------------------------------------------

/**
 * questionEditRights
 *
 * Retourne si oui ou non l'utilisateur a les droits pour éditer la question
 *
 *
 * @access	public
 * @param   $idQuestion : integer
 * @return  bool
 */

	public function questionEditRights ($idQuestion)
	{
		$this->load->helper('user/check_connect_helper');

		//si connecté
		if(check_connect())
		{
			$this->_arrayResults = $this->_db->select('QU_membre_id')
											->from('t_question_qu')
											->where('QU_id',$idQuestion)
											->get()->row_array();

			if(empty($this->_arrayResults)) return FALSE;

			return $this->encrypt->decode($this->session->userdata('iddem_code')) == $this->_arrayResults['QU_membre_id'] ? TRUE : FALSE;
		}
		else
		{
			$this->load->helper('cookie');

			return ($this->encrypt->decode(get_cookie('qId', TRUE)) == $idQuestion) ? TRUE : FALSE;			
		}


	}


//------------------------------------------------------------------------------------------------------------

/**
 * getReponsesQCM
 *
 * retourne le pourcentage des réponses à la question
 *
 *
 * @access	public
 * @param   $idQuestion : integer
 * @return  array | bool
 */

	public function getReponsesQCM($idQuestion)
	{
		$req = "CALL statsQCM(?)";
		$params = array($idQuestion);

		$result = $this->_db->query($req,$params);
		$this->_arrayData = $result->result_array();
		$result->next_result();
		$result->free_result();

		return empty($this->_arrayData) ? FALSE : $this->_arrayData;


	}

//------------------------------------------------------------------------------------------------------------

/**
 * getReponsesLibre
 *
 * Retourne les réponses d'une question
 *
 * @access	public
 * @param   $idQuestion
 * @return  int | bool
 */

	public function getReponsesLibre($idQuestion)
	{
		$sql=	"SELECT RL_label
				FROM t_reponse_libre_rl
				WHERE RL_question_id= ?
				";
		$query = $this->db->query($sql, array($idQuestion) )->result_array();
		return empty($query) ? FALSE : $query;


	}



//------------------------------------------------------------------------------------------------------------

/**
 * updateQuestion
 *
 * met à jour une question
 *
 *
 * @access	public
 * @param   $typeQuestion : intg
 * @return  bool
 */

	public function updateQuestion ($idQuestion)
	{

		$this->_bool = $this->updateQuestionInfos($idQuestion);

		if(!$this->_bool)
		{
			return FALSE;
		}


		if($this->input->post('TypeQuestion') != 3) //QCM
		{

			$this->_bool = $this->updateQuestionCM($idQuestion);
		}

		return $this->_bool;		
	}	


//------------------------------------------------------------------------------------------------------------

/**
 * updateQuestionInfos
 *
 * Mets à jour les informations de la question
 *
 *
 * @access	public
 * @param   $idQuestion
 * @return  bool
 */

	public function updateQuestionInfos ($idQuestion)
	{	
		$this->_arrayData = array(
								'QU_label'=>$this->input->post('NomQuestion'),
								//'QU_max_time'=>$this->input->post('MaxTime'),
								//'QU_questionnaire_id'=>$this->input->post('idQuestionnaire'),
								'QU_type_id'=>$this->input->post('TypeQuestion'),
								'QU_fermee'=>$this->input->post('QU_fermee'));

		if( ! $this->_db->update('t_question_qu', $this->_arrayData, array('QU_id' => $idQuestion)))
		{

			return FALSE;
		}

		return TRUE;

	}

//------------------------------------------------------------------------------------------------------------

/**
 * getReponseId
 *
 * Retourne un id de réponse en fonction du Label et de l'id de question
 *
 *
 * @access	public
 * @param   $idQuestion : integer
 * @param   $labelQuestion : String
 * @return  int | bool
 */

	public function getReponseId ($idQuestion,$labelQuestion)
	{
		if(! is_numeric($idQuestion) || ($labelQuestion != ''))
		{
			$req = $this->_db->select('CR_id')
								->from('t_choix_reponse_cr')
								->where('CR_question_id',$idQuestion)
								->where('CR_label',$labelQuestion)
								->get()->row_array();
		
			$this->_bool = empty($req['CR_id']) ? FALSE : $req['CR_id'];

		}
		else
		{
			$this->_bool = false;
		}

		return $this->_bool;
	}



//------------------------------------------------------------------------------------------------------------

/**
 * updateQuestionCM
 *
 * Met à jour une question à choix multiple
 *
 *
 * @access	public
 * @param   questionId int
 * @return  bool
 */

	public function updateQuestionCM ($idQuestion)
	{

		//début d'une transaction (cas d'une mise à jour simultanée)
		$this->_db->trans_start();

	
		//reset du tableau
		$this->_arrayData = array();

		//tableaux temporaires
		$arrayLibelleCb = $this->input->post('optionName');
		$arrayCrId = $this->input->post('optionId');

		//création tableau temporaire permettant de mettre à jour sans changer les stats
		$temp_array = $this->getProposals($idQuestion);

		

		//si les 2 tableaux font la même taille alors on peut faire une association 
		if( (sizeof($arrayLibelleCb) == sizeof($arrayValCb))) 
		{
			$arrayKey = 0;
			if(! empty($arrayCrId))
			{
				foreach ($arrayCrId as $key=>$val) 
				{
					//tableau de mise à jour des valeurs différentes des propositions en base
					$this->_arrayData[$key] = array(
												'CR_id'=>$this->encrypt->decode($val),
												'CR_label'=>$arrayLibelleCb[$key],
												'CR_question_id'=>$idQuestion,
												'CR_correct'=>$arrayValCb[$key]
												);		
					

					$this->_db->where('CR_id', $this->_arrayData[$key]['CR_id']);
					if(! $this->_db->update('t_choix_reponse_cr',$this->_arrayData[$key]))
					{
						show_error('Erreur update des mises à jour');
					}	

					//on incrémente la clé
					$arrayKey++;
				}	


				//suppresion
				//tableau des différences
				$result = array_map('unserialize', array_diff(
    				array_map('serialize', $temp_array), array_map('serialize', $this->_arrayData)));

				foreach ($result as $key => $value) {
					if(! $this->_db->delete('t_choix_reponse_cr',array('CR_id'=>$value['CR_id'])))
					{
						show_error('Erreur delete des mises à jour');
					}
				}



			}
			else
			{
				//on supprime toutes les lignes
				if($this->_db->delete('t_choix_reponse_cr',array('CR_question_id'=>$idQuestion)) == FALSE)
				{
						show_error('Erreur delete des mises à jour');

				}
			}

				//on insert les lignes supplémentaires
				for ($i=$arrayKey; $i < sizeof($arrayLibelleCb); $i++) { 
					
					$this->_arrayData = array('CR_label'=>$arrayLibelleCb[$i],
												'CR_correct'=>$arrayValCb[$i],
												'CR_question_id'=>$idQuestion);		

					if(! $this->_db->insert('t_choix_reponse_cr',$this->_arrayData))
					{
						show_error('Erreur insert des mises à jour');
					}
				}
		

		}
		else
		{
			show_error('Erreur : les champs n\'ont pas été correctement remplis');
		}
		
		// print_r($a);



		$this->_db->trans_complete();

		$this->_bool = true;

		if ($this->_db->trans_status() === FALSE)
		{
			show_error('Erreur à la fin de la mise à jour');	
		}			
	
	
	return $this->_bool;
	
	}




//------------------------------------------------------------------------------------------------------------


/**
* closeQuestion
*
* ferme une question 
* les résultats des réponses déjà réalisées sont conservées
* mais on ne peut plus répondre à cette question maintenant
*
* @access	public 
* @param   $idQuestion : int
* @return   bool
*/

	public function closeQuestion($idQuestion){

		$this->_db->where('QU_id',$idQuestion);
		if( ! $this->_db->update('t_question_qu', $this->_arrayData, array('QU_fermee' => '1')))
		{
			$this->_bool = FALSE;
		}
			
		return TRUE;
	}



//------------------------------------------------------------------------------------------------------------


/**
*openQuestion
*
* ouvre une question 
* on peut de nouveau répondre à  cette question après que celle ci a été fermée
*
* @access	public 
* @param   $idQuestion : int
* @return  bool
*/

	public function openQuestion($idQuestion){
		
		$this->_db->where('QU_id',$idQuestion);
		if ( ! $this->_db->update('t_question_qu', $this->_arrayData, array('QU_fermee' => '0')))
		{
			return FALSE;
		}

		return TRUE;
	}

//------------------------------------------------------------------------------------------------------------

/**
 * eraseResults
 *
 * Supprime les résultats d'une question
 *
 *
 * @access	public
 * @param   $idQuestion : int
 * @return  bool
 */

	public function eraseResults($idQuestion)
	{
		$type = $this->getTypeFromQuestion($idQuestion);

		if($type == FALSE){
			return FALSE;
		}
		else
		{
			//cas d'une question libre
			if($type == 3)
			{
				return $this->_db->delete('t_reponse_libre_rl',array('RL_question_id'=>$idQuestion)) == FALSE ? FALSE : TRUE;
			}
			else if($type == 1 || $type == 2)
			{
				return $this->_db->delete('tj_reponse_propose_rq',array('RQ_id_question'=>$idQuestion)) == FALSE ? FALSE : TRUE;
			}

		}

	}

//------------------------------------------------------------------------------------------------------------

/**
 * deleteQuestion
 *
 * Supprime une question
 *
 *
 * @access	public
 * @param   $idQuestion : int
 * @return  bool
 */

	public function deleteQuestion ($idQuestion)
	{
		return $this->_db->delete('t_question_qu',array('QU_id'=>$idQuestion)) == FALSE ? FALSE : TRUE;
	}
	
	//------------------------------------------------------------------------------------------------------------

/**
 * getOrdre
 *
 * Retourne le prochain ordre d'une question
 *
 *
 * @access	public
 * @param   $idQuestionnaire : int
 * @return  bool
 */

	public function getOrdre($idQuestionnaire)
	{
		$this->_arrayResults = $this->_db	->select('MAX(QU_ordre) AS ordre')
											->from('t_question_qu')
											->where('QU_questionnaire_id',$idQuestionnaire)
											->get()->row_array();
		return empty($this->_arrayResults) ? FALSE : $this->_arrayResults['ordre']+1;

	}



//------------------------------------------------------------------------------------------------------------

/**
 *
 * getQuestionsNoQuestionnaire
 *
 * Retourne les questions d'un utilisateur qui ne sont pas associées à un questionnaire
 *
 *
 * @access public
 * @return array | bool
 */
	
	public function getQuestionsNoQuestionnaire()
	{
		$this->_arrayResults = $this->_db	->select('QU_id')
											->select('QU_label')
											->from('t_question_qu')
											->where('QU_questionnaire_id',1)
											->where('QU_membre_id',$this->encrypt->decode($this->session->userdata('iddem_code')))
											->get()->result_array();

		return empty($this->_arrayResults) ? FALSE : $this->_arrayResults;
	}


}




/* End of file question_model.php */
/* Location: ./application/models/quizz/question_model.php */