<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reponse_model extends CI_Model {

	/**
	 * @var $_arrayResults
	 */
	private $_arrayResults = array();

	/**
	 * @var $_bool
	 */
	private $_bool = FALSE;

	/**
	 * @var $_arrayData
	 */
	private $_arrayData = array();



	//constructeur
	function __construct()
	{
		parent::__construct();

		$this->load->helper('user/get_user');

	}


//------------------------------------------------------------------------------------------------------------

/**
 * repLibre
 *
 * Permet de répondre à une question libre
 * Ajoute des données aux stats
 *
 * @access	public
 * @param   $id_Question integer
 * @return  bool
 */

	public function repLibre($id_Question)
	{
		$this->_arrayData = array(
							'RL_label'=>$this->input->post('repQuestion'),
							'RL_question_id'=>$id_Question,
							'RL_membre_id'=>getUser(true)
							);

		if($this->_db->insert('t_reponse_libre_rl',$this->_arrayData))
		{
			return true;
		}

		return false;
	}

//------------------------------------------------------------------------------------------------------------

/**
 * repQCM
 *
 * Ajout de stats sur les réponses au QCM
 *
 *
 * @access	public
 * @param   $id_Question : integer
 * @return  bool
 */

	public function repQCM($id_Question)
	{
		foreach ($this->input->post('optionVal') as $key => $value) {

			if($value == 1)
			{
				if(! $this->_db->insert('tj_reponse_propose_rq',array(
										'RQ_id_question'=>$id_Question,
										'RQ_session_id'=>$this->session->userdata('session_id'),
										'RQ_id_reponse'=>$this->encrypt->decode($_POST['optionId'][$key]),
										'RQ_id_membre'=>getUser(true))))
				{
					return FALSE;
				}				
			}

		}

		return TRUE;
	}
	
//------------------------------------------------------------------------------------------------------------

/**
 * repSingleQCM
 *
 * Ajoute 1 réponse à un QCM
 *
 *
 * @access	public
 * @param   $id_Question : integer
 * @param 	$id_reponse : integer
 * @return  bool
 */
	public function repSingleQCM($id_Question,$id_reponse)
	{
		$this->_arrayData = array(
							'RQ_id_question'=>$id_Question,
							'RQ_session_id'=> $this->session->userdata('session_id'),
							'RQ_id_reponse'=>$id_reponse,
							'RQ_id_membre'=>getUser(true)
							);

		if($this->_db->insert('tj_reponse_propose_rq',$this->_arrayData)) $this->_bool = TRUE;

		return $this->_bool;
	}


//------------------------------------------------------------------------------------------------------------

/**
 * repSingleQLibre
 *
 * Ajoute 1 réponse à une question libre
 *
 *
 * @access	public
 * @param   $id_Question : integer
 * @param 	$label_reponse : String
 * @return  bool
 */
	public function repSingleQLibre($id_Question,$label_reponse)
	{

		$this->_arrayData = array(
							'RL_label'=>$label_reponse,
							'RL_question_id'=>$id_Question,
							'RL_membre_id'=>getUser(true)
							);

		if($this->_db->insert('t_reponse_libre_rl',$this->_arrayData))
		{
			$this->_bool = TRUE;
		}

		return $this->_bool;		
	}
}

/* End of file reponse_model.php */
/* Location: ./application/models/quizz/reponse_model.php */