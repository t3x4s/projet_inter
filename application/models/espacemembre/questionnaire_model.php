<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Questionnaire_model extends CI_Model {



	// recupere l'ensemble des questionnaire lié a un demandeur
	// TODO attention il faudra surement gerer les droit question demander renseignement a thibaut
	public function recupererQuestionnaires($iddemandeur)
	{
		$sql=  "SELECT `DQ_id_questionnaire` 
				FROM `tj_droits_questionnaire_dq`
				WHERE DQ_id_membre = ?
				";
		$query = $this->db->query($sql, array($iddemandeur) );
	
		$res = $query->result_array();
		$tabidquestionnaire = array();
		foreach ($res as $idqu) {
			 $tabidquestionnaire[]= $idqu['DQ_id_questionnaire'];
		}
		return $tabidquestionnaire;
	}


	//recupere l'ensemble des questions d'un questionnaire
	public function recupererQuestionsdunQuestionnaire($idquestionnaire)
	{


		$sql= 	"SELECT * 
				FROM `t_question_qu` 
				WHERE `QU_questionnaire_id` = ?
				ORDER BY `QU_ordre`
				";

		$query = $this->db->query($sql, array($idquestionnaire));
		return $query->result_array();
	}


	public function nomQuestionnaire($idquestionnaire)
	{
		$sql=  "SELECT `QT_label` 
				FROM `t_questionnaire_qt` 
				WHERE `QT_id` = ?
				";
		$query = $this->db->query($sql, array($idquestionnaire));
		$line= $query->row_array();
		return $line['QT_label'];
	}




	public function creerQuestionnaire($nom, $iddemandeur)
	{

		// on cree le questionnaire
		$sql= "INSERT INTO `t_questionnaire_qt`
				( `QT_label`, `QT_subcours_id`, `QT_est_note`) 
				VALUES ( ? , 1, 0)
				";
		// de base on pars sur du non noté on verra plus tard
  		// et on ne gere pas les cours pour l'instant
		$query = $this->db->query($sql, array($nom));



		// on recupere l'id du questionnaire crée
		$query= $this->db->query("SELECT MAX(`QT_id`) FROM `t_questionnaire_qt` WHERE `QT_label` = ?", array($nom));
		$query = $query->row_array();	
		$idQT= $query['MAX(`QT_id`)'];

		//et on le lie a notre demandeur
		$sql= "INSERT INTO `tj_droits_questionnaire_dq`
				(`DQ_id_questionnaire`, `DQ_id_droit_question`, `DQ_id_membre`) 
				VALUES ( ?, 1, ?)
				";
		$query = $this->db->query($sql, array($idQT, $iddemandeur ));
		return $idQT;
			

	}


	public function changeOrder($idQuizz, $tabIdQuestions)
	{
		for($i = 0, $max = count($tabIdQuestions); $i < $max ; $i++) {
			$sql='UPDATE `t_question_qu`
					SET `QU_ordre`= ?
					WHERE `QU_id`= ?
					';
			$query = $this->db->query($sql, array($i + 1, $tabIdQuestions[$i]));
		}
	}



	public function modifierNomQuestionnaire($nouveauNom, $idquestionnaire)
	{
	
		$sql=	"UPDATE `t_questionnaire_qt`
				SET `QT_label`= ? 
				WHERE `QT_id`= ?
				";
				
		$query = $this->db->query($sql, array($nouveauNom, $idquestionnaire ));

	}


	public function supprimerQuestionnaire($idquestionnaire)
	{
		$sql=  "DELETE 
				FROM `t_questionnaire_qt` 
				WHERE `QT_id` = ?
				";
		$this->db->query($sql, array($idquestionnaire));

		
	}


/*
	public function afficherQuestions($idquestionnaire)
	{
		// affiche toute les questions dont le QU_questionnaire_id est 

	}

	public function voirResultats($id_quest)
	{
		// afficher tout les resultats de toutes les question dont le QU_questionnaire_id correspond a $idquest
	}	
*/

}

/* End of file questionnaire_model.php */
/* Location: ./application/models/questionnaire_model.php */