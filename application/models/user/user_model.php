<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{


	/**
	 * @var $_arrayResults
	 */
	private $_arrayResults = array();

	/**
	 * @var $_bool
	 */
	private $_bool = FALSE;

	/**
	 * @var $_arrayData
	 */
	private $_arrayData = array();


	//constructeur
	function __construct()
	{
		parent::__construct();
	}

//------------------------------------------------------------------------------------------------------------

/**
 * 
 * add_member Model function
 *
 * insert Data from form to the user_main table
 *
 * Code : 32 
 *
 * @access	public
 * @param  	$code : integer
 * @return  bool
 */
function add_Member($code)
{

	$this->_arrayData = array('ME_mail' => $this->input->post('email'),
		'ME_password' => sha1($this->input->post('pass')),
		'ME_login'=>$this->input->post('login')
		);

	//check insert
	if($this->_db->insert('t_membre_me',$this->_arrayData) == FALSE)
	{
		log_message('error','models/user_model#add_Member :  - insert db_main t_ut');
		return FALSE;
	}
	
	//get id user
	if( ($user = $this->getUserID($this->input->post('email'))) == FALSE)
	{
		log_message('error','models/user_model#add_Member :  - getUserID');
		return FALSE;
	}
	else
	{
		//ajout table s_valider_ens_va
		$data_activation = array('VA_id'=> $user,
			'VA_code'=>$code,
			'VA_id_membre'=>$this->_db->insert_id());		

			//check insert
		if($this->_db->insert('s_valider_ens_va',$data_activation) == FALSE)
		{
			log_message('error','models/user_model#add_Member : insert db_main activation');
			return FALSE;
		}		
				
		
	}
		

		return TRUE;
}

//------------------------------------------------------------------------------------------------------------

/**
 * getUserID
 *
 * Retourne l'id de l'utilisateur avec un email
 *
 *
 * @access	private
 * @param   $email
 * @return  bool | int
 */

	private function getUserID ($email)
	{
		$this->_arrayResults = $this->_db	->select('ME_id')
											->from('t_membre_me')
											->where('ME_mail',$email)
											->get()->row_array();

		return empty($this->_arrayResults) ? FALSE : $this->_arrayResults['ME_id'];
	}

//------------------------------------------------------------------------------------------------------------

/**
 * Member authentification
 *
 *
 * @access	public
 * @param	-
 * @return bool
 */
function auth_Member()
{

	$this->_arrayResults = $this->_db	->select('ME_id')
										->select('ME_login')
										->select('ME_mail')
										->select('ME_password')
										->from('t_membre_me')
										->where('ME_mail',$this->input->post('login'))
										->or_where('ME_login',$this->input->post('login'))
										->get()->row_array();

	//check empty
	if(empty($this->_arrayResults))
	{
		log_message('error','Database#user_model#auth_member : Error empty tab auth data');
		return false;
	}

	//check password
	if($this->_arrayResults['ME_password'] != sha1($this->input->post('password'))) return false;

	$session = array(
		'e_code' => $this->encrypt->encode($this->_arrayResults['ME_mail']),
		'iddem_code' => $this->encrypt->encode($this->_arrayResults['ME_id']));
	$this->session->set_userdata($session);

	return true;
}

//------------------------------------------------------------------------------------------------------------

/**
 * Email validation
 *
 * Valide une demande d'enseignement
 *  
 * @access	public
 * @param	string : user email
 * @param 	string : code validation
 * @return 	bool
 */
function validate_member($email,$code)
{
	//on vérifie qu'il y a bien un champ dans la bdd
	$this->_arrayResults = $this->_db	->select('VA_id')
										->select('VA_id_membre')
										->from('s_valider_ens_va')
										->join('t_membre_me','ME_id=VA_id_membre','inner')
										->where('ME_mail',$email)
										->where('VA_code',$code)
										->get()->row_array();

	if (empty($this->_arrayResults)) return FALSE;

	if($this->_db->delete('s_valider_ens_va', array('VA_id' => $this->_arrayResults['VA_id'])))
	{

		//on ajoute les droits
		$this->_arrayData = array(
							'DM_id_droit'=>1, // droits enseignant
							'DM_id_membre'=>$this->_arrayResults['VA_id_membre']);
		
		if(! $this->_db->insert('tj_droits_membre_dm',$this->_arrayData))
		{
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}
	
	return TRUE;
}


//------------------------------------------------------------------------------------------------------------

/**
 * update_password
 *
 * met à jour le mot de passe utilisateur
 *
 *
 * @access	public
 * @param   new_pass : string
 * @param 	email : string
 * @return  bool
 */
	
	public function update_password($new_pass,$email)
	{
		$sql= 	"UPDATE `t_membre_me` SET `ME_password`= ?  WHERE `ME_id`= ?";
		$query = $this->db->query($sql, array($new_pass,$email));

		if($this->_db->affected_rows() == 0)
		{
			log_message('error','Database#user_model#change_password : erreur maj');
			return FALSE;
		} 
		return TRUE;

	}


//------------------------------------------------------------------------------------------------------------

/**
 * isMemberActivated
 *
 * Vérifie que le membre est bien activé
 * Renvoie vrai si le membre est activé
 *
 * @access	public
 * @param   $input : string
 * @return  bool
 */

	public function isMemberActivated ($input)
	{
		$this->load->helper('email_helper');

		if(valid_email($input))
		{
			$this->_arrayResults = $this->_db->select('VA_id_membre')
											->from('s_valider_ens_va')
											->join('t_membre_me','ME_id=VA_id_membre','inner')
											->where('ME_mail',$input)
											->get()->row_array();
		}
		else
		{
			$this->_arrayResults = $this->_db->select('VA_id_membre')
														->from('s_valider_ens_va')
														->where('VA_id_membre',$input)
														->get()->row_array();
		}


		return empty($this->_arrayResults) ? TRUE : FALSE; 
	}

}
/* End of file user_model.php */
/* Location: ./application/models/user_model.php */	
