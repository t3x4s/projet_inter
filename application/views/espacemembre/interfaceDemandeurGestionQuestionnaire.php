<fieldset id='GestionQuestionnaire'>
<?php
if(empty($idquestionnaire)) {
	echo '<legend>' . $this->lang->line('aucunQuestionnaire') . '</legend>';
}
else {
?>
		<legend><?=$this->lang->line('questionnaire_manage');?></legend> 

		<table class="table table-striped table-hover" id="quizzTable">
		<?php 
	  		for ($i=0, $max = count($idquestionnaire); $i < $max ; $i++) { 
	 			$nomQt = $nomQuestionnaire[$i];
	 			$idQt  = $idquestionnaire[$i];
	 			$nbQu = $nbquestionQuestionnaire[$i];
				?>

			<tr>
				<td>
				
				<!-- nom questionnaire et nbquestions associées -->
					<?=anchor( 'gererQuestionnaire/'.$idQt, '<span class="caret rotate2"></span> ' . $nomQt, array('class'=> 'btnsizelink gestQuest', 'id' => uniqid()));?>
					
					<span class="label label-primary hasTooltip nbQu" data-toggle="tooltip" data-placement="right" title="<?php
					if($nbQu > 1 )
						echo $nbQu .' '.$this->lang->line('questions');
					else if($nbQu == 0)
						echo $this->lang->line('no_question');
					else
						echo $nbQu.' '.$this->lang->line('question');
					?>">
					<?=$nbQu?>
					</span>
				
				<!-- menu de gestion questionnaire -->
				<td class="text-right">
					<div class="btn-group">
					<?=anchor( 'qrQt/'.$idQt, '<span class="glyphicon glyphicon-share"></span>  '. $this->lang->line('share'), array('class'=> 'btn btn-sm btn-primary questionShare'));?>
					<?=anchor( 'statsQuestionnaire/'.$idQt, '<span class="glyphicon glyphicon-stats"></span> '. $this->lang->line('stats'), array('class'=> 'btn btn-sm btn-primary')  );?>
					</div>
					<div class="btn-group text-left">
						<?=anchor( 'gererQuestionnaire/'.$idQt, '<span class="glyphicon glyphicon-edit"></span> '. $this->lang->line('manage'), array('class'=> 'btn btn-success btn-sm gestQuest2'));?>
						<button type="button" class="btn btn-sm btn-success dropdown-toggle" data-toggle="dropdown">	
							<span class="caret"></span>
							<span class="sr-only">Toggle Dropdown</span>
						</button>
						<ul class="dropdown-menu dropdown-menu-manage" role="menu">
							<li><?=anchor( 'modifierNom/' . $idQt, '<span class="glyphicon glyphicon-pencil"></span> '. $this->lang->line('questionnaire_changeName'), array('class'=>'nameChange', 'name'=>$nomQt))?></li>
							<li><?=anchor( 'exportQuestionnaire/'.$idQt, '<span class="glyphicon glyphicon-export"></span> '. $this->lang->line('export'))?></li>
							<li class="divider"></li>
							<li><?=anchor( 'viderQuestionnaire/'.$idQt, '<span class="glyphicon glyphicon-refresh"></span> '. $this->lang->line('delete_answers'), array('class'=> 'viderQuest'));?></li>
							<li><?=anchor( 'rmQuestionnaire/'.$idQt, '<span class="glyphicon glyphicon-trash"></span> '. $this->lang->line('remove'), array('class'=> 'rmQuest'));?></li>
						</ul>
					</div>
				</td>
			</tr>

			<?php
 		}
		?>
		</table>
<?php }?>

		<? if(isset($errors) && !empty($errors)):?>

			<div id="success_message" class="alert alert-danger">  
			  <a class="close" data-dismiss="alert">×</a>  
				<?=$errors;?>
			</div>  
		<? endif;?>
		

		
<?/*
	// onclick

		<?php
		$idOCQTF=$OneClickQuestion[0];
		$idOCQML=$OneClickQuestion[1];
		$idOCQ3=$OneClickQuestion[2];
		$idOCQ4=$OneClickQuestion[3];
		?>

		<fieldset class='oneClickQuestion'>
			<legend> <?=$this->lang->line('oneclick_name');?> </legend>
			<a href="qr/<?=$idOCQTF?>"> <?=$this->lang->line('true/false') ?></a> <br>  <!-- a degager plus tard -->
			<a href="qr/<?=$idOCQML?>"> <?=$this->lang->line('more/less') ?></a>
			<a href="qr/<?=$idOCQ3?>"> <?=$this->lang->line('threePropositions') ?></a>
			<a href="qr/<?=$idOCQ4?>"> <?=$this->lang->line('fourPropositions') ?></a>
		</fieldset>
*/?>
	</fieldset>
	
	<?=form_fieldset($this->lang->line('questionnaire_add'));?>
		<?=form_open('creerQuestionnaire', 'id="addQuizzForm"');?>
		<div class="col-lg-10">
			<div class="input-group">
			  <input type="text" class="form-control" name="nomQuestionnaire" maxlength="45" placeholder="<?=$this->lang->line('questionnaire_add_placeholder')?>">
			  <span class="input-group-btn">
				<button class="btn btn-default" type="submit"><?=$this->lang->line('questionnaire_add_submit')?></button>
			  </span>
			</div>
		</div>
		<?=form_close();?>
	<?=form_fieldset_close();?>

	<?=form_open('', 'id="changeNameForm"');?>
	<input type="hidden" name="ajax" value="ajax">
	<div id="nameChange" class="modal fade">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title"><?=$this->lang->line('questionnaire_changeName')?></h4>
		  </div>
		  <div class="modal-body">
			<input type="text" name="nomQuestionnaire" maxlength="45" size="50" id="nomQuestionnaire" class="form-control">
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('close');?></button>
			<button type="submit" class="btn btn-primary"><?=$this->lang->line('questionnaire_add_submit');?></button>
		  </div>
		</div>
	  </div>
	</div>
	<?=form_close();?>

	<div id="modalBloc" class="modal fade"></div>

	<script type="text/javascript">
	$(document).ready(function(){
		
		$('.hasTooltip').each(function() {
			$(this).tooltip({});
		});
		
		loadedID = -1;
		
		$('.rmQuest').on('click',function(e){
			e.preventDefault();
			if(confirm('<?=$this->lang->line('questionnaire_confirm_remove')?>')){
				var bloc = $(this).parent().parent().parent().parent().parent();
				if(bloc.find('.gestQuest').attr('id') == loadedID) {
					// fermeture du questionnaire ouvert en ajax
					$('#ajaxQuestionnaire').remove();
					$('a .caret').addClass('rotate2');
					$('.electedlink').removeClass('electedlink');
				}
				var button = $(this);
				button.attr('disabled','disabled');
				
				$.ajax({
					type: 'POST',
					url: button.attr('href'),
					data: {
						<?=$this->security->get_csrf_token_name()?>: '<?=$this->security->get_csrf_hash()?>',
						ajax: true
					},
					success: function(data, textStatus, jqXHR) {
						if(data) {
							bloc.hide(500,function() {
								bloc.remove();
							});
							notify("<?=$this->lang->line('quizz_erased')?>");
						}
						else {
							notify("<?=$this->lang->line('quizz_not_erased')?>");
							button.removeAttr('disabled');
						}
					},
					error: function(jqXHR, textStatus, errorThrown) {
						notify("<?=$this->lang->line('quizz_not_erased')?>");
						button.removeAttr('disabled');
					}
				});
				
			}
		});
		
		$('.viderQuest').on('click',function(e){
			e.preventDefault();
			if(confirm("<?=$this->lang->line('questionnaire_confirm_delete_answer')?>")){

				var button = $(this);
				button.attr('disabled','disabled');
				
				
				$.ajax({
					type: 'POST',
					url: button.attr('href'),
					data: {
						<?=$this->security->get_csrf_token_name()?>: '<?=$this->security->get_csrf_hash()?>',
						ajax: true
					},
					success: function(data, textStatus, jqXHR) {
						if(data) {
							notify("<?=$this->lang->line('answers_erased')?>");
							
							if(button.parent().parent().parent().parent().parent().find('.gestQuest').attr('id') == loadedID) {
								// maj des compteurs de reponses
								$('.compteurRep').each(function() {
									$(this).text('0');
								});	
							}
							
						}
						else {
							notify("<?=$this->lang->line('answers_not_erased')?>");
						}
					},
					complete:function(jqXHR, textStatus) {
						button.removeAttr('disabled');
					},
					error: function(jqXHR, textStatus, errorThrown) {
						notify("<?=$this->lang->line('answers_not_erased')?>");
					}
				});
			}
		});
		
		$('.gestQuest').on('click',function(e){
			e.preventDefault();
			
			var id = $(this).attr('id');
			
			if(loadedID != -1) {
				$('#ajaxQuestionnaire').remove();
				$('#quizzTable a .caret').addClass('rotate2');
				$('.electedlink').removeClass('electedlink');
			}
			
			if(loadedID != id) {
				$(this).find('.caret').removeClass('rotate2');
				$(this).addClass('electedlink');
				var link = $(this).parent().parent().find('a').attr('href');
				
				var bloc = $('<tr>');
				var subbloc = $('<td>');
				
				bloc.attr('id', 'ajaxQuestionnaire');
				subbloc.attr('colspan', '2');
				subbloc.attr('id', 'ajaxReceiver');
				
				subbloc.appendTo(bloc);
				$(this).parent().parent().after(bloc);
				
				loadQuest(subbloc, link);
				
				loadedID = id;
			}
			else {
				// fermeture d'une page sans ouverture ajax
				loadedID = -1;
			}
		});
		
		$('.gestQuest2').on('click',function(e){
			e.preventDefault();
			$(this).parent().parent().parent().find('.gestQuest').trigger('click');
		});
		
		$('.nameChange').on('click',function(e){
			e.preventDefault();
			$('#nomQuestionnaire').val($(this).attr('name'));
			$('#changeNameForm').attr('action',$(this).attr('href'));
			$('#nameChange').modal();
		});
		
		$(document).on('click', '.questionShare', function(e) {
			e.preventDefault();
			var href = $(this).attr("href");
			$.ajax({
				type: 'POST',
				url : href,
				data: {
					<?=$this->security->get_csrf_token_name()?>: '<?=$this->security->get_csrf_hash()?>',
					ajax: true
				},
				success:function(data, textStatus, jqXHR) {
					$('#modalBloc').html(data).modal();
				},
				error: function(jqXHR, textStatus, errorThrown) {
					notify(errorThrown);
				}
			});
			e.unbind();
		});
		
		
		<?php	
		if($loadFirst) {
		?>
			$('.gestQuest:last').click();
		<?php } ?>
		
		// ajout champ pour savoir si javascript
		$('#addQuizzForm').append($('<input type="hidden" name="js" value="true">'));
		
	});
	
	function loadQuest(bloc, link) {
		$(bloc).html('<span class="glyphicon glyphicon-refresh rotating"></span> <?=$this->lang->line('loading_button')?>');
		$.ajax({
			type: 'POST',
			url: link,
			data: {
				<?=$this->security->get_csrf_token_name()?>: '<?=$this->security->get_csrf_hash()?>',
				ajax: true
			},
			success: function(data) {
				bloc.html(data);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				bloc.html(textStatus);
			}
		});
	}
	
	</script>
