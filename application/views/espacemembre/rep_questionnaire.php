<? if(isset($success) && $success): ?>
	<div id="success_message" class="alert alert-success">  
	  <a class="close" data-dismiss="alert">×</a>  
		<p><?=$this->lang->line('questionnaire_success');?></p>
	</div>  

<?endif;?>


<? if(isset($error)) : ?>
	<div class="alert alert-danger">
		<a class="close" data-dismiss="alert">×</a>  
		<p><?=$error;?></p>
	</div>	
<? endif;?>

<div>
	<h4><?=$nomQT;?></h4>

	<?=form_open('qt/'.$idQT,array('class'=>'well form-horizontal','role'=>'form'));?>

	<?php
	$counter_q = 0;
	foreach ($questions as $key => $value) :
	?>
		
		<div>
			<?=form_fieldset($value['QU_label']);?>
				
				<div class="form-group questiongroup">
					<?	if( ($value['QU_type_id'] !=3 )) :
						$counter_pp = 0;
						foreach ($purposals[$key] as $k=>$v) : ?>
						<div>
							<input type="<?=$value['QU_type_id']==1  ? 'checkbox' : 'radio'; ?>" name=<?='checkBox['.$key.']'.'[]';?> id="qCB<?php echo $counter_q . '-' . $counter_pp; ?>">

							<label class="btn btn-default" for="qCB<?php echo $counter_q . '-' . $counter_pp; ?>">
								<span class="glyphicon"></span>
								<?=$v['CR_label'];?>
							</label>
							
							<input type="hidden" class="hidCb" name=<?='optionVal['.$key.']'.'[]';?> value="0">
							<input type="hidden" name=<?='optionId['.$key.']'.'['.$k.']';?> value="<?=$this->encrypt->encode($v['CR_id']);?>">
						</div>
						<?php
						$counter_pp++;
						endforeach;
						?>
					<? else : ?>
					    <div class="col-sm-12">
							<?=form_textarea(array('name'=>'repQuestion['.$key.']','value'=>set_value('repQuestion'),'class'=>'form-control','rows'=>3));?>
					    </div>

					<? endif; ?>


				</div>

			<?=form_fieldset_close();?>

		</div>

		<input type="hidden" name=<?='qId[]';?> value=<?=$value['QU_id'];?>>
	<?php
	$counter_q++;
	endforeach;
	?>
<?php if(!(isset($success) && $success)) { ?>
	<?=form_submit('Envoyer',$this->lang->line('questionnaire_add_submit'), 'class = "btn btn-primary"')?>
<?php } ?>
	<?=form_close();?>

</div>
<script type="text/javascript">
$(document).ready(function(){

	//gestion des checkbox
	$('input[type=checkbox]').on("change",function(){
	    var target = $(this).parent().find('.hidCb').val();
	    if(target == 0)
	    {
	        target = 1;
	    }
	    else
	    {
	        target = 0;
	    }
	    $(this).parent().find('.hidCb').val(target);
	});

	//gestion radio
	$('input[type=radio]').on("change",function(){

	    var target = $(this).parent().find('.hidCb').val();
	    
		$('input[type=radio]').each(function(){
			$(this).parent().find('.hidCb').val(0);
		});
		

		if(target == 0)
		{
			target = 1;
		}
		else
		{
			target = 0;
		}

		$(this).parent().find('.hidCb').val(target);
	});

});




</script>
