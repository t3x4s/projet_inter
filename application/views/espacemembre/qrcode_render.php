<!-- Transmission du lien vers la page de reponse -->
		<div>
			<div id="blocQrCode" >
				<?=$this->lang->line('questionnaire_broadcastQrcode');?>
				<div class="row vspace3 text-center">
					<?php
						echo	'<a href="'.base_url().$img.'" download="Qrcode_'.$idQt.'.jpg" >
									<img src="'.base_url().$img.'" alt="Qrcode">
								</a>';
					?>
				</div>
			</div>

			<div id="lienVersionTxt">
				<span id='lienVersionTexte'> <?=$this->lang->line('questionnaire_broadcastLink');?></span>
				
				<!-- reducteur d'url -->
				<div class="form-group"> 
					<label for="inputQuestion" class="col-sm-2 control-label"><?=$this->lang->line('question_sharelink');?></label>
					<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon"><span class="glyphicon glyphicon-link"></span></span>
							<?=form_input(array('id'=>'longUrl', 'value'=>base_url().'qt/'.$idQt, 'class'=>'form-control','readonly'=>'readonly','onclick'=>'$(this).select()'));?>
						</div>
					</div>
				</div>

				<div class="col-sm-12 vspace2">
					<div class="input-group">
						<span class="input-group-btn" id="shortUrlContainer">
							<button id="shortUrl" data-loading-text="<span class='glyphicon glyphicon-refresh rotating'></span> <?=$this->lang->line('loading_button');?>" class="btn btn-default" type="button">
								<span class="glyphicon glyphicon-compressed"></span>
								<?=$this->lang->line('shorten_link');?>
							</button>
						</span>
					</div>
				</div>
				<!-- fin du reducteur d'url -->

			</div>
		</div>

	</body>

		

<!--

	<?=js('jquery.urlshortener.min.js');?>

	<script type="text/javascript">
		$('#shortUrl').click(function () {
			$('#shortUrl').button('loading');
			jQuery.urlShortener({
				longUrl: $('#longUrl').val(),
				success: function (shortUrl) {
					$('#shortUrl').parent().parent().append($('<input id="shortUrlInput" value="' + shortUrl + '" class="form-control">'));
					$('#shortUrl').button('reset').attr('id', 'shortUrlUsed');
					$('#shortUrlInput').select();
				},
				error: function(err)
				{
					$('#shortUrl').html(JSON.stringify(err));
				}
			});
		});
		$('#shortUrlUsed').on('click',function () {
			$('#shortUrlInput').select();
		});


	</script>-->
