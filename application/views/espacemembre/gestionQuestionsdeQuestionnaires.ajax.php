<?=form_open('changeOrder/' . $idQT,array('role'=>'form','id'=>'orderForm'));?>
<table class="table table-striped table-hover table-condensed" id="questionList">
	
	<?php
		foreach ($questions as $q) {
		// affichage des questions du questionnaire
		$nbrep = $q['nbanswers'];
		$idQu = idToToken($q['QU_id']);
		$intitule = $q['QU_label'];
	?>
	<tr>
		<td><span class="handle glyphicon glyphicon-sort"></span>
			<?=$intitule?>
			<span class="label label-success hasTooltip" data-toggle="tooltip" data-placement="right" title="<?php
			if($nbrep > 1 )
				echo $nbrep.' '.$this->lang->line('answers');
			else if($nbrep == 0)
				echo $this->lang->line('no_answer'); 
			else
				echo $nbrep.' '.$this->lang->line('answer');
			?>">
			<span class="compteurRep"><?=$nbrep?></span>
			</span>
		</td>
		<td class="text-right">
		
		<div class="btn-group text-left">
		  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
			<?=$this->lang->line('operations_question')?>
			<span class="caret"></span>
		  </button>
		  <ul class="dropdown-menu">
			<li>
			<?=anchor('qr/' . $idQu . '/' . $idQT,
					  '<span class="glyphicon glyphicon-share"></span> ' . $this->lang->line('question_share'),
					  'class="questionShare"')?>
			</li>
			<li>
			<?=anchor('question/stats/' . $idQu,
					  '<span class="glyphicon glyphicon-stats"></span> ' . $this->lang->line('stats'))?>
			</li>
			<li>
			<?=anchor('question/clear/' . $idQT . '/' . $idQu,
					'<span class="glyphicon glyphicon-refresh"></span> ' . $this->lang->line('delete_answers'),
					'class="clearQuestion"'
					)?>
			</li>
			<li>
			<?=anchor('question/delete/' . $idQT . '/' . $idQu,
					'<span class="glyphicon glyphicon-trash"></span> ' . $this->lang->line('remove'),
					'class="removeQuestion"'
					  )?>
			</li>
		  </ul>
		</div>
		<input type="hidden" name="quId[]" value="<?php echo $idQu; ?>">
		</td>
	</tr>
	<?php
	}
	?>
</table> 
<?=form_close();?> 

<div>
	<?=anchor('question/create/' . $idQT,
			  $this->lang->line('questionnaire_addQuestion'),
			  'id="createQuestion" class="btn btn-default" data-loading-text="<span class=\'glyphicon glyphicon-refresh rotating\'></span> ' . $this->lang->line('loading_button') . '"'
		)?>
</div>

<div class="collapse" id="addQuestion"></div>

<?=js('jquery-sortable-min.js');?>

<script type="text/javascript">
$(document).ready(function() {


	$('#questionList').sortable({
		axis: "y",
		cursor: "move",
		handle: 'span.handle',
		containerSelector: 'table.sortable',
		itemPath: '> tbody',
		itemSelector: 'tr',
		placeholder: '<tr class="placeholder caret rotate2"/>',
		onDrop: function ($item, container, _super) {
			$item.removeClass("dragged").removeAttr("style");
			$("body").removeClass("dragging");
			var form = $('#orderForm');
			var postData = form.serializeArray();
			var action = form.attr("action");
			postData.push({name:'ajax',value:true});
			$.ajax({
				type: "POST",
				url : action,
				data : postData,
				success:function(data, textStatus, jqXHR) {
					if(data) {
						notify("<?=$this->lang->line('questionnaire_changedOrder')?>");
					}
					else notify("<?=$this->lang->line('error')?>");
				},
				error: function(jqXHR, textStatus, errorThrown) {
					notify(errorThrown);
				}
			});
		}
	});

	$('.hasTooltip').each(function() {
		$(this).tooltip({});
	});

	$('#createQuestion').on('click',function(e){
		e.preventDefault();
		loadCreatePage();
	});
	
	$('.clearQuestion').on('click',function(e){
		e.preventDefault();
		if(confirm("<?=$this->lang->line('question_confirm_delete_answer')?>")) {
			var button = $(this);
			var bloc = button.parent().parent().parent().parent().parent();
			button.attr('disabled','disabled');
			
			$.ajax({
				type: 'POST',
				url: button.attr('href'),
				data: {
					<?=$this->security->get_csrf_token_name()?>: '<?=$this->security->get_csrf_hash()?>',
					ajax: true
				},
				success: function(data, textStatus, jqXHR) {
					if(data) {
						notify("<?=$this->lang->line('answers_erased_question')?>");
						bloc.find('.compteurRep').text('0');
					}
					else {
						notify("<?=$this->lang->line('answers_not_erased_question')?>");
					}
				},
				complete:function(jqXHR, textStatus) {
					button.removeAttr('disabled');
				},
				error: function(jqXHR, textStatus, errorThrown) {
					notify("<?=$this->lang->line('answers_not_erased_question')?>");
				}
			});
			
		}
	});
	
	$('.removeQuestion').on('click',function(e){
		e.preventDefault();
		if(confirm("<?=$this->lang->line('question_confirm_remove')?>")) {
			var button = $(this);
			var bloc = button.parent().parent().parent().parent().parent();
			button.attr('disabled','disabled');
			
			$.ajax({
				type: 'POST',
				url: button.attr('href'),
				data: {
					<?=$this->security->get_csrf_token_name()?>: '<?=$this->security->get_csrf_hash()?>',
					ajax: true
				},
				success: function(data, textStatus, jqXHR) {
					if(data) {
						var counter = $('#ajaxQuestionnaire').prev().find('.nbQu');
						counter.text(+counter.text()-1);
						notify("<?=$this->lang->line('question_erased')?>");
						
						bloc.hide(500,function() {
							bloc.remove();
						});
					}
					else {
						notify("<?=$this->lang->line('question_not_erased')?>");
						button.removeAttr('disabled');
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					notify("<?=$this->lang->line('question_not_erased')?>");
					button.removeAttr('disabled');
				}
			});
		}
	});
	
	<?php
	if(count($questions) == 0) {
		echo 'loadCreatePage();';
	}
	?>
	
});

function loadCreatePage() {
		$('#createQuestion').button('loading');
		$.ajax({
			type: 'POST',
			url: $('#createQuestion').attr('href'),
			data: {
				<?=$this->security->get_csrf_token_name()?>: '<?=$this->security->get_csrf_hash()?>',
				<?php
				if($this->input->post('ajax')) {
				//transmit two levels of ajax
				echo 'ajaxlvl2:true,';
				}
				?>
				ajax: true
			},
			success: function(data, textStatus, jqXHR) {
				$('#createQuestion').remove();
				$('#addQuestion').html(data);
				$('#addQuestion').slideDown();
			},
			error: function(jqXHR, textStatus, errorThrown) {
				$('#addQuestion').html(textStatus);
			}
		});
}

</script>
