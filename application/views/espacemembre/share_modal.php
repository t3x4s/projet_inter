<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title"><?=$this->lang->line('questionnaire_submit')?></h4>
		</div>
		<div class="modal-body">
			<div class="row vspace3 text-center">

				<?php
					echo	'<a href="'.base_url().$img.'" download="Qrcode_'.$idQt.'.jpg" >
								<img src="'.base_url().$img.'" alt="Qrcode">
							</a>';
				?>
			</div>
			<div class="form-group">
				<?=form_input(array('id'=>'longUrl', 'value'=>base_url().'qt/'.$idQt, 'class'=>'form-control','readonly'=>'readonly','onclick'=>'$(this).select()'));?>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('close');?></button>
		</div>
	</div>
</div>
