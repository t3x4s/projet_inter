	
	<script type="text/javascript"> 
		function confirmer(msg, action ) {
			if(confirm(msg)){
				 document.location.href=action; 
			}
		}
	</script>

	<fieldset>
		<legend>
			<?=form_open('modifierNom/' . $idQT, '');?>
			<div class="vspace1" id="nomDiv">
				<?php echo $nomQT; ?>
				<input type="text" name="nomQuestionnaire" maxlength="45" size="50" id="nomQuestionnaire" value="<?=htmlspecialchars($nomQT)?>">
				<button class="btn btn-primary" id="nomSubmit">
					<span class="glyphicon glyphicon-edit"></span>
					<?=$this->lang->line('questionnaire_changeName')?>
				</button>
			</div>
			<?=form_close();?>
		</legend>
		<div class="btn-toolbar vspace2">
			<?=anchor('qrQt/' . $idQT,
					  '<span class="glyphicon glyphicon-share"></span> ' . $this->lang->line('questionnaire_submit'),
					  'class="btn btn-sm btn-info"'
				);?>
			<?=anchor('viderQuestionnaire/' . $idQT,
					  '<span class="glyphicon glyphicon-refresh"></span> ' . $this->lang->line('delete_answers'),
					  'class="btn btn-sm btn-warning"'
				);?>
			<?=anchor('rmQuestionnaire/'. $idQT,
					  '<span class="glyphicon glyphicon-trash"></span> ' . $this->lang->line('questionnaire_remove'),
					     
					  array('class' => "btn btn-sm btn-danger", 
					  		'onClick' => "confirmer( '".$this->lang->line('questionnaire_confirm_remove')."', '".base_url()."rmQuestionnaire/".$idQT."'); return false;")

					  ); ?>
			<?=anchor( 'statsQuestionnaire/'.$idQT,
								'<span class="glyphicon glyphicon-stats"></span> '. $this->lang->line('stats'),
								array('class'=> 'btn btn-sm btn-primary')  );?>
			
		</div>

		<?php
			$this->load->view('espacemembre/gestionQuestionsdeQuestionnaires.ajax.php');
		?>
		
	</fieldset>

	<script type="text/javascript">
	$(document).ready(function() {
		$('#nomQuestionnaire').hide();
		
		$('#nomSubmit').on('click',function(e){
			e.preventDefault();
			
			var container = $('#nomDiv');
			var div = $('<div>');
			var subdiv = $('<div>');
			var submit = $('<span>');
			var input = $('#nomQuestionnaire').removeAttr('id').show();
			var button = $('#nomSubmit').removeAttr('id');
			
			div.addClass('form-group');
			subdiv.addClass('input-group');
			input.addrm-cClass('foontrol');
			submit.addClass('input-group-btn');
			button.addClass('btn btn-default');
			container.addClass('form-inline');
			
			submit.append(button);
			subdiv.append(input);
			subdiv.append(submit);
			
			
			div.append(subdiv);
			container.empty();
			container.append(div);
		});
		
	});
	</script>
