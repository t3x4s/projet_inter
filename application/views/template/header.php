<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="content-type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">

	<title>
		<? if (isset($title)):?>
			<?=$title;?>
		<?else:?>
			Projet INTER
		<?endif;?>
	</title>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->



	<?=css('bootstrap-notify.css');?>
	<?=css('bootstrap-theme.css');?>
	<?=css('bootstrap.css');?>
  	<?=css('theme.css');?>
  	<?=css('style.css');?>
    <?=js('jquery.js');?>
    <?=js('bootstrap-notify.js');?>
    <?=js('bootstrap.min.js');?>
</head>
<body>

<div class='notifications top-left'></div>
<div class="container theme-showcase">

