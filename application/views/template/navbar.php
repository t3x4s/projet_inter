<!-- Fixed navbar -->
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?=base_url(); ?>"><?=$title;?></a>
    </div>
    <div class="navbar-collapse collapse">
	  <ul class="nav navbar-nav">
        <!-- <li class="active"><a href="<?=site_url(); ?>">Home</a></li> -->
        <li><a href="<?=base_url().'question/create';?>"><?=$this->lang->line('navbar_create_question')?></a></li>

        <?php if ($this->session->userdata('e_code')):?>
            <li><a href="<?=base_url().'mesQuestionnaires';?>"><?=$this->lang->line('navbar_myquestionnaires')?></a></li>
            

      
        <? endif;?>
      </ul>
	  <ul class="nav navbar-nav navbar-right">
		<?php if ($this->session->userdata('e_code')): ?>
            <li class="dropdown">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="glyphicon glyphicon-user"></span> <?=$this->lang->line('profil_title')?><b class="caret"></b></a>
              
              <ul class="dropdown-menu">
                <li><a href="<?=base_url().'change_password';?>"><?=$this->lang->line('passwd_change')?></a></li>
                <li><a href="<?=base_url().'logout';?>"><?=$this->lang->line('navbar_logout')?></a></li>
              </ul>
           </li>
		<?php else: ?>
		<li><a href="<?=base_url().'register';?>"><?=$this->lang->line('navbar_register')?></a></li>
		<li><a href="<?=base_url().'login';?>"><?=$this->lang->line('navbar_login')?></a></li>
		<?php endif; ?>
<li class="dropdown">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?=$this->lang->line('lang_title')?><b class="caret"></b></a>
              
              <ul class="dropdown-menu">
    <li><a href="<?=base_url().'switchlang/1'?>"><img src="<?php echo site_url('assets/img/lang/fr.gif'); ?>" alt="<?=$this->lang->line('french_title')?>"  /> <?=$this->lang->line('french_title')?></a></li>
    <li><a href="<?=base_url().'switchlang/2'?>"><img src="<?php echo site_url('assets/img/lang/gb.gif'); ?>" alt="<?=$this->lang->line('english_title')?>"  /> <?=$this->lang->line('english_title')?></a></li>
	</ul>
           </li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</div>

