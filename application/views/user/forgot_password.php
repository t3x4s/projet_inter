

<? $errors=validation_errors();?>
<?if($errors != ""):?>
    <div id="success_message" class="alert alert-danger">
          <a class="close" data-dismiss="alert">×</a>
            <?=$errors;?>
    </div>
<? endif;?>



<div id="forgot_password">

    <?=form_open('forgot_password',array('class'=>'well form-horizontal'));?>

        <?=form_fieldset($this->lang->line('fieldset_forgot_pw'));?>

            <div class="form-group">
                <label for="inputEmail" class="col-sm-2 control-label"><?=$this->lang->line('validation_email');?></label>
                <div class="col-sm-10">
                    <?=form_input(array('name'=>'email','value'=>set_value('email'),'class'=>'form-control'));?>
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail" class="col-sm-3 control-label"><?=$this->lang->line('captcha_copy');?></label>
                <div class="col-sm-2"> <?=$captcha;?></div>
                <div class="col-sm-3">
                    <?=form_input(array('name'=>'captcha','class'=>'form-control'));?>
               </div>
            </div>

            <div class="form-actions">
                <button type="submit" class="btn btn-primary"><?=$this->lang->line('form_send');?></button>
            </div>

        <?=form_fieldset_close(); ?>

    <?=form_close(); ?>

</div>

