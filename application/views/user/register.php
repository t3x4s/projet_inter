<? if(isset($success) && $success == TRUE): ?>
    <div id="success_message" class="alert alert-success">  
        <a class="close" data-dismiss="alert">×</a>  
        <?=$this->lang->line('succes_register');?>    
    </div>  
<?endif;?>

<? if(isset($errors) && !empty($errors)):?>

    <div id="success_message" class="alert alert-danger">  
        <a class="close" data-dismiss="alert">×</a>  
        <?=$errors;?>
    </div>  
<? endif;?>

<div id="register">


    <?=form_open('register',array('class'=>'well form-horizontal','role'=>'form'));?>

    <?=form_fieldset($this->lang->line('fieldset_connection'));?>

    <div class="form-group">
        <label for="inputEmail" class="col-sm-2 control-label"><?=$this->lang->line('validation_email');?></label>
        <div class="col-sm-10">
            <?=form_input(array('name'=>'email','value'=>set_value('email'),'class'=>'form-control', 'id'=>'inputEmail'));?>
        </div>
    </div>

    <div class="form-group">
        <label for="inputLogin" class="col-sm-2 control-label"><?=$this->lang->line('validation_login');?></label>
        <div class="col-sm-10">
            <?=form_input(array('name'=>'login','value'=>set_value('login'),'class'=>'form-control', 'id'=>'inputLogin'));?>
        </div>
    </div>




    <div class="form-group">
        <label for="inputPassword" class="col-sm-2 control-label"><?=$this->lang->line('validation_pass');?></label>
        <div class="col-sm-10">
            <?=form_password(array('name'=>'pass','value'=>set_value('pass'),'class'=>'form-control', 'id'=>'inputPassword'));?>
        </div>
    </div>

    <div class="form-group">
        <label for="inputPassword2" class="col-sm-2 control-label"><?=$this->lang->line('validation_pass2');?> </label>
        <div class="col-sm-10">
            <?=form_password(array('name'=>'pass2','value'=>set_value('pass'),'class'=>'form-control', 'id'=>'inputPassword2'));?>
        </div>
    </div>



    <?=form_fieldset_close();?>




    <?=form_fieldset($this->lang->line('fieldset_security'));?>

    <div class="form-group">
        <label for="inputCaptcha" class="col-sm-3 control-label"><?=$this->lang->line('captcha_copy');?> </label>
        
            <div class="col-sm-2"> <?=$captcha;?></div>
            <div class="col-sm-3"> 
                <?=form_input(array('name'=>'captcha','value'=>set_value('captcha'),'class'=>'form-control', 'id'=>'inputCaptcha'));?>

       </div>
    </div>



<?=form_submit('send', $this->lang->line('form_send'), "class='btn btn-primary'");?>

    <?=form_fieldset_close();?>

    <?=form_close();?>
</div>

<script type="text/javascript">
$(document).ready(function(){

    //gestion des checkbox
    $('input[type=checkbox]').on("change",function(){
        var target = $(this).parent().find('input[type=hidden]').val();
        if(target == 0)
        {
            target = 1;
        }
        else
        {
            target = 0;
        }
        $(this).parent().find('input[type=hidden]').val(target);
    });

    var myCheckBox = $('input[type=checkbox]');
    var parent = $(myCheckBox).parent().find('input[type=hidden]').val();

    if(parent == 1)
    {
        myCheckBox.attr('checked', true);
    }

});
</script>

