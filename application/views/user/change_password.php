<? if(isset($success) && $success == TRUE): ?>
<div id="success_message" class="alert alert-success">  
  <a class="close" data-dismiss="alert">×</a>  
  <?=$this->lang->line('success_register');?>
</div>  
<?endif;?>

<? if(isset($errors) && !empty($errors)):?>

<div id="success_message" class="alert alert-danger">  
  <a class="close" data-dismiss="alert">×</a>  
  <?=$errors;?>
</div>
<? endif;?>

<? if(isset($update_error) && $update_error) :?>
   <div id="resu_req" class="alert alert-danger">
    <?=$this->lang->line('error_pw_change');?>
 </div>
<? endif;?>

<div id="change_pw">


	<?=form_open('change_password',array('class'=>'well form-horizontal','role'=>'form'));?>
	<?=form_fieldset('Changer de mot de passe');?>

    <div class="form-group">
        <label for="inputEmail" class="col-sm-2 control-label"><?=$this->lang->line('validation_old_password');?></label>
        <div class="col-sm-10">
            <?=form_password(array('name'=>'old_password','value'=>set_value('old_password'),'class'=>'form-control'));?>
        </div>
    </div>

    <div class="form-group">
        <label for="inputEmail" class="col-sm-2 control-label"><?=$this->lang->line('new_password');?></label>
        <div class="col-sm-10">
            <?=form_password(array('name'=>'pass','value'=>set_value('pass'),'class'=>'form-control'));?>
        </div>
    </div>


    <div class="form-group">
        <label for="inputEmail" class="col-sm-2 control-label"><?=$this->lang->line('new_password_confirm');?></label>
        <div class="col-sm-10">
            <?=form_password(array('name'=>'pass2','value'=>set_value('pass2'),'class'=>'form-control'));?>
        </div>
    </div>


<div class="form-actions">
   <button type="submit" class="btn btn-primary"><?=$this->lang->line('form_send');?></button>
   <button type="reset" class="btn">Annuler</button>
</div>

<?=form_fieldset_close();?>


<?=form_close();?> 

</div>