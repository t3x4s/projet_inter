<? if(isset($errors) && !empty($errors)):?>

    <div id="success_message" class="alert alert-danger">  
        <a class="close" data-dismiss="alert">×</a>  
        <?=$errors;?>
    </div>  
<? endif;?>

<? if(isset($error_login)):?>
    <div id="success_message" class="alert alert-danger">  
        <a class="close" data-dismiss="alert">×</a>  
        <?= $this->lang->line('error_login')?>
    </div>  
<? endif;?>
<div id="login">


    <?=form_open('login',array('class'=>'well form-horizontal','role'=>'form'));?>

    <?=form_fieldset($this->lang->line('fieldset_connection'));?>

    <div class="form-group">
        <label for="inputEmail" class="col-sm-2 control-label"><?=$this->lang->line('validation_login_or_email');?></label>
        <div class="col-sm-10">
            <?=form_input(array('name'=>'login','value'=>set_value('login'),'class'=>'form-control', 'id'=>'inputEmail'));?>
        </div>
    </div>


    <div class="form-group">
        <label for="inputPassword" class="col-sm-2 control-label"><?=$this->lang->line('validation_pass');?></label>
        <div class="col-sm-10">
            <?=form_password(array('name'=>'password','value'=>set_value('password'),'class'=>'form-control', 'id'=>'inputPassword'));?>
        </div>
    </div>



    <?=form_fieldset_close();?>

	    
	<div class="form-actions">
	  <button type="submit" id="submit" class="btn btn-primary"><?=$this->lang->line('form_send');?></button>
	  <a href="<?=base_url().'forgot_password';?>"><i class="icon-lock"></i><?=$this->lang->line('forgot_password');?></a>
	</div>


    <?=form_close();?>
</div>



