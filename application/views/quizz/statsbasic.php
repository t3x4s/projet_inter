
<div>
	<h3><?=$this->lang->line('answers_to_question')?><?=$titre?></h3>
	<p>
	<?
   	if($nb == 0){
		echo $this->lang->line('question_stats_no_answers');
   	}
	elseif($nb == 1) {
		echo $this->lang->line('question_stats_one_answer');
	}
	else echo $nb . $this->lang->line('question_stats_multiple_answers');
	?>
	</p>
</div>
