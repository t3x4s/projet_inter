<div id="create_question">
	<?=form_open('question/create/'.$defaultQuestionnaire,array('class'=>'well form-horizontal','role'=>'form','id'=>'questForm'));?>
	<?=form_fieldset($this->lang->line('question_fieldset'));?>

	<?php
	if($this->input->post('ajaxlvl2')) {
		echo form_hidden('ajax','true');
	}
	?>
	
	<div class="form-group">
		<label for="inputQuestion" class="col-sm-2 control-label"><?=$this->lang->line('question_libelle');?></label>
		<div class="col-sm-10" id="quest">
			<?=form_input(array('name'=>'NomQuestion','value'=>set_value('NomQuestion'),'class'=>'form-control','id'=>'inputQuestion','required'=>'required','maxlength'=>'100'));?>
		</div>
	</div>


	<div class="form-group">
		<label class="col-sm-2 control-label"><?=$this->lang->line('question_type');?></label>
		<div class="col-sm-10" id="type">
			<div class="btn-group" data-toggle="buttons" id="typeQuest">
				<label class="btn btn-default btn-sm">
					<span class="glyphicon glyphicon-check"></span>
					<input type="radio" name="type_question" value="1" checked> <?=$this->lang->line('question_qcm');?>
				</label>
				<label class="btn btn-default btn-sm">
					<span class="glyphicon glyphicon-record"></span>
					<input type="radio" name="type_question" value="2"> <?=$this->lang->line('question_qcs');?>
				</label>
				<label class="btn btn-default btn-sm">
					<span class="glyphicon glyphicon-pencil"></span> <?=$this->lang->line('question_libre');?>
					<input type="radio" name="type_question" value="3">
				</label>
			</div>
		</div>
	</div>


	<?=form_hidden('questionnaire',$defaultQuestionnaire);?>
	

	<div id="questChoix"  class="form-group">
		<label class="col-sm-2 control-label"><?=$this->lang->line('question_choix');?></label>
		<div id="cbList" class="col-sm-10 sortable">
			<?php for($i = 0, $max = max(count($values),2), $disabled = ($max == 2); $i < $max ; $i++) { ?>
			<div class="input-group proposition vspace2">
				<span class="handle glyphicon glyphicon-sort input-group-addon"></span>
				<input value="<?=$values[$i]?>" required="required" maxlength="100" class="form-control optname" name="optionName[]" type="text">
				<span class="input-group-btn">
					<a class="btn btn-danger gl"<?php if($disabled) echo ' disabled'; ?>>
						<span class="glyphicon glyphicon-remove"></span>
					</a>
				</span>
			</div>
			<?php } ?>
		</div>
	</div>
	
	<div class="form-group" id="addChoiceDiv">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="button" class="btn btn-default" id="addChoice">
			<span class="glyphicon glyphicon-plus"></span>
			<?=$this->lang->line('question_option');?>
			</button>
		</div>
	</div>
	
	<div class="col-sm-offset-2 col-sm-10 alert alert-warning alert-dismissable collapse" id ="limit-warning">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  <span class="glyphicon glyphicon-warning-sign"></span>
	  <strong>Warning!</strong> You are limited to 10 different answers
	</div>

	<? if (!$connected): ?>
    <?=form_fieldset($this->lang->line('fieldset_security'));?>

	<div class="form-group">
		<label for="inputCaptcha" class="col-sm-3 control-label"><?=$this->lang->line('captcha_copy');?> </label>
		<div class="col-sm-2"><?=$captcha;?></div>
		<div class="col-sm-3">
			<?=form_input(array('name'=>'captcha','value'=>set_value('captcha'),'class'=>'form-control', 'id'=>'inputCaptcha', 'required'=>'required'));?>
		</div>
	</div>
	<?endif;?>


	<div class="form-group">
		<div class=" col-sm-10">
			<button id="submitBtn" type="submit" class="btn btn-primary "><?=$this->lang->line('question_envoi');?></button>
		</div>
	</div>
	<?=form_fieldset_close();?>
	<?=form_close();?> 
	
</div>

<?=js('jquery-sortable-min.js');?>

<script type="text/javascript">

$(document).ready(function(){
	/* au chargement */
	
	$('#cbList').sortable({
		axis: "y",
		cursor: "move",
		handle: 'span.handle',
		containerSelector: 'div.sortable',
		itemPath: '',
		itemSelector: 'div',
		placeholder: '<span class="placeholder glyphicon glyphicon-chevron-right">',
		onDrop: function ($item, container, _super) {
		  $item.removeClass("dragged").removeAttr("style");
		  $("body").removeClass("dragging");
		  reorder();
		}
	});
 
	// coche le type activé au chargement de la page
	$('input[name=type_question]:checked', '#typeQuest').parent().addClass("active");
	// reactive le bouton ajout (firefox)
	$('#addChoice').removeAttr('disabled');
	
	typeActuel = $('input[name=type_question]:checked', '#typeQuest').val();
	
	if(typeActuel == 3)
	{
		$('#questChoix').hide();
		$('#addChoiceDiv').hide();
	}
	
	//ajout choix via bouton
	$('#addChoice').on('click', function(){
		var propId = $('#cbList div').length;
		
		if(propId > 8) {
			$('#addChoice').attr('disabled','disabled');
			$('#limit-warning').collapse('show');
		}
		
		if(propId > 9) {
			return;
		}
		
		var div = $('#cbList div').last().clone();
		div.appendTo('#cbList');
		div.find('.optname').val('').focus();

		if(propId >= 2) {
			$('#cbList .gl').each(function() {
				$(this).removeAttr('disabled');
			});
		}
		
	});
	
	//suppression d'un choix via bouton
	//syntaxe particuliere car element ajouté via dom, cf "jQuery function not binding to newly added dom elements"
	$(document).on('click', '.gl', function(e) {
		e.preventDefault();
		$('#addChoice').removeAttr('disabled');
		$(this).parent().parent().remove();
		reorder();
		if($('#cbList div').length <= 2) {
			$('#cbList .gl').each(function() {
				$(this).attr('disabled','disabled');
			});
		}
		
	});

	// detection de changement de type de question
	$('#typeQuest input').on('change',function(e){
		e.preventDefault();
		
		var newVal = $('input[name=type_question]:checked', '#typeQuest').val();
		
		if(newVal != 3 && typeActuel == 3)
		{
			// passage d'un champ libre a QCM / choix unique => on affiche les propositions
			$('#questChoix').slideDown();
			$('#addChoiceDiv').slideDown();
			// champs requis
			$('#cbList .optname').each(function() {
				$(this).attr('required','required');
			});
		}
		
		if(newVal == 3) {
			// champ libre => on cache les propositions
			$('#questChoix').slideUp();
			$('#addChoiceDiv').slideUp();
			// champ plus requis
			$('#cbList .optname').each(function() {
				$(this).removeAttr('required');
			});
		}
		typeActuel = newVal;
	});
	
	<?php
	// si appel ajax
	if($this->input->post('ajaxlvl2')) {
	?>
	$('#questForm').on('submit',function(e) {
		e.preventDefault();
		$('#submitBtn').attr('disabled','disabled');
		var postData = $(this).serializeArray();
		var action = $(this).attr("action");
		$.ajax({
			type: "POST",
			url : action,
			data : postData,
			success:function(data, textStatus, jqXHR) {
				$('#modalBloc').html(data).modal();
			},
			complete:function(jqXHR, textStatus) {
				var counter = $('#ajaxQuestionnaire').prev().find('.nbQu');
				counter.text(+counter.text()+1);
				loadQuest($('#ajaxReceiver'), $('#ajaxQuestionnaire').prev().find('.gestQuest').attr('href'));
			},
			error: function(jqXHR, textStatus, errorThrown) {
				notify(errorThrown);
			}
		});
		e.unbind();
	});
	<?php
	}
	else {
	?>
	$('#questForm').on('submit',function(e) {
		$('#submitBtn').attr('disabled','disabled');
	});
	<?php
	}
	?>
});

function reorder(){
	var id = 0;
	$('#cbList .propId').each(function() {
		$(this).attr('value', id);
		id++;
	});
	id = 0;
	$('#cbList .debug').each(function() {
		$(this).text(id);
		id++;
	});
}

function clearAll()
{
	$('#cbList').empty();
}

</script>
