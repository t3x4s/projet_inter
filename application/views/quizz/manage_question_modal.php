<? if(isset($share) || (isset($success) && $success == TRUE) && isset($idQuestion)) { ?>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title"><?php if(isset($share)) echo $this->lang->line('question_share'); else echo $this->lang->line('question_success'); ?></h4>
		</div>
		<div class="modal-body">
			<?php if(isset($success)) echo form_fieldset($this->lang->line('question_share')); ?>
			<div class="row vspace3 text-center">

				<?php
					echo	'<a href="'.base_url().$img.'" download="Qrcode_'.$idQuestion.'.jpg" >
								<img src="'.base_url().$img.'" alt="Qrcode">
							</a>';
				?>
			</div>


			<div class="form-group">
			<?=form_input(array('id'=>'longUrl', 'value'=>base_url().'q/'.$idQuestion, 'class'=>'form-control','readonly'=>'readonly','onclick'=>'$(this).select()'));?>
			</div>
			<?php if(isset($success)) { ?>
			<div class="form-group">
			<?=anchor('question/stats/'.$idQuestion, '<span class="glyphicon glyphicon-stats"></span> ' . $this->lang->line('question_get_stats'), 'class="btn btn-default"');?>
			</div>
			<?php } ?>
			<?php if(isset($success)) echo form_fieldset_close(); ?>
			
			<?php if($idQuestionnaire) { ?>
			<?=form_fieldset($this->lang->line('quizz_share'));?>
			<div class="col-sm-12">
				<div class="form-group">
				<?=form_input(array('id'=>'longUrl', 'value'=>base_url().'qt/'. $idQuestionnaire, 'class'=>'form-control','readonly'=>'readonly','onclick'=>'$(this).select()'));?>
				</div>
			</div>
			<?=form_fieldset_close();?>
			<?php } ?>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('close');?></button>
		</div>
	</div>
</div>

<?php
}


if(isset($errors) && !empty($errors)) { ?>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">Error</h4>
		</div>
		<div class="modal-body">
			<p><?=$errors;?></p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('close');?></button>
		</div>
	</div>
</div>

<?php } ?>
