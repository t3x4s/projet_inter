<? if(isset($success_input) && ($success_input == TRUE)): ?>

	<div id="success_message" class="alert alert-success">  
	  <a class="close" data-dismiss="alert">×</a>  
	  	<?=$this->lang->line('reponse_correct_input');?>
	</div> 
<? endif;?>

<? if(isset($errors) && !empty($errors)):?>

	<div id="success_message" class="alert alert-danger">  
	  <a class="close" data-dismiss="alert">×</a>  
		<?=$errors;?>
	</div>

<? endif;?>


<div id="repQCM">
	<?=form_open('question/repondre/'.idToToken($dataQuest['QU_id']),array('class'=>'well form-horizontal','role'=>'form'));?>
	<?=form_fieldset($dataQuest['QU_label']);?>

	<div class="form-group questiongroup">
		<?php
		if( ($dataQuest['QU_type_id'] !=3 ) && !empty($purposals)) :
		$counter = 0;
			foreach ($purposals as $key=>$value) :
			?>
			<div>
				<input type="<?php echo ($dataQuest['QU_type_id']==1 ) ? 'checkbox' : 'radio'; ?>" name="checkBox[]" id="qCB<?php echo $counter; ?>">

				<label class="btn btn-default" for="qCB<?php echo $counter; ?>">
					<span class="glyphicon"></span>
					<?=$value['CR_label'];?>
				</label>
				
				<input type="hidden" class="hidCb" name="optionVal[]" value="0">
				<input type="hidden" name="optionId[]" value="<?=$this->encrypt->encode($value['CR_id']);?>">
				
			</div>
			<?php
			$counter++;
			endforeach;
		endif;
		?>
	</div>

	<?php
	if(!isset($success_input)) {
		echo form_submit('Envoyer',$this->lang->line('reponse_envoi'), 'class = "btn btn-primary"');
	}
	
	echo form_fieldset_close();
	echo form_close();
	?>

</div>

<script type="text/javascript">
$(document).ready(function(){

	//gestion des checkbox
	$('input[type=checkbox]').on("change",function(){
	    var target = $(this).parent().find('.hidCb').val();
	    if(target == 0)
	    {
	        target = 1;
	    }
	    else
	    {
	        target = 0;
	    }
	    $(this).parent().find('.hidCb').val(target);
	});

	//gestion radio
	$('input[type=radio]').on("change",function(){

	    var target = $(this).parent().find('.hidCb').val();
	    
		$('input[type=radio]').each(function(){
			$(this).parent().find('.hidCb').val(0);
		});
		

		if(target == 0)
		{
			target = 1;
		}
		else
		{
			target = 0;
		}

		$(this).parent().find('.hidCb').val(target);
	});

});



	function clearAll()
	{
		$('.divChoix').empty();
	}


</script>
