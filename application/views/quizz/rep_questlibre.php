<? if(isset($success) && $success): ?>
	<div id="success_message" class="alert alert-success">  
	  <a class="close" data-dismiss="alert">×</a>  
	  	<?=$this->lang->line('reponse_enregistree');?>
	</div>  
<?endif;?>

<? if(isset($errors) && !empty($errors)):?>

	<div id="success_message" class="alert alert-danger">  
	  <a class="close" data-dismiss="alert">×</a>  
		<?=$errors;?>
	</div>

<? endif;?>




<div id="repQLibre">

	<?=form_open('question/repondre/'.idToToken($data['QU_id']),array('class'=>'well form-horizontal','role'=>'form'));?>
	<?=form_fieldset($data['QU_label']);?>

	<div class="form-group">
	    <label for="anwser" class="col-sm-2 control-label"><?=$this->lang->line('reponse_titre');?></label>
	    <div class="col-sm-10">
			<?=form_textarea(array('name'=>'repQuestion','value'=>set_value('repQuestion'),'class'=>'form-control','rows'=>4,'id'=>'anwser','maxlength'=>500));?>
	    </div>
	</div>
	
	<?php
	if(!(isset($success) && $success)) {
		echo form_submit('Envoyer',$this->lang->line('reponse_envoi'), 'class = "btn btn-primary"');
	}
	echo form_fieldset_close();
	echo form_close();
	?>

</div>

