<? if(isset($success) && $success == TRUE && isset($idQuestion) ): ?>
		<div id="modalBloc" class="modal fade"></div>
		
		<h1><?=$this->lang->line('question_success')?></h1>
		<p><?=$this->lang->line('question_new_link')?></p>
			<!--<input type="text" onclick="$(this).select();" value="<?=base_url().'q/'.$idQuestion ?>" size="30" readonly />-->

		<div class="form-group">
			<label for="inputQuestion" class="col-sm-2 control-label"><?=$this->lang->line('question_sharelink');?></label>
			<div class="col-sm-10" id="urlShorten">
				<div class="input-group vspace2">
					<span class="input-group-addon"><span class="glyphicon glyphicon-link"></span></span>
					<?=form_input(array('value'=>base_url().'q/'.$idQuestion, 'class'=>'form-control url','readonly'=>'readonly','onclick'=>'$(this).select()'));?>
					<span class="input-group-btn shortBtn">
						<button data-loading-text="<span class='glyphicon glyphicon-refresh rotating'></span> <?=$this->lang->line('loading_button');?>" class="btn btn-default shortUrl" type="button">
							<span class="glyphicon glyphicon-compressed"></span>
							<?=$this->lang->line('shorten_link');?>
						</button>
					</span>
				</div>
			</div>
		</div>
	
		<div class="col-sm-12">
			<?=anchor('question/stats/'.$idQuestion, '<span class="glyphicon glyphicon-stats"></span> ' . $this->lang->line('question_get_stats'), 'class="btn btn-default"');?>
			<?=anchor('qr/'.$idQuestion, '<span class="glyphicon glyphicon-qrcode"></span> ' . $this->lang->line('question_qrcode'), 'class="btn btn-default" id="statsBtn"');?>
			<?//=anchor('question/edit/'.$defaultQuestionnaire.'/'.$idQuestion, '<span class="glyphicon glyphicon-edit"></span> ' . $this->lang->line('question_edit'), 'class="btn btn-default"');?>
		</div>

		<?// echo $this->lang->line('question_get_stats_later').$idQuestion;?>
		
		<?=js('jquery.urlshortener.min.js');?>
		<script type="text/javascript">
		$('#urlShorten .shortUrl').click(function () {
			$(this).button('loading');
			jQuery.urlShortener({
				longUrl: $('#urlShorten .url').val(),
				success: function (shortUrl) {
					//$('#shortUrl').parent().parent().append($('<input id="shortUrlInput" value="' + shortUrl + '" class="form-control">'));
					//$('#shortUrl').button('reset').attr('id', 'shortUrlUsed');
					//$('#shortUrlInput').select();
					$('#urlShorten .shortBtn').remove();
					
					var div = $('#urlShorten div').clone();
					div.attr('id', 'url2');
					div.find('.url').val(shortUrl);
					div.appendTo('#urlShorten');
				//	$('#shortUrlContainer').html($('<input id="shortUrlInput" size="20" value="' + shortUrl + '" class="form-control">'));
					
				},
				error: function(err)
				{
					$('#urlShorten').html(JSON.stringify(err));
				}
			});
		});
		$('#shortUrlUsed').on('click',function () {
			$('#shortUrlInput').select();
		});
				
		$('#statsBtn').on('click',function(e) {
			e.preventDefault();
			var btn = $(this);
			btn.attr('disabled','disabled');
			var action = btn.attr("href");
			
			$.ajax({
				type: "POST",
				url : action,
				data : {
					<?=$this->security->get_csrf_token_name()?>: '<?=$this->security->get_csrf_hash()?>',
					ajax: true
				},
				success:function(data, textStatus, jqXHR) {
					$('#modalBloc').html(data).modal();
				},
				complete:function(jqXHR, textStatus) {
					btn.removeAttr('disabled');
				},
				error: function(jqXHR, textStatus, errorThrown) {
					notify(errorThrown);
				}
			});
			e.unbind();
		});
		
		
		</script>


<? endif;?>

<? if(isset($errors) && !empty($errors)):?>

	<div id="success_message" class="alert alert-danger">  
	  <a class="close" data-dismiss="alert">×</a>  
		<?=$errors;?>
	</div>  

<? endif;?>
