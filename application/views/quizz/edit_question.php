<? if(isset($success) && $success == TRUE ): ?>
	<div id="success_message" class="alert alert-success">  
	  <a class="close" data-dismiss="alert">×</a>  
	 	 <p> <?=$this->lang->line('modification_question_ok')?> </p>
	 	 <?=anchor('gererQuestionnaire/'.$idqu, 'Retourner au questionnaire'); ?>
	</div>
	<? elseif (isset($success) && $success == FALSE) : ?>
	<div id="success_message" class="alert alert-danger">  
	  <a class="close" data-dismiss="alert">×</a>  
	  	<blink>Errors</blink>
	</div>    
<?endif;?>




<? if(isset($errors) && !empty($errors)):?>

	<div id="success_message" class="alert alert-danger">  
	  <a class="close" data-dismiss="alert">×</a>  
		<?=$errors;?>
	</div>  

<? endif;?>


<div id="create_question">


	<?=form_open('question/edit/'.$this->uri->segment(3).'/'.$this->uri->segment(4) ,array('class'=>'well form-horizontal','role'=>'form'));?>
	<?=form_fieldset($this->lang->line('modify_question'));?>

	<div class="form-group">
		<label for="inputQuestion" class="col-sm-2 control-label"><?=$this->lang->line('question_libelle');?></label>
		<div class="col-sm-10" id="quest">
			<?=form_input(array('name'=>'NomQuestion','value'=>set_value('NomQuestion',$infosQuestion['QU_label']),'class'=>'form-control'));?>
		</div>
	</div>


	<div class="form-group">
		<label for="inputTypeQuestion" class="col-sm-2 control-label"><?=$this->lang->line('question_type');?></label>
		<div class="col-sm-10" id="type">
			<?=form_dropdown('TypeQuestion', $options_question,set_value('TypeQuestion',$infosQuestion['QU_type_id']),'class="form-control" id="typeQuest"');?>
		</div>
	</div>


	<div class="form-group">
		<!-- <label for="inputQuestionnaire" class="col-sm-2 control-label">Questionnaire</label> -->
		<div class="col-sm-10">
			<?=form_hidden('questionnaire',$idqu);?>
		</div>

	</div>

	<div class="form-group" id="questChoix">
		<label for="inputArea" class="col-sm-2 control-label"><?=$this->lang->line('question_choix');?></label>
		<div class="col-sm-10" id="qArea">
			<div class="divChoix" id="cbList">

			<? if( ($infosQuestion['QU_type_id'] != 3) && !empty($purposals)) : ?>
				<? foreach ($purposals as $key => $value) : ?>
				<div>
					<? if($infosQuestion['QU_type_id'] == 1 )://QCM ?>
						<input type="checkbox" name="checkBox[]" id="qCB">	

					<? elseif ($infosQuestion['QU_type_id'] == 2): //radio ?>
						<input type="radio" name="checkBox[]" id="qCB" <? if ($value['CR_correct'] == 1) echo "checked";?> >	

					<? endif;?>

					
						<div class="input-group col-sm-6">
							<input type="text" name="optionName[]" value="<?=!isset($_POST['optionName[]']) ? $value['CR_label'] :$_POST['optionName'][$key];?>" class="form-control">

							<span id="gl"class="input-group-addon glyphicon glyphicon-remove" style="border:0;"></span>    
						</div>
							<input type="hidden" id="hidCb" name="optionVal[]" value="<?=!isset($_POST['optionVal[]']) ? $value['CR_correct'] : $_POST['optionVal'][$key];?>">
							<input type="hidden" id="hidCR" name="optionId[]" value="<?=$this->encrypt->encode($value['CR_id']);?>">
						</div>
							

				<? endforeach;?>
			<? endif;?>
			</div>
		</div>
	</div>


	


<div class="form-group" id="addChoiceDiv">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="button" class="btn btn-default" id="addChoice"><?=$this->lang->line('question_option');?></button>
    </div>
  </div>

	

<div class="form-group">
    <div class=" col-sm-10">
	<button type="submit" class="btn btn-primary "><?=$this->lang->line('question_edit_confirm');?></button>    </div>
</div>	
  <?=form_fieldset_close();?>
	
	<?=form_close();?> 
</div>

<script type="text/javascript">
$(document).ready(function(){

	//au chargement
	if($('#typeQuest').find('option:selected').val() == 3)
	{
		$('#questChoix').hide();
		$('#addChoiceDiv').hide();			
		
	}

	init();

	$('#addChoice').click(function(){
		addChoice($('.divChoix'));
	});

	$('#gl').on('click',function(e){
		e.preventDefault; 
		$(this).parent().parent().remove();

	});

	//gestion des checkbox
	$('input[type=checkbox]').on("change",function(){
	    var target = $(this).parent().find('#hidCb').val();
	    if(target == 0)
	    {
	        target = 1;
	    }
	    else
	    {
	        target = 0;
	    }
	    $(this).parent().find('#hidCb').val(target);
	});

	//gestion radio
	$('input[type=radio]').on("change",function(){

	    var target = $(this).parent().find('#hidCb').val();
	    
		$('input[type=radio]').each(function(){
			$(this).parent().find('#hidCb').val(0);
		});
		

		if(target == 0)
		{
			target = 1;
		}
		else
		{
			target = 0;
		}

		$(this).parent().find('#hidCb').val(target);
	});


	$('select').on('change',function(e){
		e.preventDefault()
		clearAll();
		var newVal = $('select').val();

		if(newVal != 3)
		{
			$('#questChoix').show(100);		
			$('#addChoiceDiv').show(100);	
			addChoice($('.divChoix'));

		}
		else
		{
			$('#questChoix').hide();
			$('#addChoiceDiv').hide();		
		}

	});





});

	//coche les cases en $POST
	function init()
	{
		$('[id*="hidCb"]').each(function(e) {
		    var val=$(this).val(); 
		    var parentCb = $(this).parent().find('input:checkbox');
		    var parentval = parentCb.is(':checked');


		    if(parentCb.is(':checked') == true)
		    {
		    	parentval = 1;
		    }
		    else
		    {
		    	parentval = 0;
		    }

		   
		    if(parentval != val)
			{
		    	if(val == 0)
		    	{

		    		$(this).parent().find('input:checkbox').prop('checked',false);


		    	}
		    	else if (val == 1)
		    	{
		    		$(this).parent().find('input:checkbox').prop('checked',true);

		    	}
		    	else
		    	{
		    	}
		    }


		});
	}





	function addChoice(){
		var container = $('#cbList');
		var div = $("<div>");
		var subdiv = $("<div>");
		var inputs = container.find('input');
		var id = inputs.length+1;

		var questionType = $('select').val();

		div.appendTo(container);

		//Cas d'un QCM
		if(questionType == 1)
		{
			$('<input />',{type : 'checkbox',name:'checkBoxQuest[]',id:'qCB'}).appendTo(div);

		}
		else if(questionType == 2)
		{
			$('<input />',{type : 'radio',name:'checkBoxQuest[]',id:'qCB'}).appendTo(div);
		}


			subdiv.addClass("input-group col-sm-6");
			$('<input />',{type : 'text',name : 'optionName[]',class:'form-control'}).appendTo(subdiv);

			$('<span />',{style:"border:0;",class:'input-group-addon glyphicon glyphicon-remove',id:'gl'}).appendTo(subdiv);

			div.append(subdiv);
			$('<input />',{type: 'hidden',name:'optionVal[]',id:'hidCb',value:0}).appendTo(div);

		container.append(div);

	}

	function clearAll()
	{
		$('.divChoix').empty();
	}






</script>
