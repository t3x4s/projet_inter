<div class="jumbotron">
	<p><?=$this->lang->line('home_welcome');?></p>
	<p> <?=$this->lang->line('home_youcan');?>  <?=anchor(base_url().'question/create', $this->lang->line('home_create_question'));?></a>
	
	<? if($this->session->userdata('iddem_code')): ?>
		<p> <?=$this->lang->line('home_consult_quizz'); ?> <?=anchor('mesQuestionnaires', 'questionnaires'); ?>  </p>

	<? else: ?>	
		<p>  <?=$this->lang->line('home_elseyoucan');?> <?=anchor(base_url().'login', $this->lang->line('home_login'));?>  
			 <?=$this->lang->line('home_or');?>    <?=anchor(base_url().'register', $this->lang->line('home_register'));?> </p>
	<?endif;?>
</div>