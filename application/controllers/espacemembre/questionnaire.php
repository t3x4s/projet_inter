<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
*  Classe questionnaire
*/


class Questionnaire extends MY_Controller {


	//règles de validation
	private $_rules_string = 'trim|htmlspecialchars|required|max_length[100]|xss_clean|prep_for_form|encode_php_tags';
	private $_rules_natural = 'trim|htmlspecialchars|required|xss_clean|prep_for_form|encode_php_tags|is_natural';
	private $_rules_nomquestionnaire = 'trim|htmlspecialchars|required|max_length[45]|xss_clean|prep_for_form|encode_php_tags';

	// constructeur
	public function __construct()
	{
		parent::__construct();
		$this->load->database();

		$this->load->model('espacemembre/questionnaire_model');
		$this->load->model('quizz/question_model');

		$this->load->language('questionnaire');

		$this->load->library('form_validation');

	}

	// fonction qui assure le fait qu'une personne ne peut agir que sur ses propres questionnaire
	private function autorise($idquestionnaire)
	{
		$iddemandeur= $this->encrypt->decode($this->session->userdata('iddem_code'));
		$tabidquestionnaire= $this->questionnaire_model->recupererQuestionnaires($iddemandeur);	
		// on verifie que le questionnaire auquel l'user essaye d'acceder lui appartient
		return in_array($idquestionnaire, $tabidquestionnaire);
	}


//------------------------------------------------------------------------------------------------------------

/**
 * gerer
 *
 * Permet a l'utilisateur de gerer son questionnaire $idquestionnaire
 * Liste les question d'un questionnaire et propose des actions: gerer/supprimer/nouveauQuestionnaire/exporter/...
 *
 * @access	public
 * @param   $idquestionnaire : integer
 */

	public function gerer($idquestionnaire)
	{
		$this->load->helper('url_token');
		$idquestionnaire = tokenToId($idquestionnaire);
		if($this->autorise($idquestionnaire)){

			$nomQT = $this->questionnaire_model->nomQuestionnaire($idquestionnaire);
			$tabquestions = $this->questionnaire_model->recupererQuestionsdunQuestionnaire($idquestionnaire);

			foreach ($tabquestions as $key => $q) {
				$tabquestions[$key]['nbanswers'] = $this->question_model->getNbReponsesLibre($q['QU_id']) + $this->question_model->getNbReponsesQCM($q['QU_id']) ;
			}
		
			$data = array('idQT' => idToToken($idquestionnaire), 'nomQT'=> $nomQT,  'questions' => $tabquestions);
		
			if($this->input->post('ajax')) {
				$this->load->view('espacemembre/gestionQuestionsdeQuestionnaires.ajax.php', $data);
			}
			else {
				$this->_title = $this->lang->line('questionnaire_manage');
				$this->_render(array('espacemembre/gestionQuestionsdeQuestionnaires.php' => $data));
			}
		}
		else{
		
			if($this->input->post('ajax')) {
				$this->load->view('errors/error_accessright');
			}
			else {
				$this->_render(array('errors/error_accessright' => ''));
			}
		}
	}

//------------------------------------------------------------------------------------------------------------

	public function modifierNom($idquestionnaire)
	{
		$this->load->helper('url_token');
		$idquestionnaire = tokenToId($idquestionnaire);
		if($this->autorise($idquestionnaire)){

			$this->load->library('form_validation');
			//règles de validation
			$this->form_validation->set_rules('nomQuestionnaire', $this->lang->line('questionnaire_field_name') ,$this->_rules_nomquestionnaire);
		
			if($this->form_validation->run()){

				$nouveaunom = $this->input->post('nomQuestionnaire');

				$this->questionnaire_model->modifierNomQuestionnaire($nouveaunom, $idquestionnaire);
			    if($this->input->post('ajax')) redirect('mesQuestionnaires');
				else redirect('gererQuestionnaire/'.idToToken($idquestionnaire));
			}
			else{
				show_error('Le nom de votre questionnaire est incorrect');
			}

		}
		else{
			$this->_render(array('errors/error_accessright' => ''));
		}
	}
	

//------------------------------------------------------------------------------------------------------------
	
	
	// TODO verification formulaire
	public function changeOrder($idquestionnaire)
	{
		$this->load->helper('url_token');
		$idquestionnaire = tokenToId($idquestionnaire);
		
		if($this->autorise($idquestionnaire)){

			$this->load->library('form_validation');
			//règles de validation
			$this->form_validation->set_rules('quId[]', $this->lang->line('questionnaire_field_name') ,$this->_rules_string);
			
			if($this->form_validation->run()){
				$quId = Array();
				
				foreach($this->input->post('quId') as $quToken) {
					$quId[] = tokenToId($quToken);
				}
				
				$this->questionnaire_model->changeOrder($idquestionnaire, $quId);
			    if($this->input->post('ajax')) {
					$this->load->view('espacemembre/confirmRequest', array('accessrights' => true));
				}
				else {
					redirect('gererQuestionnaire/'.idToToken($idquestionnaire));
				}
			}
			else {
				if($this->input->post('ajax')) {
					$this->load->view('espacemembre/confirmRequest', array('accessrights' => false));
				}
				else {
					redirect('gererQuestionnaire/'.idToToken($idquestionnaire));
				}
			}

		}
		else {
			if($this->input->post('ajax')) {
				$this->_render(array('errors/error_accessright' => ''));
			}
			else {
				$this->load->view('espacemembre/confirmRequest', array('accessrights' => false));
			}
		}
	}


//------------------------------------------------------------------------------------------------------------	
	
	public function nouveauQuestionnaire()
	{
			$this->load->library('form_validation');
			//règles de validation
			$this->form_validation->set_rules('nomQuestionnaire', $this->lang->line('questionnaire_field_name') ,$this->_rules_nomquestionnaire);
			

			// si ca roule on crée le nouveau questionnaire
			if($this->form_validation->run()){
			
				// l'id du demandeur (avec la session)
				$iddemandeur= $this->encrypt->decode($this->session->userdata('iddem_code'));
				if($iddemandeur != ""){
					//On recupere le nom du questionnaire a ajouter
					$nom = $this->input->post('nomQuestionnaire');
					$this->load->helper('url_token');
					$idQT= idToToken($this->questionnaire_model->creerQuestionnaire($nom, $iddemandeur)) ;
					
					if($this->input->post('js')) redirect('mesQuestionnaires/1');
					else redirect('gererQuestionnaire/' . $idQT);
				}
				else{
					$this->_render(array('errors/error_accessright' => ''));
				}
			}

			// sinon on recharge la page en signalant l'erreur (code index() demandeur.php)
			else
			{

				// si un anonyme tente d'acceder a la gestion de ses questionnaires
				if($this->session->userdata('iddem_code') !=""){
				
					//on recupere l'id de l'user connectÃ©
					$iddemandeur= $this->encrypt->decode($this->session->userdata('iddem_code'));
					// on recupere dans tabidquestionnaire tout les questionnaires associÃ© a ce demandeur
					$tabidquestionnaire= $this->questionnaire_model->recupererQuestionnaires($iddemandeur);	

			       // si on ne ramasse aucun questionnaire le signaler proposer que d'en crÃ©er un nouveau
					if(empty($tabidquestionnaire)){ 
			 			$this->_render(array('espacemembre/formCreerQuestionnaire' => array('iddemandeur' => $iddemandeur)));
						return;
					}

					// on recupere les noms de ceux ci
					$nomquestionnaires = array();
					$nbQuestionquestionnaire = array();
					foreach ($tabidquestionnaire as $id) {
						$res = $this->questionnaire_model->nomQuestionnaire($id);
						$nomquestionnaires[] = $res;
						//$nbQuestionquestionnaire[] =  $this->questionnaire_model->recupererQuestionsdunQuestionnaire
						$nbQuestionquestionnaire[] = count($this->questionnaire_model->recupererQuestionsdunQuestionnaire($id));
					}
					$data = array('errors'=>validation_errors(), 'nomQuestionnaire' => $nomquestionnaires, 'idquestionnaire'=> $tabidquestionnaire, 'iddemandeur'=> $iddemandeur, 'nbquestionQuestionnaire'=>$nbQuestionquestionnaire);
					$this->_render(array('espacemembre/interfaceDemandeurGestionQuestionnaire' => $data));
				}
				else{
					$this->_render(array('errors/error_accessright' => ''));
				}			
			}


	}


//------------------------------------------------------------------------------------------------------------	

/**
 * Supprime un questionnaire selectionné par l'user
 * @param  [int] $idquestionnaire [l'id du questionnaire a supprimer]
 */
	public function supprimerQuestionnaire($idquestionnaire)
	{
		$this->load->helper('url_token');
		$idquestionnaire = tokenToId($idquestionnaire);
		if($this->autorise($idquestionnaire)){
		    $this->questionnaire_model->supprimerQuestionnaire($idquestionnaire);
			if($this->input->post('ajax')) {
				$this->load->view('espacemembre/confirmRequest', array('accessrights' => true));
			}
			else redirect('mesQuestionnaires');
		}
		else {
			if($this->input->post('ajax')) {
				$this->load->view('espacemembre/confirmRequest', array('accessrights' => false));
			}
			else {
				$this->_render(array('errors/error_accessright' => ''));
			}
		}
	}


//------------------------------------------------------------------------------------------------------------

/**
 * repondre
 *
 * Permet de répondre à un questionnaire
 *
 *
 * @access	public
 * @param   $idquestionnaire
 */

public function repondre($idquestionnaire)
{

	$this->load->helper('url_token');
	$idquestionnaire = tokenToId($idquestionnaire);

	$this->load->model('quizz/question_model');
	$this->load->model('quizz/reponse_model');

	$tabquestions = $this->questionnaire_model->recupererQuestionsdunQuestionnaire($idquestionnaire);
	//on récupère les propositions
	foreach ($tabquestions as $key => $value) {
		if($value['QU_type_id'] == 3)
		{
			$arrayPurposals[] = array(); //tableau vide si champ libre
		}
		else
		{
			$arrayPurposals[] = $this->question_model->getProposals($value['QU_id']);

		}
	}

	//si on a bien la même taille de tableau
	if( count(array_keys($tabquestions)) == count(array_keys($arrayPurposals)))
	{

		$data['questions'] = $tabquestions;
		$data['purposals'] = $arrayPurposals;
		$nomQT = $this->questionnaire_model->nomQuestionnaire($idquestionnaire);
		$data['idQT'] = idToToken($idquestionnaire); 

		$data['nomQT']= $nomQT;		

		$this->load->helper('cookie');

		//vérification réponse questionnaire
		if($this->encrypt->decode(get_cookie('rQtId')) == $idquestionnaire)
		{
			$error = $this->lang->line('questionnaire_already_done');
		}
		else
		{
			//si on a envoyé le formulaire
			if($this->input->post('Envoyer'))
			{
				$nbrepTotal = 0;

				if( ($this->input->post('optionVal') && $this->input->post('optionId')))
				{
						
					//vérification manuelle (pas possible d'appliquer les set rules sur un tableau 2 dim
					if(count(array_keys($_POST['optionVal'])) != count(array_keys($_POST['optionId']))) show_error('Une erreur est survenue');


					//compteur QuestLibres
					$count_questLibre = 0;
					if(isset($_POST['repQuestion']))
					{
						$count_questLibre = count(array_keys($_POST['repQuestion']));
					}

					//compteur QCM
					$counter_questCM = 0;
					if(isset($_POST['optionVal']))
					{
						$counter_questCM = count(array_keys($_POST['optionVal']));
					}
					if( ( $counter_questCM + $count_questLibre) != count(array_keys($_POST['qId']))) show_error('Error');



					//pas de valeurs != 0 ou 1 dans $_POST('optionVal')
					foreach ($_POST['optionVal']  as $k => $v) {
						foreach ($v as $key => $value) {
							if(! is_numeric($value)) show_error('Une erreur est survenue');

							
							if($value == 1){
								$nbrepTotal++;
								if($this->reponse_model->repSingleQCM($_POST['qId'][$k],$this->encrypt->decode($_POST['optionId'][$k][$key])) == FALSE)
									$error = $this->lang->line('questionnaire_error_submit');

							}
						}
					}
				}	
				
				if($this->input->post('repQuestion'))
				{
					

					foreach ($_POST['repQuestion'] as $key => $value) {
							if($value != '')
							{
								$nbrepTotal++;
								if($this->reponse_model->repSingleQLibre($_POST['qId'][$key],htmlspecialchars($_POST['repQuestion'][$key])) == FALSE)
									$error = $this->lang->line('questionnaire_error_submit');								
							}
					}
							
				}

					if($nbrepTotal > 0){
						$this->load->helper('cookie');
						
						//cookie pour éviter qu'il ne réponse 2 fois au questionnaire
						$cookie = array(
			                   'name'   => 'rQtId',
			                   'value'  => $this->encrypt->encode($idquestionnaire),
			                   'expire' => '86500'
			               );
						set_cookie($cookie);		

						$data['success'] = TRUE;
					}
					else
					{
						$error =$this->lang->line('questionnaire_no_answer');
					}
					
			}
			else
			{
				if(! empty($_POST)){
					$error = $this->lang->line('questionnaire_error_submit');
				}
				
			}			
		}

	
		
		if(isset($error)) $data['error'] = $error;
		
		$this->_render(array($this->load->view('espacemembre/rep_questionnaire', $data, TRUE)));

	}
	else
	{
		show_error($this->lang->line('questionnaire_error_results'));
	}
	
}



//------------------------------------------------------------------------------------------------------------

/**
 * QrCode
 *
 * renvoie un QrCode du questionnaire
 *
 *
 * @access	public
 * @param   $idqestionnaire : integer
 */

	public function QrCode($tokenquestionnaire)
	{
		$this->load->helper('url_token');

		$idquestionnaire = tokenToId($tokenquestionnaire);

		if($this->autorise($idquestionnaire)){
			$img = 'qrcodes/qt_'.$tokenquestionnaire.'.jpg';
			
			if(!file_exists($img)) {
				// fichier a creer localement
				$this->load->library('gc_qrcode');
				$this->gc_qrcode->size(547)
						->data(base_url().'qt/'.$this->uri->segment(2))
						->output_encoding('UTF-8')
						->error_correction_level('L')
						->margin(0);

				file_put_contents($img, file_get_contents($this->gc_qrcode->url()));
			}
			if($this->input->post('ajax')) {
				$this->load->view('espacemembre/share_modal', array('idQt' => $tokenquestionnaire, 'img' => $img ));
			}
			else $this->_render(array('espacemembre/qrcode_render' => array('idQt'=>$tokenquestionnaire, 'img'=> $img )));
			
		}
		else{
			$this->_render(array('errors/error_accessright' => ''));
		}

	}




//------------------------------------------------------------------------------------------------------------

/**
 * Exporter(télécharger) questionnaire
 *
 * exporte un questionnaire au format xml
 *
 *
 * @access	public
 * @param   fichier xml
 */

	public function exporter($idquestionnaire)
	{
		$this->load->model('questionnaire_model');
		$this->load->model('question_model');
		
		$this->load->helper('file_helper');
		$this->load->helper('xml');


		$this->load->helper('url_token');
		$idquestionnaire = tokenToId($idquestionnaire);
		if($this->autorise($idquestionnaire)){
			$titre = $this->questionnaire_model->nomQuestionnaire($idquestionnaire);

			$_array_questions = $this->questionnaire_model->recupererQuestionsdunQuestionnaire($idquestionnaire);

			$chaine = 
			'<?xml version="1.0" encoding="UTF-8"?>' .'
		'.'<questionnaire>'.' 
			'.'<label>' . xml_convert($titre)  . '</label>';

			foreach ($_array_questions as $question){
				$labelQuestion = $question['QU_label'];
				$maxtimeQuestion = $question['QU_max_time'];
				$typeQuestion = $question['QU_type_id'];
				$fermeQuestion = $question['QU_fermee'];
				$ordreQuestion = $question['QU_ordre'];
				$idQuestion = $question['QU_id'];
				$_reponses_question = $this->question_model->getIdAnswers($idQuestion);

				$string_rep = '';

				if($_reponses_question){
				
					$string_rep = $string_rep . '<reponses_proposees>';

					foreach ($_reponses_question as $reponse){
						$labelReponse = $this->question_model->getLabelAnswers($reponse);
						$correct = $this->question_model->getCorrectAnswers($reponse);
						$string_rep = $string_rep . 
		    				'<reponse>'. 
		    					'<label>' . xml_convert($labelReponse) . ' </label> '.'
		    					'.'<correct>'. xml_convert($correct) . '</correct>'.'


		    				'.'</reponse>'.'
							';
					}

					$string_rep = $string_rep . '</reponses_proposees>';
				}

	    		$chaine = $chaine . 
	    		'<question>'.'
					'.'<label>' . xml_convert($labelQuestion) . '</label>'.'
					'.'<max_time>' . xml_convert($maxtimeQuestion) . '</max_time>'.'
					'.'<type>' . xml_convert($typeQuestion) . '</type>'.'
					'.'<ferme>' . xml_convert($fermeQuestion) . '</ferme>'.'
					'.'<ordre>' . xml_convert($ordreQuestion) . '</ordre>'.'
					'.$string_rep.'
					'.'
				'.'</question>'.'
				';
			}

			$chaine = $chaine .'
			'.'</questionnaire>';

			write_file('./questionnairesxml/'.$titre.'_'.idToToken($idquestionnaire).'.xml', $chaine);

			$this->load->helper('download');
			$data = file_get_contents('./questionnairesxml/'.$titre.'_'.idToToken($idquestionnaire).'.xml'); // Read the file's contents
			$name = $titre.'.xml';
			force_download($name, $data);
		}else{
			$this->_render(array('errors/error_accessright' => ''));
		}

	}

//------------------------------------------------------------------------------------------------------------



	public function stats($idquestionnaire)
	{
		$this->load->helper('url_token');
		$idquestionnaire = tokenToId($idquestionnaire);
		if($this->autorise($idquestionnaire)){
			
			$this->load->helper('url');
			$this->load->library('gcharts');
	        $this->gcharts->load('PieChart');
	        $this->load->library('../controllers/quizz/question');

			//on recupere les questions
			$tabquestions = $this->questionnaire_model->recupererQuestionsdunQuestionnaire($idquestionnaire);
			
			$this->load->view('template/header');
			$this->load->view('template/navbar', array( 'title' =>$this->_title ));
			foreach ($tabquestions as $q) {
				$id_question = $q['QU_id'];
				$this->question->statsview(idToToken($id_question));
			}
			$this->load->view('template/footer');
		}else{
			$this->_render(array('errors/error_accessright' => ''));
		}
		
	}



//------------------------------------------------------------------------------------------------------------

	public function effacerResultat($tokenquestionnaire)
	{

		$this->load->helper('url_token');
		$idquestionnaire = tokenToId($tokenquestionnaire);
		if($this->autorise($idquestionnaire)){
			

			$this->load->model('quizz/question_model');

			//on recupere les questions et on efface les résultats de chacun d'eux
			$tabquestions = $this->questionnaire_model->recupererQuestionsdunQuestionnaire($idquestionnaire);
			foreach ($tabquestions as $q) {
				$id_question = $q['QU_id'];

				if($this->question_model->questionEditRights($id_question))
				{
					//suppression bdd
					if($this->question_model->eraseResults($id_question))
						echo $this->lang->line('result_erase'); //Todo
				}
				else
				{
					show_error($this->lang->line('error_erase_result'));
				}
			}
			if($this->input->post('ajax')) {
				$this->load->view('espacemembre/confirmRequest', array('accessrights' => true));
			}
			else redirect('gererQuestionnaire/' . $tokenquestionnaire);
		}
		else {
			if($this->input->post('ajax')) {
				$this->load->view('espacemembre/confirmRequest', array('accessrights' => false));
			}
			else {
				$this->_render(array('errors/error_accessright' => ''));
			}
		}
		
	}



/*********************************************************
* fonction qui prend en paramètre un id correspondant 
* à une sorte de one-click question
* 1: True/False
* 2: More/Less
* 3: Question avec 3 propositions (A,B,C)
* 4: Question avec 4 propositions (A,B,C,D)
*
*  return: l'identifiant de la one-click question voulue
*/
 
public function idOneClickQuestion($id)
{
	$this->load->model('quizz/question_model');

	if($id==1){
		return $this->question_model->addQuickQuestionCM();
	}
	if($id==2){
		return $this->question_model->addQuickQuestionMoreLess();
	}
	if($id=3){
		return $this->question_model->addQuickQuestionCM(3);
	}
	if($id=4){
		return $this->question_model->addQuickQuestionCM(4);
	}
	else{
		return FALSE;
	}
}



}
/* End of file questionnaire.php */
/* Location: ./application/controllers/questionnaire.php */