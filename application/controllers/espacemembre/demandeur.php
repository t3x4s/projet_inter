<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Demandeur extends MY_Controller {


	public function __construct()
	{
		parent::__construct();
			$this->load->helper('url_token');
			$this->load->model('espacemembre/questionnaire_model');
			$this->load->language('questionnaire');
			$this->load->library('form_validation');
	}


	// INTERFACE DE GESTION DES QUESTIONNAIRES DE L'USER CONNECTE
	// affiche tout les questionnaires du demandeur
	public function manageQuizzes($loadFirst = FALSE)
	{

		// si un anonyme tente d'acceder a la gestion de ses questionnaires
		if($this->session->userdata('iddem_code') !=""){
		
			//on recupere l'id de l'user connecté
			$iddemandeur= $this->encrypt->decode($this->session->userdata('iddem_code'));

			// on recupere dans tabidquestionnaire tous les questionnaires associÃ© a ce demandeur
			$tabidquestionnaire= $this->questionnaire_model->recupererQuestionnaires($iddemandeur);
			$tabTokenQT = array();
			foreach ($tabidquestionnaire as $id) {
				$tabTokenQT[] = idToToken($id);
			}
			
			// on recupere les noms de ceux ci
			$nomquestionnaires = array();
			$nbQuestionquestionnaire = array();
			foreach ($tabidquestionnaire as $id) {
				$nomquestionnaires[] = $this->questionnaire_model->nomQuestionnaire($id);
				$nbQuestionquestionnaire[] = count($this->questionnaire_model->recupererQuestionsdunQuestionnaire($id));
			}

			$data = array('nomQuestionnaire' => $nomquestionnaires, 'idquestionnaire'=> $tabTokenQT, 'iddemandeur'=> $iddemandeur, 'nbquestionQuestionnaire'=>$nbQuestionquestionnaire, 'loadFirst' => $loadFirst);
			$this->_render(array('espacemembre/interfaceDemandeurGestionQuestionnaire' => $data));
		
		}
		else{
			$this->_render(array('errors/error_accessright' => ''));
		}


	}

}

/* End of file demandeur.php */
/* Location: ./application/controllers/demandeur.php */