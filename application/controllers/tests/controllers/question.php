<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* TestQuestion
*/
class Question extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		


		$this->load->helper('url');
        $this->load->library('gcharts');
  
        $this->gcharts->load('PieChart');

        $slice1 = rand(0,40);
        $slice2 = rand(0,40);
        $slice3 = rand(0,40);
        $slice4 = rand(0,40);

        $this->gcharts->DataTable('Foods')
                      ->addColumn('string', '22s', 'food')
                      ->addColumn('string', 'Amount', 'amount')
                      ->addRow(array('Pizza', $slice1))
                      ->addRow(array('Beer', $slice2))
                      ->addRow(array('Steak', $slice3))
                      ->addRow(array('Bacon', $slice4));

        $config = array(
            'title' => 'My Foods',
            'is3D' => TRUE
        );

        $this->gcharts->PieChart('Foods')->setConfig($config);

       $this->_render(array($this->load->view('gcharts/pie_chart_basic','',TRUE),TRUE));


	}
}

/* End of file question.php */
/* Location: ./application/controllers/tests/controllers/question.php */