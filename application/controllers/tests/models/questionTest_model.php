<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* test model
*/
class QuestionTest_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
}

/* End of file questionTest_model.php */
/* Location: ./application/controllers/tests/models/questionTest_model.php */