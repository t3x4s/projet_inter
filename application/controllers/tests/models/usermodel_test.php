<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Test du modèle user
*/
class usermodel_test extends MY_Controller
{
	


	function __construct()
	{
		parent::__construct();

		//load unit_test lib
		$this->load->library('unit_test');
	}

	function index()
	{
		$this->_init();


		$this->add_Member_Test();

		echo $this->unit->report();
	}

	
	/*
	* @init
	*/
	function _init()
	{
		$this->load->model('user/user_model');
	}

	/**
	 * add_Member_Test
	 */
	function add_Member_Test()
	{
		$this->unit->run(
			$result = $this->user_model->add_Member('123456'),
			$expected = FALSE,
			'add_Member_Test : pas de valeurs POST'
		);		
	}


}



/* End of file userModel_test.php */
/* Location: ./application/controllers/tests/models/userModel_test.php */