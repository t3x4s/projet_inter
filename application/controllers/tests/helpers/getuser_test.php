<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Test helper getUser
*/
class getuser_test extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();

		//load unit_test lib
		$this->load->library('unit_test');
	}



	//Test display
	function index()
	{
		$this->_init();

		$this->testLookUp();

		$this->testNumericsWrongMail();

		$this->testNumericsCorrectMail();
		//Show reports
		echo $this->unit->report();
	}


	/*
	* @init
	*/
	function _init()
	{
		$this->load->helper('user/get_user_helper');
		
	}	


	/**
	 * testLookUp
	 */
	function testLookUp()
	{
		$array = array(
			'e_code' => $this->encrypt->encode('test')
		);
		
		$this->session->set_userdata( $array );	
		$this->unit->run(
			$result = getUser('',TRUE),
			$expected = 'test'
		);
	}

	/**
	 * testNumericsWrongMail
	 */
	function testNumericsWrongMail()
	{
		$array = array(
			'e_code' => $this->encrypt->encode('t@hotmail.fr')
		);
		
		$this->session->set_userdata( $array );

		$this->unit->run(
			$result = getUser(TRUE),
			$expected = 0,
			'getUser(TRUE)'
		);

		$this->unit->run(
			$result = getUser(),
			$expected = FALSE,
			'getUser(FALSE,FALSE)'
		);
	}

	/**
	 * testNumericsCorrectMail
	 */
	function testNumericsCorrectMail()
	{
		$array = array(
			'e_code' => $this->encrypt->encode('membre5@univ-rennes1.fr')
		);
		
		$this->session->set_userdata( $array );	

		$this->unit->run(
			$result = getUser(TRUE),
			$expected = 5,
			'getUser(TRUE)'
		);

		$this->unit->run(
			$result = getUser(),
			$expected = 5,
			'getUser(FALSE,FALSE)'
		);
	}
}

/* End of file getuser_test.php */
/* Location: ./application/controllers/tests/helpers/getuser_test.php */