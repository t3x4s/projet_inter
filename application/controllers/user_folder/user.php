<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
TODO :
	- Adapter la langue
*/




	class User extends MY_Controller
	{

	//register rules
		private $rules_password = 'trim|htmlspecialchars|required|min_length[6]|max_length[50]|xss_clean|prep_for_form|encode_php_tags';
		
		private $rules_password_confirm = 'trim|htmlspecialchars|required|matches[pass]|xss_clean|prep_for_form|encode_php_tags';
		private $rules_email = 'trim|htmlspecialchars|required|max_length[50]|is_unique[t_membre_me.ME_mail]|valid_email|xss_clean|prep_for_form|encode_php_tags';	
		private $rules_login = 'trim|htmlspecialchars|required|xss_clean|prep_for_form|encode_php_tags|min_length[3]|max_length[32]|is_unique[t_membre_me.ME_login]';
		private $rules_prof = 'trim|htmlspecialchars|required|xss_clean|prep_for_form|encode_php_tags|exact_length[1]|numeric|integer';
		
	//login rules
		private $rules_password_login = 'trim|htmlspecialchars|required|max_length[50]|xss_clean|prep_for_form|encode_php_tags';
		private $rules_input_login = 'trim|htmlspecialchars|required|max_length[50]||xss_clean|prep_for_form|encode_php_tags';

	//change password rules
		private $rules_password_change = 'trim|htmlspecialchars|required|min_length[6]|max_length[50]|xss_clean|prep_for_form|encode_php_tags|callback_check_old_pw';

	//captcha rules
		private $rules_captcha = 'trim|htmlspecialchars|required|max_length[10]|xss_clean|prep_for_form|encode_php_tags|callback_check_captcha|numeric';


	//Constructeur
	public function __construct()
	{
		parent::__construct(); 

		$this->load->model('user/user_model'); //charge modèle utilisateur	

		//helpers
		$this->load->helper('user/check_connect');

		//langue
		$this->load->language('user');
	}



	//contrôleur index
	public function index()
	{
		$this->load->language('home');
		$this->_render(array('home/home_page' => ''));

		

	}

//------------------------------------------------------------------------------------------------------------

	public function switchLang($id_lang)
	{

		$lang = array('lang' =>'french') ;
		

		switch ($id_lang) {
			
			case 1:
				$lang = array('lang' =>'french') ;
       			break;

			case 2:
				$lang = array('lang' =>'english') ;
				break;
			
			default:
				break;
		}
		$this->session->set_userdata($lang);

		      redirect($_SERVER['HTTP_REFERER']);
	}


//------------------------------------------------------------------------------------------------------------

/**
 *	Register
 * 
 *
 * @access	public
 */

public function register()
{
		//Appel aux helpers
	$this->load->helper(array('form','email','captcha'));

		//librairies
	$this->load->library(array('email','form_validation'));


		//si on a une session active
	if($this->session->userdata('e_code'))
	{
		redirect('index');
	}

		//validation rules
	$this->form_validation->set_rules('email',$this->lang->line('validation_email'),$this->rules_email);
	$this->form_validation->set_rules('pass',$this->lang->line('validation_pass'),$this->rules_password);
	$this->form_validation->set_rules('pass2',$this->lang->line('validation_pass2'),$this->rules_password_confirm);
	$this->form_validation->set_rules('login',$this->lang->line('validation_login'),$this->rules_login);
	$this->form_validation->set_rules('captcha',$this->lang->line('validation_captcha'),$this->rules_captcha);

		//validation check
	if ($this->form_validation->run())
	{

		$code = md5(uniqid(rand(), true)); //clé d'activation
		

		//Model call
		if($this->user_model->add_Member($code) == TRUE) //transmet au modèle tous les paramètres			
		{

			$code_email = $this->encrypt->encode($this->input->post('email'));
			$content=	$this->lang->line('email_intro').'
						{unwrap} <a href="'.base_url().'email_validation/'.$code_email.'/'.$code.'">'
						.base_url().'email_validation/'.$code_email.'/'.$code.'</a> {/unwrap}';

			//Email sending
			$this->email->from('inter@legtux.org',$this->lang->line('email_from'));
			$this->email->to($this->input->post('email'),$this->lang->line('email_register_title'));
			$this->email->subject($this->lang->line('email_register_subject'));
			$this->email->message($content);	
			
			//vérification envoi email
			if($this->email->send())
			{
				//ajout d'une question 
				$this->load->model('quizz/question_model');

				//ajout d'une question
				$OneClickQuestion= array($this->question_model->addQuickQuestionCM(), //pas de valeur de retour nécessaire
				$this->question_model->addQuickQuestionCM(3), //3 réponses
				$this->question_model->addQuickQuestionCM(4), //4 réponses
				$this->question_model->addQuickQuestionMoreLess());//ajoute one click +/-

				//redirect to sucess page
				$data['sucess_desc'] = $this->lang->line('success_desc_register');
				$data['redirection_message'] = $this->lang->line('success_redirect');
				$data['message_code'] = $this->lang->line('success_info');

				$this->_render(array($this->load->view('user/sucess',$data, TRUE)));
			}
			else
			{
				show_error($this->lang->line('error_send_email'));
			}			

		}
	    else 
	    {
	    	show_error($this->lang->line('error_add_member'));
	    }

	}
	else
	{

		//création du captcha
		$tab_captcha = array(
			'word'=> random_string('numeric', 6),
			'img_path'	 => './captcha/',
			'img_url'	 => base_url().'captcha/',
			'img_width'     => '120',
			'img_height' => 30,
			'expiration' => 60
			);
		
		//test création captcha
		if( ($captcha = create_captcha($tab_captcha)) == FALSE)
		{
	    	show_error($this->lang->line('error_create_captcha'));
		}

		//captcha en session
		if($this->session->userdata('c_val'))
		{
			$this->session->unset_userdata('c_val');
		}
		$this->session->set_userdata(array('c_val'=>$this->encrypt->encode($captcha['word'])));
		//donnees captcha envoi vue
		$data = array('captcha'=>$captcha['image'],'errors'=>validation_errors());


		$this->_render(array($this->load->view('user/register',$data,TRUE)));

	}
}

//------------------------------------------------------------------------------------------------------------

/**
 * Fonction login
 *
 * 1 - Check if existing session = redirect() 
 * 2 - Check form validation
 * 3 - if ok : model call -> session created
 *
 * @access	public
 * @param   - 
 * @return  -
 */

public function login()
{
	//Appel aux helpers
	$this->load->helper(array('captcha','form'));

	//librairies
	$this->load->library(array('email','form_validation'));


	//redirection si connecté
	if($this->session->userdata('e_code'))
	{
		redirect(site_url());
	}

	//validation
	$this->form_validation->set_rules('login',$this->lang->line('validation_login_or_email'),$this->rules_input_login);
	$this->form_validation->set_rules('password',$this->lang->line('validation_pass'),$this->rules_password_login);

	if($this->form_validation->run())
	{

		//test création de session
		if($this->user_model->auth_Member() && $this->session->userdata('e_code'))
		{

			/*
			//redirect to sucess page
			$data['sucess_desc'] = $this->lang->line('sucess_desc_login');
			$data['redirection_message'] = $this->lang->line('success_redirect');
			$data['message_code'] = $this->lang->line('success_info');
			$this->_render(array($this->load->view('user/sucess',$data, TRUE)));
			*/
			redirect('mesQuestionnaires');

		}
		else
		{	
			// affichage de l'erreur
			
			$this->_render( array( 'user/login' =>  array('error_login' => ''))) ;

		}
			
	}
	else
	{

		

		//donnees envoi vue
		$data = array('errors'=>validation_errors());

		
		$this->_render(array($this->load->view('user/login',$data,true)));

		
	}

}

//------------------------------------------------------------------------------------------------------------

/**
 * check_logs function
 * 
 * Callback function : form_validation rule
 *
 * Results checked (in order) : 
 *	- Existing email
 *  - Activation code is OK (=1)
 *	- Check password
 * 
 * 	Return bool + String message
 *
 * @access	public
 * @param 	string : email
 * @return 	bool
 */

public function check_logs($email)
{

		//data request
	$data = $this->_db	->select('ME_id')
	->select('ME_password')
	->from('t_membre_me')
	->where('ME_mail',$email)
	->get()->row_array();




	if(!empty($data)) 
	{
		if($data['ME_password'] ==sha1($this->input->post('password')))
		{
			if($this->user_model->isMemberActivated($this->input->post('login')) == TRUE)
			{
				return true;
			}
			else
			{
				$this->form_validation->set_message('check_logs',$this->lang->line('user_not_activated'));
				return false;
			}
		} 
		else
		{
			$this->form_validation->set_message('check_logs',$this->lang->line('incorrect_password'));
			return false;
		}

	}
	else
	{
		$this->form_validation->set_message('check_logs',$this->lang->line('unknown_user'));
		return false;
	}

}


//------------------------------------------------------------------------------------------------------------

/**
 * check_captcha fonction
 *
 * Fonction de callback qui vérifie que le captcha est correct
 *
 *
 * @access	public
 * @param   captcha : string
 * @return  bool : res
 */

public function check_captcha()
{
	if($this->session->userdata('c_val'))
	{
		if($this->encrypt->decode($this->session->userdata('c_val')) == $this->input->post('captcha'))
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('check_captcha',$this->lang->line('incorrect_captcha'));
			return false;
		}
	}
	else
	{
		$this->form_validation->set_message('check_captcha',$this->lang->line('missing_captcha'));
		return false;
	}

}

//------------------------------------------------------------------------------------------------------------

/**
 * Email validation function
 *
 * Uri segment : /<email_encrypted>/<activation_code>
 * Decode the uri segment containing email
 * Model call with params
 *
 * @access	public
 * @param   mail_code : string 
 * @param   activation_code : string
 * @return  -
 */
public function email_validation($mail_code,$activation_code)
{

  	

	if(strlen($activation_code) > 32) //code modifié
	{

		$data['sucess_desc'] = $this->lang->line('error_email_validation');
		$data['redirection_message'] = $this->lang->line('success_redirect');
		$data['message_code'] = $this->lang->line('success_info');
	}
	else
	{

		//chargement du modèle
		$this->load->model('quizz/question_model');

		//Data retrieving
		$email = htmlspecialchars($this->encrypt->decode($mail_code));

		if($this->user_model->isMemberActivated($email) == TRUE)
		{
			//si déjà activé
			$data['message'] = 	$this->lang->line('active_account');
		}
		else
		{

			$res = $this->user_model->validate_member($email,$activation_code);

			if ($res == TRUE)
			{

				$data['message'] = $this->lang->line('email_validation_success');
			}
			else
			{
				if($this->user_model->isMemberActivated($email)  == TRUE) //si l 'activation n'a pas marchée
				{

					$data['message'] = $this->lang->line('email_validation_failure');
				}
			}
		}

		$data['sucess_desc'] = $data['message'];
		$data['redirection_message'] = $this->lang->line('success_redirect');
		$data['message_code'] = $this->lang->line('success_info');

   		//load view
		$this->_render(array($this->load->view('user/sucess',$data,true)));

	}
}

//------------------------------------------------------------------------------------------------------------

/**
 * Logout function
 *
 * unset : email, logged
 * redirection vers l'url de base
 *
 * @access	public
 * @param 	-
 * @return  -
 */
public function logout()
{


	$this->session->unset_userdata('e_code');

	$this->session->sess_destroy();

	redirect(site_url());
}


//------------------------------------------------------------------------------------------------------------

/**
 * Password change function
 *
 * Password change steps :
 * - 1 old password retieve
 * - 2 new password matchs
 *
 *
 * @access	public
 * @param 	-
 * @return 	-
 */
public function change_password()
{
	$this->load->helper('form');

		//librairie
	$this->load->library('form_validation');

	if(!check_connect()) redirect(site_url());

	//validation rules
	$this->form_validation->set_rules('old_password',$this->lang->line('validation_old_password'),$this->rules_password_change);
	$this->form_validation->set_rules('pass',$this->lang->line('validation_pass'),$this->rules_password);
	$this->form_validation->set_rules('pass2',$this->lang->line('validation_pass2'),$this->rules_password_confirm);

	if($this->form_validation->run())
	{
		$id_user = $this->encrypt->decode($this->session->userdata('iddem_code'));

		if(!$this->user_model->update_password(sha1($this->input->post('pass')),$id_user))
		{
			$this->_render(array('user/change_password' => array('update_error'=>TRUE)));
		}
		else
		{
			$data['sucess_desc'] = $this->lang->line('password_changed');
			$data['redirection_message'] = $this->lang->line('success_redirect');
			$data['message_code'] = $this->lang->line('success_info');

			$this->_render(array('user/sucess' => $data));
		}				
	}
	else
	{
		$data['errors'] = validation_errors();
		$this->_render(array('user/change_password' => $data));
	}

}
//------------------------------------------------------------------------------------------------------------

/**
 * Call back change password function
 *
 * Check if the value entered matches with the old password
 *
 * @access	public
 * @return  bool
 */

 public function check_old_pw()

 {
	//decrypt email in session data
	$email_user = $this->encrypt->decode($this->session->userdata('e_code')); 

	//get the data pass
	$query = $this->_db->get_where('t_membre_me', array('ME_mail' => $email_user,'ME_password'=>sha1($this->input->post('old_password'))));

 
	if($query->num_rows() == 0)

 	{

		$this->form_validation->set_message('check_old_pw',$this->lang->line('wrong_old_password'));
 		return false;
 	}
 	else
 	{
 		return true;
 	}
 }


//------------------------------------------------------------------------------------------------------------

/**
 * forgot_password
 *
 * Renvoie le mot de passe de l'utilisateur par email
 *
 *
 * @access	public
 * @param   -
 * @return  -
 */

public function forgot_password()
{

		//helpers
	$this->load->helper(array('captcha','form','email'));

		//librairies
	$this->load->library(array('email','form_validation'));

		//validation rules
	$this->form_validation->set_rules('email','Email','trim|htmlspecialchars|required|max_length[32]|valid_email|xss_clean|prep_for_form|encode_php_tags|callback_check_email');
	$this->form_validation->set_rules('captcha','Captcha',$this->rules_captcha);


	if($this->form_validation->run())
	{

			$code = md5(uniqid(rand(), true)); //helper string
			


			//Envoi d'email
			$this->email->from('inter@legtux.org',$this->lang->line('email_from'));
			$this->email->to($this->input->post('email'));
			$this->email->subject($this->lang->line('email_password_subject'));
			$this->email->message($this->lang->line('email_password_content').'
				{unwrap} <a href="'.base_url().'password_reinit/'.urlencode($this->encrypt->encode($this->input->post('email'))).'/'.$code.'">'
				.base_url().'password_reinit/'.urlencode($this->encrypt->encode($this->input->post('email'))).'/'.$code.' </a>  {/unwrap}
				<br/>');


    		//vérification envoi email
			if($this->email->send())
			{
				$data['sucess_desc'] = $this->lang->line('success_password_reinit');
				$data['redirection_message'] = $this->lang->line('success_redirect');
				$data['message_code'] =  $this->lang->line('success_info');

     			//view
				$this->_render(array($this->load->view('user/sucess',$data,true)));

			}
			else
			{
				show_error($this->lang->line('error_send_email'));
			}
		}
		else
		{
				//création du captcha
			$tab_captcha = array(
				'word'=> random_string('numeric', 6),
				'img_path'	 => './captcha/',
				'img_url'	 => base_url().'captcha/',
				'img_width'     => '120',
				'img_height' => 30,
				'expiration' => 60
				);

				//test vérification création captcha
			if( ($captcha = create_captcha($tab_captcha)) == FALSE)
			{
				show_error($this->lang->line('error_create_captcha'));
			}	

				//captcha en session
			if($this->session->userdata('c_val'))
			{	
				$this->session->unset_userdata('c_val');
			}
			$this->session->set_userdata(array('c_val'=>$this->encrypt->encode($captcha['word'])));

				//donnees envoi vue
			$data = array('captcha'=>$captcha['image'],'errors'=>validation_errors());
			
			$this->_render(array($this->load->view('user/forgot_password',$data,TRUE)));
			
			
		}

	}	


//------------------------------------------------------------------------------------------------------------

/**
 * password_reinit
 *
 * fonction qui réinitialise un mot de passe
 *
 *
 * @access	public
 * @param   email : string
 * @param   code : string
 * @return  
 */

	public function password_reinit($email,$code)
	{
		//load helper
		$this->load->helper(array('string','email'));

		//librarie
		$this->load->library('email');


		$email = rawurldecode($this->encrypt->decode($email));
		$code = htmlspecialchars($code);

  		if(strlen($code) != 32 ) //code modifié
  		{

  			$data['sucess_desc'] = $this->lang->line('error_password_reinit');
  			$data['redirection_message'] = $this->lang->line('success_redirect');
  			$data['message_code'] = $this->lang->line('success_info');

  			
  			$this->_render(array($this->load->view('user/sucess',$data,true)));

  		}
  		else
  		{

  			//on crée un nouveau mot de passe
  			$new_password = random_string(); //frandom_string helper string

  			//appel du modèle
  			if($this->user_model->update_password(sha1($new_password),$email) == FALSE)
  			{
  				show_error($this->lang->line('error_password_reinit'));
  			}


			//Envoi d'email
			$this->email->from('inter@legtux.org',$this->lang->line('email_from'));
			$this->email->to($email);
  			$this->email->subject($this->lang->line('email_password_subject'));
  			$this->email->message($this->lang->line('email_password_reinit').'
  				<p> Email : <b>'.$email.'</b> </p>
  				<p> Mot de passe : <b>'.$new_password.'</b></p>
  				<br/>');



    		//vérification envoi email
  			if($this->email->send())
  			{
  				$data['sucess_desc'] = $this->lang->line('new_pass');
	  			$data['redirection_message'] = $this->lang->line('success_redirect');
	  			$data['message_code'] = $this->lang->line('success_info');

  				$this->_render(array($this->load->view('user/sucess',$data,true)));
  			}
  			else
  			{
  				show_error($this->lang->line('error_send_email'));
  			}

  		}
  	}


//------------------------------------------------------------------------------------------------------------

/**
 * check_email
 *
 * callback permettant de vérifier qu'un email est existant dans la base de données
 *
 *
 * @access	public
 * @param   $email : string
 * @return  bool
 */

	public function check_email($email)
	{

		$data = $this->_db	->select('ME_id')
		->select('ME_password')
		->from('t_membre_me')
		->where('ME_mail',$email)
		->get()->row_array();

		
		if(!empty($data))
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('check_email',$this->lang->line('unknown_email'));
			return false;
		}

	}
}
/* End of file user.php */
/* Location: ./application/controllers/user.php */