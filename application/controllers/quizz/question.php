<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Classe question
*/
class Question extends MY_Controller
{
	

	//règles de validation
	private $_rules_numeric ='trim|htmlspecialchars|required|max_length[1]|xss_clean|less_than[2]|prep_for_form|encode_php_tags|numeric';
	private $_rules_string = 'trim|htmlspecialchars|required|max_length[300]|xss_clean|prep_for_form|encode_php_tags';
	private $_rules_questionnaire ='trim|htmlspecialchars|required|xss_clean|prep_for_form|encode_php_tags|numeric';


	//règles de validation edition
	private $_rules_numeric_edit ='trim|htmlspecialchars|max_length[1]|xss_clean|prep_for_form|encode_php_tags|numeric';
	private $_rules_numeric_edit_questionnaire ='trim|htmlspecialchars|xss_clean|prep_for_form|encode_php_tags|numeric';

	private $_rules_string_edit = 'trim|htmlspecialchars|required|max_length[100]|xss_clean|prep_for_form|encode_php_tags';

		//captcha rules

	private $rules_captcha = 'trim|htmlspecialchars|required|max_length[10]|xss_clean|prep_for_form|encode_php_tags|callback_check_captcha|numeric';


	//constructeur
	function __construct()
	{
		parent::__construct();
		$this->load->language('question');
		$this->load->language('errors');
	}



//------------------------------------------------------------------------------------------------------------

/**
 * create
 *
 * Permet de créer une question via un formulaire
 *
 *
 * @access	public
 */

	public function create($idquestionnaire)
	{

		//Appel aux helpers
		$this->load->helper(array('captcha','form'));
		
		$this->load->helper('user/check_connect_helper','form');
		$this->load->library('form_validation');
		$this->load->helper('url_token');

		$this->load->model('quizz/question_model');
		$this->load->model('espacemembre/questionnaire_model');

		//tableau types de question statique
		$options_question = $this->question_model->get_TypesQuestion();

		$defaultQuestionnaire = $idquestionnaire;
		if($idquestionnaire!=1){
			$defaultQuestionnaire = tokenToId($idquestionnaire);
		}

		// utilisateur connecté ou non ?
		$connected = $this->session->userdata('iddem_code') != "";

		//règles de validation
		$this->form_validation->set_rules('NomQuestion', 'Intitulé de la question',$this->_rules_string);
		$this->form_validation->set_rules('questionnaire','Questionnaire',$this->_rules_questionnaire);
		if(!$connected) $this->form_validation->set_rules('captcha',$this->lang->line('validation_captcha'),$this->rules_captcha);

		if($this->input->post('type_question') != 3)
		{
			$this->form_validation->set_rules('optionName[]','Intitulé de la proposition',$this->_rules_string);
		}

		//tableau des questionnaires 
		$arrayQuestionnaire = array('1'=>$this->lang->line('question_withouth_questionnaire'));

		if($this->form_validation->run())
		{

			if($res = $this->question_model->addQuestion($this->input->post('type_question')))
			{
				//on crée un cookie pour la connexion anonyme
				$this->load->helper('cookie');

				$cookie = array(
                   'name'   => 'qId',
                   'value'  => $this->encrypt->encode($res),
                   'expire' => '86500'
				);

				set_cookie($cookie);
				
				$img = 'qrcodes/q_'.$res.'.jpg';
				
				if(!file_exists($img)) {
					// fichier a creer localement
					$this->load->library('gc_qrcode');
					$this->gc_qrcode->size(547)
							->data(base_url().'q/'.$res)
							->output_encoding('UTF-8')
							->error_correction_level('L')
							->margin(0);
					
					file_put_contents($img, file_get_contents($this->gc_qrcode->url()));
				}
				
				$data = array('success'=>TRUE, 'idQuestion'=>$res, 'idQuestionnaire'=> idToToken($idquestionnaire), 'defaultQuestionnaire' => $defaultQuestionnaire, 'img' => $img);
				
				if($this->input->post('ajax')) {
					$this->load->view('quizz/manage_question_modal', $data);
				}
				else {
					if($defaultQuestionnaire == 1) {
						$this->_render(array('quizz/manage_question' => $data));
					}
					else redirect('gererQuestionnaire/'. idToToken($idquestionnaire));
				}
			}
			else
			{
				show_error('Une erreur s\'est produite (database)');
			}
		}
		else
		{
			$data = array('options_question' => $options_question,
						  'defaultQuestionnaire' => $defaultQuestionnaire,
  						  'connected' => $connected
						  );
						  
			//création du captcha
		$tab_captcha = array(
			'word'=> random_string('numeric', 6),
			'img_path'	 => './captcha/',
			'img_url'	 => base_url().'captcha/',
			'img_width'     => '120',
			'img_height' => 30,
			'expiration' => 60
			);
		
		//test création captcha
		if(!$captcha = create_captcha($tab_captcha))
		{
	    	show_error($this->lang->line('error_create_captcha'));
		}

		//captcha en session
		if($this->session->userdata('c_val'))
		{
			$this->session->unset_userdata('c_val');
		}
		$this->session->set_userdata(array('c_val'=>$this->encrypt->encode($captcha['word'])));
		//donnees captcha envoi vue
		$data += array('values'=>$this->input->post('optionName'),
								 'captcha'=>$captcha['image'],
								 'errors'=>validation_errors());
		
		
					  
						  
			if($this->input->post('ajax')) {
				$this->load->view('quizz/create_question', $data);
			}
			else {
				$this->_render(array('quizz/manage_question' => array('errors'=>validation_errors()),
									 'quizz/create_question' => $data
											));
			}
		}
		
	}



//------------------------------------------------------------------------------------------------------------

/**
 * edit
 *
 * Permet d'éditer une question
 *
 *
 * @access	public
 * @param   $id_question : int
 */

	public function edit ($idquestionnaire, $id_question)
	{
		
		
		$this->load->model('quizz/question_model');
		
		$this->load->helper('url_token');
		$id_question = tokenToId($id_question);
		

		if($res = $this->question_model->getQuestion($id_question))
		{
			if($this->question_model->questionEditRights($id_question))
			{
				$this->load->helper('form');

				$this->load->library('form_validation');



				//règles de validation
				$this->form_validation->set_rules('NomQuestion', 'Intitulé de la question',$this->_rules_string);
				$this->form_validation->set_rules('TypeQuestion', 'Intitulé de la question',$this->_rules_numeric);
				$this->form_validation->set_rules('questionnaire','Questionnaire',$this->_rules_numeric_edit_questionnaire);

				$this->form_validation->set_rules('optionName[]','Titre option',$this->_rules_string_edit);

				$data['infosQuestion'] = $res;
				$data['idqu'] = $idquestionnaire;

				if($this->form_validation->run())
				{
					//mise à jour des questions
					$data['success'] = $this->question_model->updateQuestion($res['QU_id']);
						
					$data['purposals'] = $this->question_model->getProposals($res['QU_id']);
					$data['infosQuestion'] = $res;
					$data['options_question'] = $this->question_model->get_TypesQuestion();


					$this->_render(array('quizz/edit_question' => $data));



				}
				else
				{

					if($res['QU_type_id'] != 3)
					{
						$data['purposals'] = $this->question_model->getProposals($res['QU_id']);
					}
					$data['options_question'] = $this->question_model->get_TypesQuestion();						
					$data['errors'] = validation_errors();

					$this->_render(array('quizz/edit_question' => $data));


				}


			}
			else
			{
				$this->_render(array('errors/error_accessright' => ''));
			}		
		}
		else
		{
			$this->_render(array('errors/error_bad_question_id' => ''));

		}
	}









//------------------------------------------------------------------------------------------------------------

/**
 * stats
 *
 * permet de connaître les statistiques d'une question
 *
 *
 * @access	public
 * @param   $id_question : integer
 */


	public function stats($id_question)
	{
		$this->load->view('template/header');
		$this->load->view('template/navbar', array( 'title' =>$this->_title ));
		$this->statsview($id_question);
		$this->load->view('template/footer');
	}
	
	public function statsview($id_question)
	{
		$this->load->helper('url');
		$this->load->helper('url_token');
        $this->load->library('gcharts');
        $this->gcharts->load('PieChart');

		$id_question = tokenToId($id_question);
		
		
		$this->load->model('quizz/question_model');

		if($res = $this->question_model->getTypeFromQuestion($id_question))
		{
			$label = $this->question_model->getQuestion($id_question);
			$titre = $label['QU_label'];
			$type = $label['QU_type_id'];

			
			
			if($res == 3){
			// Champ libre
				$rep = $this->question_model->getReponsesLibre($id_question);
				$nb = $this->question_model->getNbReponseslibre($id_question);
				
				$data = array('titre'=>$titre, 'nb'=>$nb, 'token' => idToToken($id_question));
				$this->load->view('quizz/statsbasic', $data);
       			$this->load->view('quizz/statslibre', array('label' => $titre, 'rep'=> $rep, 'nb'=>$nb));
			}
			else {
			// QCM - QCS
				$rep = $this->question_model->getReponsesQCM($id_question);
				$nb = $this->question_model->getNbReponsesQCM($id_question);
				$data = array('titre'=>$titre, 'nb'=>$nb, 'token' => idToToken($id_question));

				$DataTable = $this->gcharts->DataTable($titre)
								->addColumn('string', 'Col1', 'col1')
								->addColumn('string', 'Col2', 'col2');

				if($nb != 0){

					foreach($rep as $row => $value){
						$count = 0;
						foreach ($value as $key => $val) {

							if($count%2 == 0)
							{
								$text = $val;
							}
							else{
								$val_stat = (int) $val;
								
								$DataTable->addRow(array(html_entity_decode($text),$val_stat));
							}

							$count = ($count+1)%2;
						}
					}
				}
				$config = array(
					'title' => $titre,
					'is3D' => TRUE
				);


				$this->load->view('quizz/statsbasic', $data);
				$this->gcharts->PieChart($titre)->setConfig($config);
				$this->load->view('gcharts/pie_chart_basic', $data);
			}



		}else
		{
			//show_error('L\'identifiant de la question est incorrect (3)');
			?>
					<script type="text/javascript">
						alert($this->lang->line('error_wrong_identifier_question'));
					</script>
					<?php
		}
	}

//------------------------------------------------------------------------------------------------------------

/**
 * QrCode
 *
 * renvoie un QrCode de la question
 *
 *
 * @access	public
 * @param   $id_question : integer
 */

	public function QrCode($id_question, $id_questionnaire = false)
	{
		$this->load->helper('form');
		$this->load->library('gc_qrcode');
		
		$img = 'qrcodes/q_'.$id_question.'.jpg';
		
		if(!file_exists($img)) {
			// fichier a creer localement
			$this->load->library('gc_qrcode');
			$this->gc_qrcode->size(547)
					->data(base_url().'q/'.$id_question)
					->output_encoding('UTF-8')
					->error_correction_level('L')
					->margin(0);
			
			file_put_contents($img, file_get_contents($this->gc_qrcode->url()));
		}
		
		
		
		if($this->input->post('ajax')) {
			$this->load->view('quizz/manage_question_modal', array('idQuestion'=>$id_question, 'idQuestionnaire'=>$id_questionnaire, 'share' => true, 'img' => $img ));
		}
		else $this->_render(array('quizz/qrcode_render' => array('img' => $img, 'idQuestion'=>$id_question)));
	}




//------------------------------------------------------------------------------------------------------------

/**
 * effacerResultats
 *
 * Efface les résultats d'une question
 *
 *
 * @access	public
 * @param   $idquestion
 */

	public function effacerResultats($id_questionnaire, $id_question)
	{
		$this->load->helper('url_token');
		
		$id_question = tokenToId($id_question);
		
		//verif idQuestion
		if(! is_numeric($id_question)) show_error($this->lang->line('error_wrong_identifier_question'));

		$this->load->model('quizz/question_model');
		
		//vérification des droits
		if($this->question_model->questionEditRights($id_question))
		{
			//suppression bdd
			if($this->question_model->eraseResults($id_question)){
				if($this->input->post('ajax')) {
					$this->load->view('espacemembre/confirmRequest', array('accessrights' => true));
				}
				else redirect('gererQuestionnaire/'.$id_questionnaire);
			}
			else
			{
				$this->load->view('espacemembre/confirmRequest', array('accessrights' => false));
			}
		}
		else
		{
			$data['error'] = $this->lang->line('error_no_rights_erase');
			$this->_render(array('errors/error_generic' => $data));
		}
	

	}


//------------------------------------------------------------------------------------------------------------

/**
 * delete
 *
 * Supprime une question et ses résultats
 *
 *
 * @access	public
 * @param   $id_question : integer
 */

	public function delete ($idquestionnaire, $id_question)
	{
		$this->load->helper('url_token');
		$id_question = tokenToId($id_question);
		//verif idQuestion
		if(! is_numeric($id_question)) show_error($this->lang->line('error_wrong_identifier_question'));

		$this->load->model('quizz/question_model');

		//vérification des droits
		if($this->question_model->questionEditRights($id_question))
		{
			//suppression bdd
			if($this->question_model->deleteQuestion($id_question)){
				if($this->input->post('ajax')) {
					$this->load->view('espacemembre/confirmRequest', array('accessrights' => true));
				}
				else redirect('gererQuestionnaire/'.$idquestionnaire);
			}
			else
			{
				if($this->input->post('ajax')) {
					$this->load->view('espacemembre/confirmRequest', array('accessrights' => false));
				}
				else {
					show_error($this->lang->line('error_delete_question'));
				}
			}
		}
		else
		{
			$data['error'] = $this->lang->line('error_no_rights_delete');
			$this->_render(array('errors/error_generic' => $data));
		}
	}


/**
 * close
 *
 * Ferme une question -> on ne peut plus y répondre
 *
 *
 * @access	public
 * @param   $id_question : integer
 */

	public function close ($id_question)
	{
		//verif idQuestion
		if(! is_numeric($id_question)) show_error($this->lang->line('error_wrong_identifier_question'));

		$this->load->model('quizz/question_model');

		//vérification des droits
		if($this->question_model->questionEditRights($id_question))
		{
			if($this->question_model->closeQuestion($id_question)){
				echo $this->lang->line('question_close'); //Todo

			}
			else
			{
				$data['error'] = $this->lang->line('error_close_question');
				$this->_render(array('errors/error_generic' => $data));
			}
		}
		else
		{
			$data['error'] = $this->lang->line('error_no_rights_close');
			$this->_render(array('errors/error_generic' => $data));
		}
	}


//------------------------------------------------------------------------------------------------------------

/**
 * check_captcha fonction
 *
 * Fonction de callback qui vérifie que le captcha est correct
 *
 *
 * @access	public
 * @param   captcha : string
 * @return  bool : res
 */

public function check_captcha()
{
	if($this->session->userdata('c_val'))
	{
		if($this->encrypt->decode($this->session->userdata('c_val')) == $this->input->post('captcha'))
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('check_captcha',$this->lang->line('incorrect_captcha'));
			return false;
		}
	}
	else
	{
		$this->form_validation->set_message('check_captcha',$this->lang->line('missing_captcha'));
		return false;
	}

}

}
