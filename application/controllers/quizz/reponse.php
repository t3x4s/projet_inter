<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Classe réponse
*/
class Reponse extends MY_Controller
{
	
	//règles de validation
	private $_rules_numeric ='trim|htmlspecialchars|required|max_length[1]|xss_clean|prep_for_form|encode_php_tags|numeric|callback_checkFields';
	private $_rules_string = 'trim|htmlspecialchars|required|max_length[500]|xss_clean|prep_for_form|encode_php_tags';

	/**
	 * @var $_arrayData
	 */
	private $_arrayData = array();


	function __construct()
	{
		parent::__construct();

		$this->load->language('question');
		$this->load->language('errors');
	}

//------------------------------------------------------------------------------------------------------------

/**
 * repondre
 *
 * Permet de répondre à une question
 *
 *
 * @access	public
 * @param   $idQuestion : int
 */

	public function repondre($idQuestion)
	{
		$this->load->model('quizz/question_model');
		$this->load->helper('cookie');
		$this->load->helper('url_token');
		$idQuestion = tokenToId($idQuestion);

		
		if($this->_arrayData['dataQuest'] = $this->question_model->getQuestion($idQuestion))
		{		


			//on vérifie qu'il n'a pas répondu à la question
			if($this->encrypt->decode(get_cookie('rId')) == $this->_arrayData['dataQuest']['QU_id'])
			{
				$this->_render( array('errors/error_already_answered' => '') );
			}
			else
			{
				//si question libre
				if($this->_arrayData['dataQuest']['QU_type_id'] == 3)
				{
					$this->traiterRepLibre($this->_arrayData['dataQuest']['QU_id']);

				}
				else
				{
					$this->traiterRepCM($this->_arrayData['dataQuest']['QU_id']);
				}

		
			}

		}
		else
		{
			$this->_render( array('errors/error_bad_question_id' => '' ));
		}
	}

//------------------------------------------------------------------------------------------------------------

/**
 * checkFields
 *
 * Vérifie qu'au moins 1 case est cochée dans le cas d'un qcm
 *
 *
 * @access	public
 * @return  bool
 */

	public function checkFields ()
	{
		$temp = $this->input->post('optionVal');
		$countNbRep = 0;

		foreach ($temp as $key => $value) {
			if ($value == 1)
			{
				$countNbRep++;
			}	
		}

		if($countNbRep == 0)
		{
			$this->form_validation->set_message('checkFields',$this->lang->line('reponse_atleast_one_option'));
			return false;
		}
		else
		{
			return TRUE;
		}

	}
//------------------------------------------------------------------------------------------------------------

/**
 * traiterRepLibre
 *
 * Permet de gérér les réponses aux questions libres
 *
 *
 * @access	private
 */

	private function traiterRepLibre ($idQuestion)
	{
		$this->load->helper('form');
		$this->load->library('form_validation');


		//règles de validation
		$this->form_validation->set_rules('repQuestion', $this->lang->line('answer'),$this->_rules_string);
		// message d'ereur personalisé
		$this->form_validation->set_message('required', $this->lang->line('error_answer_required'));


		$this->load->model('quizz/question_model');

		$res = $this->_arrayData['dataQuest'] = $this->question_model->getQuestion($idQuestion);

		if($this->form_validation->run())
		{
			//traitement questions libre
			$this->load->model('quizz/reponse_model');	

			if( ! $this->reponse_model->repLibre($res['QU_id']))
			{
				show_error($this->lang->line('error_occured'));			
			}
			else
			{
				//cookie pour éviter qu'il ne réponse 2 fois à la même question
				$cookie = array(
	                   'name'   => 'rId',
	                   'value'  => $this->encrypt->encode($idQuestion),
	                   'expire' => '86500'
	               );

				set_cookie($cookie);		
			}

			$this->_render(array($this->load->view('quizz/rep_questlibre',array('data'=>$res,'success'=>TRUE),TRUE)));	
		}
		else
		{
			$this->_render(array($this->load->view('quizz/rep_questlibre',array('data'=>$res,'errors'=>validation_errors()),TRUE)));	
		}
	
	}

//------------------------------------------------------------------------------------------------------------

/**
 * traiterRepCM
 *
 * Permet de traiter une réponse à choix multiple
 *
 *
 * @access	private
 * @param   $idQuestion : int
 * @return  bool
 */

	public function traiterRepCM ($idQuestion)
	{
		$this->load->helper('form');
		$this->load->helper('url_token');

		$this->load->library('form_validation');

		$this->load->model('quizz/question_model');


		//on récupère les propositions de réponses
		$this->_arrayData['purposals'] = $this->question_model->getProposals($idQuestion);

		if(empty($this->_arrayData['purposals'])) show_error($this->lang->line('error_answer_no_clause'));

		//règles de validation
		$this->form_validation->set_rules('optionVal[]',$this->lang->line('typed_answers'),$this->_rules_numeric);

		//on récupère les infos question
		$this->_arrayData['infosQuestion'] = $this->question_model->getQuestion($idQuestion);

		if($this->form_validation->run())
		{



			if($this->_arrayData)
			{
				$this->load->model('quizz/reponse_model');

				//ajout des réponses (stats)
				if(!$this->reponse_model->repQCM($idQuestion))
				{
					show_error($this->lang->line('error_answer_send'));
				}

					//cookie pour éviter qu'il ne réponse 2 fois à la même question
					$cookie = array(
		                   'name'   => 'rId',
		                   'value'  => $this->encrypt->encode($idQuestion),
		                   'expire' => '86500'
		               );

					set_cookie($cookie);		

					//la réponse a bien été prise en compte
					$this->_arrayData['success_input'] = TRUE;
					$this->_arrayData['dataQuest'] = $this->question_model->getQuestion($idQuestion);
					$this->_arrayData['purposals'] = $this->question_model->getProposals($idQuestion);
					$this->_render(array('quizz/rep_questCM' => $this->_arrayData));

				// print_r($this->input->post('optionVal'));
			}
		}
		else
		{
			// print_r($proposals);
			$this->_arrayData['errors'] = validation_errors();
			$this->_render(array('quizz/rep_questCM' => $this->_arrayData));	
		}
	
	}
}

/* End of file reponse.php */
/* Location: ./application/controllers/quizz/reponse.php */