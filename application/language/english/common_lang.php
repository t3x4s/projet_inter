<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$lang['questions'] = 'questions';
$lang['question'] = 'question';
$lang['answers'] = 'anwsers';
$lang['answer'] = 'answer';

$lang['close']= 'Close';

$lang['no_question'] = 'no questions';
$lang['no_answer'] = 'no anwsers';


$lang['french']='French';
$lang['english']='English';

$lang['typed_answers']='Typed answers';

//fonction check_captcha
$lang['incorrect_captcha'] = 'The typed captcha is incorrect';
$lang['missing_captcha'] = 'The field captcha has not been typed';
$lang['error_create_captcha'] ='Error during the creation of the captcha';
$lang['validation_captcha'] = 'Check code';
$lang['captcha_copy'] = 'Rewrite this text : ';

//one-click question
$lang['true']='True';
$lang['false']='False';
$lang['more']='More';
$lang['less']='Less';
$lang['threePropositions']= 'Three propositions';
$lang['fourPropositions']='Four propositions';
