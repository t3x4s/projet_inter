<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





$lang['home_welcome'] = 'Welcome to our interactive teaching tool !';

$lang['home_youcan'] = 'You can';
$lang['home_create_question'] = ' create a question';

$lang['home_consult_quizz'] = 'You can consult and manage your';

$lang['home_elseyoucan'] = 'Otherwise you can';
$lang['home_login'] = 'connect';
$lang['home_or'] = 'or you can ';
$lang['home_register'] = 'register yourself';
