<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');




//erreur question

$lang['error_no_rights_delete'] = 'You do not have the rights to delete this question';
$lang['error_no_rights_close'] = 'You do not have the rights to close this question';
$lang['error_wrong_identifier_question'] = 'The identifier oh the question is incorrect';
$lang['error_delete_question'] = 'Error during the question suppression';
$lang['error_close_question'] = 'Error during the question closure';
$lang['error_erase_result'] ='Error during the results suppression of the question';
$lang['error_no_rights_erase'] = 'You do not have the rights to erase the results of this question';
$lang['error_no_rights_action'] = 'You do not have the rights to perform this action';
$lang['error_no_question'] = 'This question does not exist';
$lang['error_occured_database'] = 'An error occured (database)';
$lang['error_occured'] = 'An error occured';

//erreur réponse

$lang['error_answered_already'] = 'You have already answered this question';
$lang['error_answer_no_clause'] = 'There is no clause for this question';
$lang['error_answer_send'] = 'An error occured during the answer sending';
$lang['error_answer_generate'] = 'An error occured during the answers generation';
$lang['error_answer_required'] = 'Thanks to select an answer';

//erreur stats
$lang['error_no_answers'] = 'There are no answers yet !';
