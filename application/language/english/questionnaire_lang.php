<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');




// questionnaire
$lang['questionnaire']= 'My quizzes ';
$lang['aucunQuestionnaire']= 'You have no quizzes !';
$lang['questionnaire_create']= 'Create a quizz';
$lang['questionnaire_remove']= 'Delete this quizz';
$lang['questionnaire_add']= 'Add a new quizz';
$lang['questionnaire_add_placeholder']= 'Quizz name';
$lang['questionnaire_add_submit']= 'Submit';
$lang['questionnaire_manage']= 'Your quizzes';
$lang['questionnaire_changeName']= 'Modify name';
$lang['questionnaire_addQuestion'] = 'Add a question to this quizz';

$lang['questionnaire_field_name']= 'quizz name';

$lang['loading_button'] = 'Loading...';
$lang['shorten_link'] = 'Shorten this link';



// diffuser le questionnaire
$lang['questionnaire_submit'] = 'Submit this quizz';
$lang['questionnaire_broadcastQrcode']= 'Broadcast this QrCode';
$lang['questionnaire_broadcastLink'] = 'Or share this link :';
$lang['questionnaire_sharelink'] = 'Share link';

// ou une question à la fois
$lang['question_share'] = 'Share';





// one click question
$lang['oneclick_name'] = 'One-click questions';
$lang['true/false'] = 'true/false';
$lang['more/less'] = 'more/less';




// Gerer les questions des questionnaire
$lang['operations_question']='Manage this question';
$lang['manage'] = 'Manage';
$lang['modify'] = 'Modify';
$lang['remove'] = 'Delete';
$lang['stats']  = 'Statistics';
$lang['share']  = 'Share';
$lang['export'] = 'Export ';
$lang['delete_answers'] = "Erase answers ";
$lang['update_order'] = "Update Order";

$lang['answers_erased'] = "All the answers have been erased";
$lang['answers_not_erased'] = "An error occured, anwsers were not erased";

$lang['answers_erased_question'] = "All the answers have been erased";
$lang['answers_not_erased_question'] = "An error occured, anwsers were not erased";

$lang['quizz_erased'] = "The quizz has been erased";
$lang['quizz_not_erased'] = "An error occured, the quizz was not erased";

$lang['question_erased'] = "The question has been erased";
$lang['question_not_erased'] = "An error occured, the question was not erased";

$lang['questionnaire_changedOrder']= 'Your quizz order was successfully changed';
$lang['questionnaire_not_changedOrder']= 'Error : your quizz order was not changed';

$lang['questionnaire_confirm_remove'] = 'This quizz and all its questions will be removed';
$lang['question_confirm_remove'] = 'This question will be removed';

$lang['questionnaire_confirm_delete_answer'] = 'All this quizz answers will be removed';
$lang['question_confirm_delete_answer'] = 'All this question answers will be removed';

$lang['error_recovery_result']='An error occured during the results recovery';

// message pour l'utilisateur
$lang['questionnaire_no_question'] = 'You don\'t have any questions linked to this quizz : ';
$lang['questionnaire_create_first_question'] = 'Add a question now ';


//Réponse questionnaire
$lang['questionnaire_success'] = "Your answers have been registered";
$lang['questionnaire_already_done'] = "You already answered this quizz";
$lang['questionnaire_error_submit'] = "An error occured when submitting your answers";
$lang['questionnaire_error_insert'] = "An error occured when submitting your answers";
$lang['questionnaire_no_answer'] = "You did not answer to the quizz";
$lang['questionnaire_error_results'] = "An error occured when gathering the answers";

