<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//email
$lang['email_from'] = 'Interactive teaching';
$lang['email_register_title'] = 'Registration';
$lang['email_register_subject'] = 'Interactive teaching : registration';
$lang['email_prof'] = '<h4>Welcome!</h4>
					<p>You are now a member of the site, <br /><br />
					we remind you that you made a request to have teacher rights.
					To confirm this request, you have to open the following link: <br />';
$lang['email_password_subject'] = 'Password recovery';
$lang['email_password_content'] = '<h4>Hello!</h4>
				<p>If you were to forget your password, we invite you to reset it</p>
				<p>For that thanks to click on this link : </p>';

$lang['email_password_reinit'] = '<h4>Hello!</h4>
  				<p>We just reseted your password</p>
  				<p>You can now connect with these new identifiers :</p>';


//success messages
$lang['success_desc_register'] = 'Successful registration !';
$lang['success_redirect'] = 'You are going to be automatically redirected';
$lang['success_info'] = 'Don\'t hesitate to contact the technical support if you encouter any problem';				

$lang['success_desc_login'] = 'Succesful connection !';
$lang['success_password_reinit'] =  'A password reset link has been sent to you by email';


//errors
$lang['error_send_email'] = 'Error during the sending of the email';
$lang['error_login'] = 'An error occured at the connection';
$lang['error_email_validation'] = 'A problem occured during the validation, please contact the technical support';
$lang['error_password_reinit'] = 'A problem occured during the reset, please contact the technical support';
$lang['error_add_member'] = 'An error occured during the account creation';

//r�gles de validation
//register
$lang['validation_email'] = 'Email';
$lang['validation_pass'] = 'Password';
$lang['validation_pass2'] = 'Password confirmation';
$lang['validation_login'] ='Login';
$lang['validation_prof'] = 'Teacher registration';
$lang['validation_old_password'] = 'Former password';

//login
$lang['validation_login_or_email'] = 'login or email';


// fonction check_logs
$lang['incorrect_password'] ='Wrong password';
$lang['unknown_user'] = 'This user account does not exist';
$lang['user_not_activated'] = 'This user account is not activate for now';


//fonction email_validation
$lang['active_account'] = 'Your account is already activate or does not exist';
$lang['email_validation_success'] = 'Thank you for be register, you can now connect';		
$lang['email_validation_failure'] = 'A problem occured during the validation, please contact the technical support';


//fonction change_password
$lang['password_changed'] = 'Your password has been changed';


//fonction check_old_pw
$lang['wrong_old_password'] = 'The former password is incorrect';


//fonction password_reinit
$lang['new_pass'] = 'A new password has been send to you by email';




// ------------------------------------------------------------------------
// Vues
// ------------------------------------------------------------------------
//change_password
$lang['success_register'] = 'Your account has been registered with success';
$lang['fieldset_pw_change'] = 'Password modification';
$lang['error_pw_change'] = 'An error occured while updating your password';
$lang['new_password'] = 'New password';
$lang['new_password_confirm'] = 'Password confirmation';


//forgot password
$lang['fieldset_forgot_pw'] = 'You forgot your password ?';


//login
$lang['fieldset_connection'] = 'Connection information';
$lang['forgot_password'] = 'You forgot your password ?';

//register
$lang['fieldset_security'] ='Security';

//success
$lang['clic_redirect'] = 'Click here if you are not redirected';

//send & abort
$lang['form_send'] = 'Send';
$lang['form_abort'] = 'Cancel';

//link
$lang['use_link']='Instead of the Qrcode you can also share this link';

//email inconnu
$lang['unknown_email'] = 'We were unable to find this email in our records.';

/* End of file user_lang.php */
/* Location: ./application/language/english/user_lang.php */
