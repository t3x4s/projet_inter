<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*   -----------------------   Navbar ------------------------   */
$lang['navbar_create_question'] = 'Ask a question ';
$lang['navbar_myquestionnaires'] = 'My quizzes ';

$lang['navbar_logout'] = 'Logout ';
$lang['navbar_login'] = 'Login ';
$lang['navbar_register'] = 'Register ';


$lang['french_title']='French';
$lang['english_title']='English';
$lang['lang_title']='Language';

$lang['profil_title']='My profile';
$lang['passwd_change']='Change password';

/*  ------------------------ End of Navbar    ------------------------    */