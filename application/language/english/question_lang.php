<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//question
$lang['question_libelle'] = 'Question\'s title';
$lang['question_type'] = 'Question\'s type';
$lang['question_choix'] = 'Clauses';
$lang['question_choix_add'] = 'Enter a choice name';
$lang['question_option'] ='Add a clause';
$lang['question_option_valide'] ='Valid answer';
$lang['question_edit_confirm'] ='Confirm your modifications';
$lang['question_fieldset'] = 'Create a question';
$lang['question_withouth_questionnaire'] = 'Not linked to a questionnaire';

//success question
$lang['question_success'] = 'The question has been registered';

$lang['question_edit'] = 'Click here to modify the question';
$lang['question_qrcode'] = 'Click here to access to the QrCode';
$lang['question_sharelink'] = 'Share link';
$lang['question_get_stats'] = 'Click here to access to the statistics';

$lang['question_share'] = 'Share this question';
$lang['quizz_share'] = 'Share the quizz';

$lang['loading_button'] = 'Loading...';
$lang['shorten_link'] = 'Shorten this link';


$lang['question_directlink'] = "You can also share this link :";


//question menu
$lang['question_ask'] = 'Ask a question';
$lang['question_stats'] = 'Statistics for a question';

//question Type
$lang['question_qcm'] = 'MCQ';
$lang['question_qcs'] = 'SCQ';
$lang['question_libre'] = 'Open question';

// statistiques
$lang['answers_are']='The answers are : ';
$lang['question_stats_no_answers'] = 'There are no answers to this question';
$lang['question_stats_one_answer'] = 'There is one answer';
$lang['question_stats_multiple_answers'] = ' people answered this question'; // sentence end


//réponse question
$lang['reponse_correct_input'] = 'The answer has been registered';
$lang['reponse_correct'] = 'Good answer';
$lang['reponse_notcorrect'] = 'Wrong answer';
$lang['reponse_titre'] = 'Your answer<br>(500 character limit)';
$lang['reponse_enregistree'] = 'Your answer has been registered';
$lang['reponse_atleast_one_option']='You must choose at least one option';

//envoi réponse
$lang['reponse_envoi'] = 'Send the answer';

//envoi question
$lang['question_envoi'] = 'Send the question';


$lang['question_close'] = 'The question has been closed';
$lang['question_delete'] = 'The question has been deleted';
$lang['result_erase'] = 'Results has been erased';
$lang['mofify_question']='Modify the question';
$lang['answer_question']='Answer to a question';
$lang['modification_question_ok']='Modifications have been take into account';
$lang['question_new_link']='From now on you can share your question with this following link : ';
$lang['answers_to_question']='Question answers : ';

/* End of file question_lang.php */
/* Location: ./application/language/english/question_lang.php */