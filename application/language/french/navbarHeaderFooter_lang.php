<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*   -----------------------   Navbar ------------------------   */
$lang['navbar_create_question'] = 'Poser une question ';
$lang['navbar_myquestionnaires'] = 'Mes questionnaires ';

$lang['navbar_logout'] = 'Se déconnecter ';
$lang['navbar_login'] = 'Se connecter ';
$lang['navbar_register'] = 'Inscription ';


$lang['french_title']='Fran&#231;ais';
$lang['english_title']='Anglais';
$lang['lang_title']='Langue';

$lang['profil_title']='Mon profil';
$lang['passwd_change']='Changer mot de passe';

/*  ------------------------ End of Navbar    ------------------------    */