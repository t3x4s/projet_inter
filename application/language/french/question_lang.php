<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//question
$lang['question_libelle'] = 'Intitulé de la question';
$lang['question_type'] = 'Type de question';
$lang['question_choix'] = 'Propositions';
$lang['question_choix_add'] = 'Entrez un nom de choix...';
$lang['question_option'] ='Ajouter une proposition';
$lang['question_option_valide'] ='Réponse valide';
$lang['question_edit_confirm'] ='Confirmer la modification';
$lang['question_fieldset'] = 'Créer une question';
$lang['question_withouth_questionnaire'] = 'Non liée à un questionnaire';

//success question
$lang['question_success'] = 'La question a bien été enregistrée';

$lang['question_edit'] = 'Cliquez ici pour la modifier';
$lang['question_qrcode'] = 'Cliquez ici pour accéder au QrCode';
$lang['question_sharelink'] = 'Lien de partage';
$lang['question_get_stats'] = 'Cliquez ici pour accéder aux statistiques';
$lang['question_get_stats_later'] = 'Pour accéder aux statistiques plus tard conservez ce code (a rentrer dans la barre de recherche) : ';

$lang['question_share'] = 'Partager cette question';
$lang['quizz_share'] = 'Partager le questionnaire';

$lang['loading_button'] = 'Chargement...';
$lang['shorten_link'] = 'Réduire ce lien';


$lang['question_directlink'] = "Vous pouvez aussi diffuser ce lien :";


//question menu
$lang['question_ask'] = 'Poser une question';
$lang['question_stats'] = 'Statistiques pour une question';

//fonction check_captcha
$lang['incorrect_captcha'] = 'Le captcha entré est incorrect';
$lang['missing_captcha'] = 'Le champ Captcha n\'a pas été entré';

//question Type
$lang['question_qcm'] = 'QCM';
$lang['question_qcs'] = 'Choix unique';
$lang['question_libre'] = 'Champ libre';


// statistiques
$lang['answers_are']= 'Les réponses sont : ';
$lang['question_stats_no_answers'] = 'Personne n\'a répondu à cette question';
$lang['question_stats_one_answer'] = 'Une personne a répondu à cette question';
$lang['question_stats_multiple_answers'] = ' personnes ont répondu à cette question'; // fin de phrase



//réponse question
$lang['reponse_correct_input'] = 'La réponse a bien été enregistrée';
$lang['reponse_correct'] = 'Bonne réponse';
$lang['reponse_notcorrect'] = 'Mauvaise réponse';
$lang['reponse_titre'] = 'Votre réponse<br>(500 caractères max)';
$lang['reponse_enregistree'] = 'Votre réponse a bien été enregistrée';
$lang['reponse_atleast_one_option']='Vous devez cocher au minimum 1 option';

//envoi réponse
$lang['reponse_envoi'] = 'Envoyer la réponse';

//envoi question
$lang['question_envoi'] = 'Envoyer la question';


$lang['question_close'] = 'La question a été fermée';
$lang['question_delete'] = 'La question a été supprimée';
$lang['result_erase'] = 'Résultats supprimés';
$lang['mofify_question']='Modifier la question';
$lang['answer_question']='Répondre à une question';
$lang['modification_question_ok']='Les modifications ont été prises en compte';
$lang['question_new_link']='Vous pouvez désormais partager votre question avec le lien de partage suivant :';
$lang['answers_to_question']='Réponses à la question : ';

/* End of file question_lang.php */
/* Location: ./application/language/french/question_lang.php */