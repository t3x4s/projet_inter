<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$lang['questions'] = 'questions';
$lang['question'] = 'question';
$lang['answers'] = 'réponses';
$lang['answer'] = 'réponse';

$lang['close']= 'Fermer';

$lang['no_question'] = 'pas de questions';
$lang['no_answer'] = 'pas de réponses';


$lang['french']='Français';
$lang['english']='Anglais';

$lang['typed_answers']='Réponses entrées';

//fonction check_captcha
$lang['incorrect_captcha'] = 'Le captcha entré est incorrect';
$lang['missing_captcha'] = 'Le champ Captcha n\'a pas été entré';
$lang['error_create_captcha'] ='Erreur lors de la création du captcha';
$lang['validation_captcha'] = 'Code de vérification';
$lang['captcha_copy'] = 'Recopiez le texte suivant : ';

//one-click question
$lang['false']='Faux';
$lang['true']='Vrai';
$lang['more']='Plus';
$lang['less']='Moins';
$lang['threePropositions']= 'Trois propositions';
$lang['fourPropositions']='Quatre propositions';
