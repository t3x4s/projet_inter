<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');




// questionnaire
$lang['questionnaire']= 'Mes questionnaires ';
$lang['aucunQuestionnaire']= 'Vous n\'avez aucun questionnaire !';
$lang['questionnaire_create']= 'Créer un questionnaire';
$lang['questionnaire_remove']= 'Supprimer le questionnaire';
$lang['questionnaire_add']= 'Ajouter un nouveau questionnaire';
$lang['questionnaire_add_placeholder']= 'Nom du questionnaire';
$lang['questionnaire_add_submit']= 'Envoyer';
$lang['questionnaire_manage']= 'Gestion de vos questionnaires';
$lang['questionnaire_changeName']= 'Modifier nom';
$lang['questionnaire_addQuestion'] = 'Ajouter une question à ce questionnaire';

$lang['questionnaire_field_name']= 'nom de votre questionnaire';

$lang['loading_button'] = 'Chargement...';
$lang['shorten_link'] = 'Réduire ce lien';



// diffuser le questionnaire
$lang['questionnaire_submit'] = 'Partager ce questionnaire';
$lang['questionnaire_broadcastQrcode']= 'Diffuser ce QrCode';
$lang['questionnaire_broadcastLink'] = 'Ou bien diffuser ce lien :';
$lang['questionnaire_sharelink'] = 'Lien de partage';

// ou une question à la fois
$lang['question_share'] = 'Partager';





// one click question
$lang['oneclick_name'] = 'One-click questions';
$lang['true/false'] = 'vrai/faux';
$lang['more/less'] = 'plus/moins';




// Gerer les questions des questionnaire
$lang['operations_question']='Opérations sur la question';
$lang['manage'] = 'Gérer ';
$lang['modify'] = 'Modifier ';
$lang['remove'] = 'Supprimer ';
$lang['stats']  = 'Statistiques ';
$lang['share']  = 'Partager';
$lang['export'] = 'Exporter ';
$lang['delete_answers'] = 'Effacer réponses ';
$lang['update_order'] = "Changer l'ordre";

$lang['answers_erased'] = 'Toutes les réponses de ce questionnaire ont été supprimées';
$lang['answers_not_erased'] = 'Erreur : les réponses n\'ont pas été supprimées';

$lang['answers_erased_question'] = 'Toutes les réponses de cette question ont été supprimées';
$lang['answers_not_erased_question'] = 'Erreur : les réponses n\'ont pas été supprimées';

$lang['quizz_erased'] = "Le questionnaire a été supprimé";
$lang['quizz_not_erased'] = "Erreur : le questionnaire n'a pas été supprimé";

$lang['question_erased'] = "La question a été supprimée";
$lang['question_not_erased'] = "Erreur : la question n'a pas été supprimée";

$lang['questionnaire_changedOrder']= 'L\'ordre du questionnaire a été changé';
$lang['questionnaire_not_changedOrder']= 'L\'ordre du questionnaire n\'a pas été changé';

$lang['questionnaire_confirm_remove'] = 'Voulez vous vraiment supprimer ce questionnaire ?';
$lang['question_confirm_remove'] = 'Voulez vous vraiment supprimer cette question ?';

$lang['questionnaire_confirm_delete_answer'] = 'Voulez vous vraiment effacer toutes les réponses ';
$lang['question_confirm_delete_answer'] = 'Voulez vous vraiment effacer toutes les réponses de cette question ?';


$lang['error_recovery_result']='Une erreur est survenue durant la récupération des résultats';

// message pour l'utilisateur
$lang['questionnaire_no_question'] = 'Vous n\'avez encore aucune question liée au questionnaire : ';
$lang['questionnaire_create_first_question'] = ' Créez en une dès maintenant ! ';


//Réponse questionnaire
$lang['questionnaire_success'] = 'Les réponses ont bien été enregistrées';
$lang['questionnaire_already_done'] = 'Vous avez déjà répondu à ce questionnaire';
$lang['questionnaire_error_submit'] = 'Une erreur est survenue lors de la soumission du questionnaire';
$lang['questionnaire_error_insert'] = 'Une erreur est survenue lors de l\'envoi du questionnaire';
$lang['questionnaire_no_answer'] = 'Vous n\'avez pas répondu au questionnaire';
$lang['questionnaire_error_results'] = 'Une erreur est survenue durant la récupération des résultats';

