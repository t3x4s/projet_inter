<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//email
$lang['email_from'] = 'Enseignement interactif';
$lang['email_register_title'] = 'Inscription';
$lang['email_register_subject'] = 'Enseignement interactif : inscription';
$lang['email_intro'] = "<h4>Bienvenue!</h4>
					Vous êtes maintenant membre du site !<br/>
					Afin de confirmer cette demande, vous devez ouvrir le lien suivant: "; 
$lang['email_password_subject'] = 'Récupération de mot de passe';
$lang['email_password_content'] = '<h4>Bonjour!</h4>
				<p>Suite à votre oubli de mot de passe, nous vous invitons à le réinitialiser</p>
				<p>Pour cela, merci de cliquer sur le lien suivant : </p>';

$lang['email_password_reinit'] = '<h4>Bonjour!</h4>
  				<p>Nous avons réinitialisé votre mot de passe</p>
  				<p>Vous pouvez désormais vous connecter avec ces identifiants :</p>';


//success messages
$lang['success_desc_register'] = 'Inscription réussie !';
$lang['success_redirect'] = 'Vous allez être redirigé automatiquement';
$lang['success_info'] = 'N\'hésitez pas à contacter le support en cas de problème';				

$lang['success_desc_login'] = 'Connexion réussie !';
$lang['success_password_reinit'] =  'Un lien de réinitialisation de mot de passe vous a été envoyé par email';


//errors
$lang['error_send_email'] = 'Erreur lors de l\'envoi de l\'email';
$lang['error_login'] ='Mauvais login/Mot de passe';
$lang['error_email_validation'] = 'Un problème est survenu lors de la validation, veuillez contacter le support';
$lang['error_password_reinit'] = 'Un problème est survenu lors de la réinitialisation, veuillez contacter le support';
$lang['error_add_member'] = 'Une erreur est survenue durant la création du compte';


//règles de validation
//register
$lang['validation_email'] = 'Email';
$lang['validation_pass'] = 'Mot de passe';
$lang['validation_pass2'] = 'Confirmation de mot de passe';
$lang['validation_login'] ='Nom d\'utilisateur';
$lang['validation_prof'] = 'Inscription professeur';
$lang['validation_old_password'] = 'Ancien mot de passe';

//login
$lang['validation_login_or_email'] = 'Nom d\'utilisateur ou email';


// fonction check_logs
$lang['incorrect_password'] ='Mot de passe incorrect';
$lang['unknown_user'] = 'Ce compte utilisateur n\'existe pas';
$lang['user_not_activated'] = 'Ce compte utilisateur n\'est pas activé';


//fonction email_validation
$lang['active_account'] = 'Votre compte est déjà activé ou inexistant';
$lang['email_validation_success'] = 'Merci de vous être enregistré, vous pouvez maintenant vous connecter';		
$lang['email_validation_failure'] = 'Un problème est survenu lors de la validation, veuillez contacter le support';


//fonction change_password
$lang['password_changed'] = 'Votre mot de passe a bien été changé';


//fonction check_old_pw
$lang['wrong_old_password'] = 'L\'ancien mot de passe est incorrect';


//fonction password_reinit
$lang['new_pass'] = 'Un nouveau mot de passe vous a été envoyé par email';




// ------------------------------------------------------------------------
// Vues
// ------------------------------------------------------------------------
//change_password
$lang['success_register'] = 'Votre compte a été enregistré avec succès';
$lang['fieldset_pw_change'] = 'Changement de mot de passe';
$lang['error_pw_change'] = ' Une erreur est parvenue pendant la mise à jour du mot de passe';
$lang['new_password'] = 'Nouveau mot de passe';
$lang['new_password_confirm'] = 'Confirmation du nouveau mot de passe';


//forgot password
$lang['fieldset_forgot_pw'] = 'Mot de passe oublié';


//login
$lang['fieldset_connection'] = 'Information de connexion';
$lang['forgot_password'] = 'Mot de passe oublié ?';

//register
$lang['fieldset_security'] ='Sécurité';

//success
$lang['clic_redirect'] = 'Cliquez ici si vous n\'êtes pas redirigé';

//send & abort
$lang['form_send'] = 'Envoyer';
$lang['form_abort'] = 'Annuler';

//link
$lang['use_link']='Au lieu du qrcode vous pouvez aussi diffuser ce lien';

//email inconnu
$lang['unknown_email'] = 'Cet email n\'est associé à aucun compte';

/* End of file user_lang.php */
/* Location: ./application/language/french/user_lang.php */
