<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





$lang['home_welcome'] = 'Bienvenue sur l\'outil d\'enseignement interactif !';

$lang['home_youcan'] = 'Vous pouvez';
$lang['home_create_question'] = ' créer une question';

$lang['home_consult_quizz'] = 'Vous pouvez consulter et gérer vos';

$lang['home_elseyoucan'] = 'Sinon vous pouvez vous';
$lang['home_login'] = 'connecter';
$lang['home_or'] = 'ou vous';
$lang['home_register'] = 'enregistrer';
