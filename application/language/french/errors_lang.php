<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');




//erreur question

$lang['error_no_rights_delete'] = 'Vous n\'avez pas les droits pour supprimer cette question';
$lang['error_no_rights_close'] = 'Vous n\'avez pas les droits pour fermer cette question';
$lang['error_wrong_identifier_question'] = 'L\'identifiant de la question est incorrect';
$lang['error_delete_question'] = 'Erreur à la suppression de la question';
$lang['error_close_question'] = 'Erreur à la fermeture de la question';
$lang['error_erase_result'] ='Erreur à la suppression des résultats de la question';
$lang['error_no_rights_erase'] = 'Vous n\'avez pas les droits pour supprimer les résultats de cette question';
$lang['error_no_rights_action'] = 'Vous n\'avez pas les droits pour effectuer cette action';
$lang['error_no_question'] = 'Cette question n\'existe pas';
$lang['error_occured_database'] = 'Une erreur s\'est produite (database)';
$lang['error_occured'] = 'Une erreur est survenue';

//erreur réponse
$lang['error_answer_required'] = 'Merci d\'indiquer une réponse';
$lang['error_answered_already'] = 'Vous avez déjà répondu à cette question';
$lang['error_answer_no_clause'] = 'Il n\'existe pas de propositions pour cette question';
$lang['error_answer_send'] = 'Une erreur est survenue lors de l\'envoi de la réponse';
$lang['error_answer_generate'] = 'Une erreur est survenue lors de la génération des réponses';

//erreur stats
$lang['error_no_answers'] = 'Personne n\'a encore répondu a cette question !';
