$(function() {

	$('#success_message').hide();

	$('#form_edit_team').submit(function(e){
  		e.preventDefault();

  		$.ajax({
  			type : "POST",
  			async : false,
  			url : "<?php echo base_url().'ajax/team/editTeam/'.$this->uri->segment(4);?>",
  			data : $(this).serializeArray(),
  			dataType : 'json',
  			success : function(data)
  			{
  				if(data.validate == false)
  				{
  					if(data.nom_team != '')
					{
						$("#nom_team").append("<span class='label label-important'>"+data.nom_team+'</span>');
					}

                    if(data.pass_team != '')
                    {
                        $("#pass_team").append("<span class='label label-important'>"+data.pass_team+'</span>');
                    }

                    if(data.tag_team != '')
                    {
                        $("#tag_team").append("<span class='label label-important'>"+data.tag_team+'</span>');
                    }

                    if(data.pays_team != '')
                    {
                        $("#pays_team").append("<span class='label label-important'>"+data.pays_team+'</span>');
                    }

                    if(data.desc_team != '')
                    {
                    	$("#desc_team").append("<span class='label label-important'>"+data.desc_team+'</span>');
                    }

                    if(data.site_team != '')
                    {
                    	$("#site_team").append("<span class='label label-important'>"+data.site_team+'</span>');
                    }
  				}
  				else if(data.validate == true)
  				{
  					$('#success_message').show(300);
  					$('#success_message').hide(3000);
  				}
  			}
  		});
  		

  		return false;
  	});
	
});