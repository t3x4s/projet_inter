$(function() {

$('#loader')
    .hide()  // hide it initially
    .ajaxStart(function() {
    	console.log(this);
        $(this).show();
    })
    .ajaxStop(function() {
    	console.log(this);
        $(this).hide();
    })
;
	$('#error').hide();

	
	$("select").change(function(){
		//clear errors
		$('#error p').empty();

		
  		if($(this).val() == "1")
  			{
  				alert("Vous êtes sur le point de promouvoir cette personne au rang de chef !");
  			}
			
		
		$.ajax({
			type : "POST",
			async : false,
			url : "<?php echo base_url().'ajax/team/edit_rights/'.$this->uri->segment(4);?>",
			data : {
				<?=$this->security->get_csrf_token_name();?> : '<?=$this->security->get_csrf_hash();?>',
				'user_role' : $(this).val(),
				'user_id' : $('td:first', $(this).parents('tr')).text()
			},
			dataType : 'json',
			success : function(data)
			{
				if(data.validate == false)
				{
					if(data.message == true)
					{
						$("#error p").append("Une équipe ne peut être sans chef !");
					}
					else
					{
						if(data.user_role != '')
						{
							$("#error").append(data.user_role);
						}

						if(data.user_id != '')
						{
							$("#error").append("<span>"+data.user_id +"</span>");
						}
					}
					

					$("#error").show(300);
				}
				else if(data.validate == true)
				{
					$("#error").addClass("alert alert-success");
					$("#error").append("<span> Votre équipe a été mise à jour </span>");
					$("#error").show(300);
				}

			}
		});

		return false;
		});
});
