$(function(){

$('#loader')
    .hide()  // hide it initially
    .ajaxStart(function() {
    	console.log(this);
        $(this).show();
    })
    .ajaxStop(function() {
    	console.log(this);
        $(this).hide();
    })
;

	var uri_segment = "<?=$this->uri->segment(3);?>";




	$('#editRights').click(function (e){
		e.preventDefault();

		$(this).parent().siblings().removeClass('active');


		$(this).parent('li').addClass('active');

		$.ajax({
			type : "GET",
			async : false,
			url : "<?=base_url().'ajax/team/get_editRights/';?>"+uri_segment,
			dataType : "html",
			success : function(data){
    				$('#divPage').html(data);
			}
		});
    	return false;
	});
	
	$('#editTeam').click(function (e) {

		e.preventDefault();
		$(this).parent().siblings().removeClass('active');

		$(this).parent('li').addClass('active');



		$.ajax({
			type : "GET",
			async : true,
			url : "<?=base_url().'ajax/team/get_editTeam/';?>"+uri_segment,
			dataType : "html",
			success : function(data){
    				$('#divPage').html(data);

			}
		});

    	return false;
 	});
	$('#addMember').click(function (e){
		
		e.preventDefault();

		$(this).parent().siblings().removeClass('active');


		$(this).parent('li').addClass('active');



			$.ajax({
				type : "GET",
				async : false,
				url : "<?=base_url().'ajax/team/get_addMember/'.$this->uri->segment(3);?>",
				dataType : "html",
				success : function(data){
					$('#divPage').html(data);

				}
			});
	    	return false;
	});


});