$(function(){
	$('#add_team').hide();

	$('#loader')
    .hide()  // hide it initially
    .ajaxStart(function() {
    	console.log(this);
        $(this).show();
    })
    .ajaxStop(function() {
    	console.log(this);
        $(this).hide();
    })
;

    $('#success_message').hide();

	$('#btn_add_team').click(function(){
		if($('#add_team').css('display') == 'none')
		{
			$('#add_team').show(700);

		}
		else
		{
			$('#add_team').hide(300);
		}

		return false;
	});


	$('form').submit(function(e){


		e.preventDefault();

        clearErrors();

		var n_team = $('input[name=nom_team]').val();
		var t_team = $('input[name=tag_team]').val();
		var pw_team = $('input[name=pass_team]').val();
		var pays_team = $('select[name=pays_team]').val();

		$.ajax({
			type : "POST",
			async : false,
			url : "<?php echo base_url().'ajax/team/CreateTeam';?>",
			data : {
					'nom_team' : n_team,
					'pass_team' : pw_team,
					'tag_team' : t_team,
					'pays_team' : pays_team,
					'<?=$this->security->get_csrf_token_name();?>' : '<?=$this->security->get_csrf_hash();?>' 
				},
			dataType : 'json',
			success : function(data)
			{

				if(data.validate == false)
				{
					if(data.nom_team != '')
					{
						$("#nom_team").append("<span class='label label-important'>"+data.nom_team+'</span>');
					}

                    if(data.pass_team != '')
                    {
                        $("#pass_team").append("<span class='label label-important'>"+data.pass_team+'</span>');
                    }

                    if(data.tag_team != '')
                    {
                        $("#tag_team").append("<span class='label label-important'>"+data.tag_team+'</span>');
                    }

                    if(data.pays_team != '')
                    {
                        $("#pays_team").append("<span class='label label-important'>"+data.pays_team+'</span>');
                    }
				}
                else if(data.validate == true)
                {
                    $('#add_team').hide(300);
                    $('#success_message').show(300);

                   window.setTimeout(function(){location.reload()},5000);

                }
                else
                {
                    alert('Unknown error');
                }


			}
			
		});
		return false;
	});

    
    
    function clearErrors() 
    {
        $("#tag_team span").remove();
        $("#nom_team span").remove();
        $("#pass_team span").remove();
        $("#pays_team span").remove();
    }

});

